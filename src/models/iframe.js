/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import { query,create, remove, update, roleList } from 'services/user'
import { selection } from 'services/organization'

import { pageModel } from 'utils/model'

const { prefix } = config

export default modelExtend(pageModel, {
  namespace: 'iframe',

  state: {
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
    isMotion: window.localStorage.getItem(`${prefix}userIsMotion`) === 'true',
    editingKey: '',
    treeSelectData: [],
    listByOrganization: []
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/app/user') {
          dispatch({
            type: 'query',
            payload: location.query,
          })
        }
      })
    },
  },

  effects: {

    * query ({ payload = {} }, { call, put }) {
      const data = yield call(query, payload)
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount,
            },
          },
        })
      }
    },

    * delete ({ payload }, { call, put, select }) {
      const data = yield call(remove, { userIds: payload })
      const { selectedRowKeys } = yield select(_ => _.user)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: selectedRowKeys.filter(_ => _ !== payload) } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * multiDelete ({ payload }, { call, put }) {
      const data = yield call(remove, payload)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: [] } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * create ({ payload }, { call, put }) {
      const data = yield call(create, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update ({ payload }, { select, call, put }) {
      const userId = yield select(({ user }) => user.currentItem.userId)
      const newUser = { ...payload, userId }
      const data = yield call(update, newUser)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * modal ({ payload }, { call, put }) {
      let listByOrganization = []
        const roleData = yield call(roleList);
        listByOrganization = roleData.list
      const data = yield call(selection)
      if (data.success) {
        yield put({ 
          type: 'showModal',
          payload: {
            modalType: payload.modalType,
            treeSelectData: data.organizationList,
            currentItem: payload.modalType==='update'?payload.currentItem:'',
            listByOrganization
          }
        })
      } else {
        throw data
      }
    },

    * roleList ({ payload }, { call, put }) {
      const data = yield call(roleList, {organizationId:payload})
      if (data.success) {
        yield put({ 
          type: 'updateState',
          payload: {
            listByOrganization: data.list
          }
        })
      } else {
        throw data
      }
    },
  },

  reducers: {

    showModal (state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal (state) {
      return { ...state, modalVisible: false }
    },

    switchIsMotion (state) {
      window.localStorage.setItem(`${prefix}userIsMotion`, !state.isMotion)
      return { ...state, isMotion: !state.isMotion }
    },

    editingKey(state,{payload}) {
      return {...state,...payload}
    }
  },
})
