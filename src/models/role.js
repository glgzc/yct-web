import modelExtend from 'dva-model-extend'
import { query, info, remove, update, save } from 'services/role'
import { query as menuSelect } from 'services/menu'

import { pageModel } from 'utils/model'

export default modelExtend(pageModel, {

  namespace: 'role',
  state: {
    treeData: [],
    treeCheckedKeys: [],
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
  },
  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/sys/role') {
          dispatch({ type: 'query',
            payload: {
              ...location.query,
            } })
        }else{
          dispatch({ type: 'clear'})
        }
      })
    },
  },

  effects: {
    * query ({
      payload,
    }, { call, put }) {
      const data = yield call(query, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(data.page.current) || 1,
              pageSize: Number(data.page.pageSize) || 10,
              total: data.page.totalPage,
            },
          },
        })
      } else {
        throw data
      }
    },
    * delete ({ payload }, { call, put, select }) {
      const roleIds = [payload.id]

      const data = yield call(remove, { roleIds })
      const { selectedRowKeys } = yield select(_ => _.role)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: selectedRowKeys.filter(_ => _ !== payload) } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * multiDelete ({ payload }, { call, put }) {
      const data = yield call(remove, payload)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: [] } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * add ({ payload }, { call, put }) {
      const data = yield call(menuSelect, payload)
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            treeData: data.list,
            treeCheckedKeys: [],
            modalType: 'create',
          },
        })
      } else {
        throw data
      }
    },
    * edit ({ payload }, { call, put }) {
      const currentItem = yield call(info, payload)
      const data = yield call(menuSelect, payload)
      const role = currentItem.role
      const treeCheckedKeys = []
      if (role.menuIdList != undefined && role.menuIdList != null) {
        role.menuIdList.map((v) => {
          treeCheckedKeys.push(String(v))
        })
      }
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            currentItem: role,
            treeCheckedKeys,
            treeData: data.list,
            modalType: 'update',
          },
        })
      } else {
        throw data
      }
    },
    * create ({ payload }, { call, put }) {
      const data = yield call(save, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update ({ payload }, { select, call, put }) {
      const roleId = yield select(({ role }) => role.currentItem.roleId)
      const newRole = { ...payload, roleId }
      const data = yield call(update, newRole)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
  },
})
