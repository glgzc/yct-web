import modelExtend from 'dva-model-extend'
import { query, select, info, save, remove, update } from 'services/menu'
import { pageModel } from 'utils/model'

export default modelExtend(pageModel, {

  namespace: 'menu',
  state: {
    selectMenus: [],
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
  },
  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/sys/menu') {
          dispatch({ type: 'query',
            payload: {
              ...location.query,
            } })
        }else{
          dispatch({ type: 'clear'})
        }

      })
    },
  },

  effects: {
    * query ({
      payload,
    }, { call, put }) {
      const data = yield call(query, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
            pagination: {

            },
          },
        })
      } else {
        throw data
      }
    },
    * delete ({ payload }, { call, put, select }) {
      const menuId = payload.id
      const data = yield call(remove, { menuId })
      const { selectedRowKeys } = yield select(_ => _.menu)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: selectedRowKeys.filter(_ => _ !== payload) } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * multiDelete ({ payload }, { call, put }) {
      const data = yield call(remove, payload)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: [] } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * add ({ payload }, { call, put }) {
      const data = yield call(select, { page: 1, pageSize: 100 })
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            selectMenus: data.menuList,
            modalType: 'create',
          },
        })
      } else {
        throw data
      }
    },
    * edit ({ payload }, { call, put }) {
      const currentItem = yield call(info, payload)
      const data = yield call(select, { page: 1, pageSize: 100 })

      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            currentItem: currentItem.menu,
            selectMenus: data.menuList,
            modalType: 'update',
          },
        })
      } else {
        throw data
      }
    },
    * create ({ payload }, { call, put }) {
      const data = yield call(save, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update ({ payload }, { select, call, put }) {
      const menuId = yield select(({ menu }) => menu.currentItem.menuId)
      const newMenu = { ...payload, menuId }
      const data = yield call(update, newMenu)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
  },
})
