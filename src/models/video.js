/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import queryString from 'query-string'
// 注意别和函数名重名。。。
import { query, downloadService,remove,} from 'services/video'

import { pageModel } from 'utils/model'

const { prefix } = config

export default modelExtend(pageModel, {
  namespace: 'video',

  state: {
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
    treeData: [],
    treeCheckedKeys: [],
    isMotion: window.localStorage.getItem(`${prefix}userIsMotion`) === 'true',
    editingKey: '',
    treeSelectData: [],
    orgMenuList: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/app/video') {
          dispatch({
            type: 'query',
            payload:queryString.parse(location.search),
          })
        }
      })
    },
  },

  effects: {
    * query({ payload = {} }, { call, put }) {
      const data = yield call(query, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount,
            },
          },
        })
      }
    },
    * changePage({ payload }, { call, put }) {
      const data = yield call(query, {
        current: payload.page, size: payload.pageSize,
        // keywords: payload.keywords,
      })
      if (data) {
        yield put({
          type: 'editingKey',
          payload: {
            list: data.page.list,
            // start: payload.start,
            // end: payload.end,
            // keywords: payload.keywords,
            pagination: {
              page: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount,
            },
          },
        })
      }
    },
    * delete({ payload }, { call, put }) {
      const data = yield call(remove, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * multiDelete({ payload }, { call, put }) {
      const data = yield call(remove, payload)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: [] } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    

    * downLoad({ payload }, { call, put }) {
      const data = yield call(downloadService, payload)
      console.log(data,'data')
    },
    * create({ payload }, { call, put }) {
      payload = {
        data: JSON.stringify(payload)
      }
      const data = yield call(createRole, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * update({ payload }, { call, put }) {
      payload = {
        data: JSON.stringify(payload)
      }
      const data = yield call(updateAuth, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

  },

  reducers: {

    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },

    switchIsMotion(state) {
      window.localStorage.setItem(`${prefix}userIsMotion`, !state.isMotion)
      return { ...state, isMotion: !state.isMotion }
    },

    editingKey(state, { payload }) {
      return { ...state, ...payload }
    },
    updateOrgMenuListByOrg(state, { payload }) {
      const temp = state.orgMenuList.slice();
      for (let i in temp) {
        if (temp[i].organizationId === payload.organizationId) {
          temp[i] = payload;
          return { ...state, orgMenuList: temp };
        }
      }
      temp.push(payload);
      return { ...state, orgMenuList: temp }
    }
  },
})
