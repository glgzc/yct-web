/* global window */
import modelExtend from 'dva-model-extend'
import { message } from 'antd'
import { routerRedux } from 'dva/router'
import { config } from 'utils'
import { list, info } from 'services/org'
import {
  priceGroupSave,
  priceGroupRemove,
  chargeInterfaceList,
  chargeInterfaceSave,
  chargeInterfaceUpdate,
  chargeInterfaceRemove,
  priceSettingEnable,
  priceSettingDisable,
  priceSettingUpdate,
  add,
  getPartyUserList,
} from 'services/price'

import { pageModel } from 'utils/model'

const { prefix } = config

export default modelExtend(pageModel, {
  namespace: 'org',

  state: {
    currentItem: {},
    visibleDetails: false,
    currentInfo: {
      basicInfo: {},
      fileList: [],
      partyList: []
    },

    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
    treeCheckedKeys: [],
    isMotion: window.localStorage.getItem(`${prefix}userIsMotion`) === 'true',

    priceGroupList: [],
    modelVisibleGroup: false,
    interfaceList: [],
    visibleCreateApi: false,
    currentApiItem: {},
    currentMethod: '',
    showAddORG: false,
    naturePeople: [],
    showEdit: false,
    isNew: false,
    visibleDetailsModal: false,
    keywords:'',
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/app/org') {
          dispatch({
            type: 'query',
            payload: location.query,
          })
          dispatch({
            type: "getPartyUserListModels"
          });
        }
        if (location.pathname.indexOf('/app/org/form') >= 0) {
          dispatch({
            type: "getPartyUserListModels"
          });
        }

      })
    },
  },

  effects: {

    * query({ payload = {} }, { call, put }) {
      const data = yield call(list, payload)
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount
            },
          },
        })
      }
    },
    * changePage({ payload }, { call, put }) {
      const data = yield call(list, { page: payload.page, pageSize: payload.pageSize ,keywords:payload.keywords,})
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount
            },
          },
        })
      }
    },
    * queryKeyWords({ payload }, { call, put }) {
      const data = yield call(list, { ...payload })
      if (data) {
        yield put({
          type: 'currentMethod',
          payload: {
            list: data.page.list,
            keywords:payload.keywords,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount
            },
          },
        })
      }
    },
    * addTheORG({ payload }, { call, put }) {
      const data = yield call(add, payload)
      const dataList = yield call(list, payload)
      const { code, msg } = data
      if (code) {
        message.error(msg)
        return
      }
      yield put({
        type: 'showDetails',
        payload: {
          list: dataList.page.list,
          pagination: {
            current: 1,
            pageSize: 10,
            total: dataList.page.totalCount
          },
          isNew: true,
          showEdit: false,
          currentItem: {},
          visibleDetails: false,
          currentInfo: {
            appPartyUser: {},
            partyFile: {},
          }
        }
      })
    },
    // getPartyUserList
    * getPartyUserListModels({ payload = {} }, { call, put }) {
      const data = yield call(getPartyUserList, payload)
      yield put({
        type: 'priceGroupListSuccess',
        payload: {
          naturePeople: data.list,
        }
      })
    },
    * info({ payload }, { call, put }) {
      const data = yield call(info, { id: payload.currentItem.creditCode });
      if (data.success) {
        yield put({
          type: 'showDetails',
          payload: {
            currentItem: payload.currentItem,
            visibleDetails: payload.visibleDetails,
            currentInfo: {
              appPartyUser: data.appPartyOrganization,
              partyFile: data.partyFile,
            }
          }
        })
      } else {
        throw data
      }
    },
    * getEdit({ payload }, { call, put }) {
      const data = yield call(info, { id: payload.currentItem.creditCode });
      if (data.success) {
        yield put({
          type: 'showDetails',
          payload: {
            currentItem: payload.currentItem,
            showEdit: true,
            isNew: false,
            currentInfo: {
              appPartyUser: data.appPartyOrganization,
              partyFile: data.partyFile,
            }
          }
        })
      } else {
        throw data
      }
    },
    * newORG({ payload }, { call, put }) {
      yield put({
        type: 'showDetails',
        payload: {
          isNew: true,
          showEdit: true,
          currentItem: {},
          visibleDetails: false,
          currentInfo: {
            appPartyUser: {},
            partyFile: {},
          }
        }
      })
    },
    * backIndex({ payload }, { call, put }) {
      yield put({
        type: 'showDetails',
        payload: {
          isNew: true,
          showEdit: false,
          currentItem: {},
          visibleDetails: false,
          currentInfo: {
            appPartyUser: {},
            partyFile: {},
          }
        }
      })
    },

    * showPriceGroup({ payload }, { call, put }) {
      yield put({
        type: 'showModal',
        payload: {
          modalType: payload.modalType,
          modelVisibleGroup: true
        },
      })
    },

    * createPriceGroup({ payload }, { call, put }) {
      const data = yield call(priceGroupSave, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * removePriceGroup({ payload }, { call, put }) {
      const data = yield call(priceGroupRemove, { priceGroupId: payload.priceGroupId })
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * interfaceList({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceList)
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            interfaceList: data.interfaceList,
            modalType: payload.modalType
          },
        })
      } else {
        throw data
      }
    },

    * interfaceListNew({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceList)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            interfaceList: data.interfaceList
          },
        })
      } else {
        throw data
      }
    },

    //禁用
    * settingDisable({ payload }, { call, put }) {
      const data = yield call(priceSettingDisable, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    //启用
    * settingEnable({ payload }, { call, put }) {
      const data = yield call(priceSettingEnable, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * create({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceSave, payload)
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ type: 'hideCreateApi' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * deleteInterface({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceRemove, { interfaceIds: payload })
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ type: 'hideCreateApi' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * showUpdateApi({ payload }, { call, put }) {
      yield put({
        type: 'showCreateApi',
        payload: {
          currentItem: payload.currentItem ? payload.currentItem : {},
          currentApiItem: payload.currentApiItem ? payload.currentApiItem : {},
          modalType: payload.modalType
        }
      })
    },

    * updateInterface({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceUpdate, payload.data)
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({
          type: 'hideCreateApi',
          payload: {
            modalType: payload.modalType
          }
        })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update({ payload }, { call, put }) {
      const data = yield call(priceSettingUpdate, payload)
      if (data.success) {
        yield put({ type: 'query' })
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
  },

  reducers: {

    showTheAddORG(state, { payload }) {
      return { ...state, ...payload, showAddORG: true }
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false, modelVisibleGroup: false, visibleCreateApi: false }
    },

    switchIsMotion(state) {
      window.localStorage.setItem(`${prefix}userIsMotion`, !state.isMotion)
      return { ...state, isMotion: !state.isMotion }
    },

    priceGroupListSuccess(state, { payload }) {
      return { ...state, ...payload }
    },

    showCreateApi(state, { payload }) {
      return { ...state, ...payload, modalVisible: true, visibleCreateApi: true }
    },
    hideCreateApi(state, { payload }) {
      return { ...state, ...payload, modalVisible: true, visibleCreateApi: false }
    },

    currentMethod(state, { payload }) {
      return { ...state, ...payload }
    },

    //org method
    showDetails(state, { payload }) {
      return { ...state, ...payload }
    },
    hideDetails(state, { payload }) {
      return { ...state, ...payload }
    }
  },
})
