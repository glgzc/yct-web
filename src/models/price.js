/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import { 
  priceGroupList, 
  priceGroupPriceList,
  priceGroupSave,
  priceGroupRemove,
  chargeInterfaceList,
  chargeInterfaceSave,
  chargeInterfaceUpdate,
  chargeInterfaceRemove,
  priceSettingEnable,
  priceSettingDisable,
  priceSettingUpdate
} from 'services/price'

import { pageModel } from 'utils/model'

const { prefix } = config

export default modelExtend(pageModel, {
  namespace: 'price',

  state: {
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
    treeCheckedKeys: [],
    isMotion: window.localStorage.getItem(`${prefix}userIsMotion`) === 'true',

    priceGroupList: [],
    modelVisibleGroup: false,
    interfaceList: [],
    visibleCreateApi: false,
    currentApiItem: {},
    currentMethod: ''
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/app/price') {
          dispatch({
            type: 'queryPriceGroupList',
          })
          dispatch({
            type: 'query',
            payload: location.query,
          })
        }
      })
    },
  },

  effects: {

    * queryPriceGroupList ({ payload = {} }, { call, put }) {
      const data = yield call(priceGroupList)
      if (data) {
        yield put({
          type: 'priceGroupListSuccess',
          payload: {
            priceGroupList: data.priceGroupList
          },
        })
      }
    },

    * query ({ payload = {} }, { call, put }) {
      const data = yield call(priceGroupPriceList, payload)
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.priceList,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.priceList[0].priceSettingEntityList.length,
            },
          },
        })
      }
    },
    * changePage({ payload }, { call, put }) {
      const data = yield call(priceGroupPriceList, { page: payload.page, pageSize: payload.pageSize })
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.priceList,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.priceList[0].priceSettingEntityList.length,
            },
          },
        })
      }
    },
    * showPriceGroup ({ payload }, { call, put }) {
      yield put({
        type: 'showModal',
        payload: {
          modalType: payload.modalType,
          modelVisibleGroup: true
        },
      })
    },

    * createPriceGroup ({ payload }, { call, put }) {
      const data = yield call(priceGroupSave, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'queryPriceGroupList' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * removePriceGroup ({ payload }, { call, put }) {
      const data = yield call(priceGroupRemove, { priceGroupId:payload.priceGroupId })
      if (data.success) {
        yield put({ type: 'queryPriceGroupList' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * interfaceList ({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceList)
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            interfaceList:data.interfaceList,
            modalType: payload.modalType
          },
        })
      } else {
        throw data
      }
    },

    * interfaceListNew ({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceList)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            interfaceList:data.interfaceList
          },
        })
      } else {
        throw data
      }
    },

    //禁用
    * settingDisable ({ payload }, { call, put }) {
      const data = yield call(priceSettingDisable, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    //启用
    * settingEnable ({ payload }, { call, put }) {
      const data = yield call(priceSettingEnable, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * create ({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceSave, payload)
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ type: 'hideCreateApi' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * deleteInterface ({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceRemove, {interfaceIds: payload})
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ type: 'hideCreateApi' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * showUpdateApi ({ payload }, { call, put }) {
      yield put({ 
        type: 'showCreateApi',
        payload: {
          currentItem: payload.currentItem ?payload.currentItem : {},
          currentApiItem: payload.currentApiItem ? payload.currentApiItem : {},
          modalType: payload.modalType
        }
      })
    },

    * updateInterface ({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceUpdate, payload.data)
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ 
          type: 'hideCreateApi',
          payload: {
            modalType: payload.modalType
          }
        })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update ({ payload }, { call, put }) {
      const data = yield call(priceSettingUpdate, payload)
      if (data.success) {
        yield put({ type: 'query' })
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
  },

  reducers: {

    showModal (state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal (state) {
      return { ...state, modalVisible: false ,modelVisibleGroup: false, visibleCreateApi: false }
    },

    switchIsMotion (state) {
      window.localStorage.setItem(`${prefix}userIsMotion`, !state.isMotion)
      return { ...state, isMotion: !state.isMotion }
    },
    
    priceGroupListSuccess(state, {payload}) {
      return {...state, ...payload}
    },

    showCreateApi (state, { payload }) {
      return { ...state, ...payload, modalVisible: true, visibleCreateApi: true }
    },
    hideCreateApi (state, { payload }) {
      return { ...state, ...payload, modalVisible: true, visibleCreateApi: false }
    },

    currentMethod (state, {payload}) {
      return {...state, ...payload}
    },
  },
})
