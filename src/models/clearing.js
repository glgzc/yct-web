import modelExtend from 'dva-model-extend';
import { query,remove } from 'services/clearing';
import { pageModel } from 'utils/model';

export default modelExtend(pageModel, {
    namespace: 'clearing',
    state: {
        list: [],
        type: [],
        pageIndex: 1,
        total: 0,
        pageSize: 10,
        loading: false
    },
    reducers: {
        reloadList: (state, action) => {
            return { ...state, list: action.list }
        },
        push: (state, action) => {
            return { ...state, list: state.list.concat(action.data) };
        },
        setPageIndex: (state, action) => {
            return { ...state, pageIndex: action.index, total: action.total }
        },
        setTypeFilter: (state, action) => {
            return { ...state, type: action.typeFilter }
        },
        setLoading: (state, action) => {
            return { ...state, loading: action.loading }
        }
    },
    effects: {
        * load(action = {}, { call, put }) {
            yield put({
                type: 'setLoading',
                loading: true
            });
            const data = yield query(action.params);
            if (data.code === 0) {
                yield put({
                    type: 'setPageIndex',
                    index: data.page.currPage,
                    total: data.page.totalCount
                })
                yield put({
                    type: 'reloadList',
                    list: data.page.list,
                });
                if (data.interfaceTypeList.length > 0) {
                    // yield put({
                    //     type: 'setTypeFilter',
                    //     typeFilter: data.typeGroup.map( group => ({
                    //         id: group.priceGroupId,
                    //         name: group.title,
                    //         types: group.priceSettingEntityList
                    //     }))
                    // })
                } else if (data.typeGroup.length > 0) {
                    let types = [];
                    data.typeGroup.forEach(element => {
                        types = types.concat(element.priceSettingEntityList);
                    });
                    types = unique5(types);
                    yield put({
                        type: 'setTypeFilter',
                        typeFilter: types
                    })
                }
                yield put({
                    type: 'setLoading',
                    loading: false
                });
            }

        },
        * delete({ payload }, { call, put }) {
            const data = yield call(remove, payload);
            if(!data.code){
                yield put({ type: 'load',params: payload })
            }
        },
    },
    subscriptions: {
        setup({ dispatch, history }) {
            history.listen((location) => {
                if (location.pathname === '/app/clearing') {
                    dispatch({
                        type: 'load'
                    })
                }
            })
        },
    },
})


function unique5(array) {
    var r = [];
    for (var i = 0, l = array.length; i < l; i++) {
        for (var j = i + 1; j < l; j++)
            if (array[i].interfaceId === array[j].interfaceId) j = ++i;
        r.push(array[i]);
    }
    return r;
}