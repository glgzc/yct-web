import modelExtend from 'dva-model-extend'
import { parse } from 'qs'
import { pageModel } from 'utils/model'
import { myCity, queryWeather, query } from 'services/dashboard'


export default modelExtend(pageModel, {
  namespace: 'dashboard',
  state: {
    numbers: [],
    sumList:[],
    countList:[],
  },
  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/dashboard') {
          dispatch({
            type: 'query',
            payload: location.query,
          })
        }
      })
    },
  },
  effects: {
    * query ({
      payload,
    }, { call, put }) {
      //const data = yield call(query, parse(payload))
      const numbers=[
          {icon: "api", color: "#f50", title: "接口调用次数", number: 2781},
          {icon: "team", color: "#8fc9fb", title: "用户", number: 3241},
          {icon: "pay-circle", color: "#d897eb", title: "余额", number: 253},
        ]
      yield put({ 
        type: 'updateState', 
        payload: { 
          numbers:numbers,
        } 
      })
    },
  },
  reducers: {
  },
})
