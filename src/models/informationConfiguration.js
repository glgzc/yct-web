import { request } from 'utils';
import {isAdmin} from 'utils/util';

export default {
    namespace: 'infoConfig',
    state: {
        dataLoading: false,
        editLoading: false,
        organization: [],
        data: [],
        modalVisiable: false
    },
    reducers: {
        // data: {}
        setList(state, { list }) {
            return { ...state, data: list };
        },
        setOrgList(state, { list }) {
            return { ...state, organization: list };
        },
        delete(state, { id }) {
            return { ...state, data: state.data.filter(item => item.id !== id) };
        },
        save(state, { item }) {
            let flag = 0;
            const temp = state.data.map(temp => {
                if (temp.id === item.id) {
                    flag ++;
                    return item;
                } else {
                    return temp;
                }
            })
            if(!flag) temp.unshift(item);
            return {
                ...state, data:temp
            }
        },
        setDataLoading(state, {loading}) {
            return {...state, dataLoading: loading};
        },
        setModalVisiable(state, {visiable}) {
            return {...state, modalVisiable: visiable}
        }
    },
    effects: {
        // * queryNotary
        * queryOrgList({ }, { put }) {
            const res = yield request({
                url: "/sys/organization/listTree",
                method: 'post'
            })
            if(res.code === 0) {
                yield put({
                    type: 'setOrgList',
                    list: res.data
                })
            }
            
        },
        * queryNotaryList({ payload }, { put }) {
            yield put({
                type: 'setDataLoading',
                loading: true
            })
            const res = yield request({
                url: "/business/appBasicinfoTag/list",
                method: 'post',
                data: payload,
            });
            yield put({
                type: 'setDataLoading',
                loading: false
            })
            if (res.code === 0) {
                yield put({
                    type: 'setList',
                    list: res.list
                })
                
            }
        },
        * queryRemarksList({ payload }, { put }) {
            yield put({
                type: 'setDataLoading',
                loading: true
            })
            const res = yield request({
                url: "/business/appRemarksConfigure/list",
                method: 'post',
                data: payload,
            });
            yield put({
                type: 'setDataLoading',
                loading: false
            })
            if (res.code === 0) {
                yield put({
                    type: 'setList',
                    list: res.list
                });
            }
        },
        * deleteNotary({ id }, { put }) {
            const res = yield request({
                url: '/business/appBasicinfoTag/del',
                method: 'post',
                data: {
                    id: id
                }
            });
            if (res.code === 0) {
                yield put({
                    type: 'delete',
                    id: id
                });
            }
        },
        * deleteRemarks({ id }, { put }) {
            const res = yield request({
                url: '/business/appRemarksConfigure/del',
                method: 'post',
                data: {
                    id: id
                }
            });
            if (res.code === 0) {
                yield put({
                    type: 'delete',
                    id: id
                });
            }
        },
        * setNotaryItem({item}, {put}){
            const res = yield request({
                url: '/business/appBasicinfoTag/save',
                method: 'post',
                data: item
            });
            if(res.code === 0) {
                return yield put({
                    type: 'save',
                    item: res.BasicinfoTag
                })
            }
        },
        * setRemarksItem({item}, {put}) {
            const res = yield request({
                url: '/business/appRemarksConfigure/save',
                method: 'post',
                data: item
            });
            if(res.code === 0) {
                return yield put({
                    type: 'save',
                    item: res.RemarksConfigure
                })
            }
        }

    },
    subscriptions: {
        setup({ dispatch, history }) {
            history.listen((location) => {
                if (location.pathname === '/informationConfiguration/notary') {
                    dispatch({
                        type: 'queryNotaryList'
                    })
                } else if (location.pathname === '/informationConfiguration/remarks') {
                    dispatch({
                        type: 'queryRemarksList'
                    })
                }
            })
                dispatch({
                    type: 'queryOrgList'
                })
        },
    },


}