/* global window */
import modelExtend from 'dva-model-extend'
import { message } from 'antd'
import { routerRedux } from 'dva/router'
import { config } from 'utils'
import { list, info, add,deleteImg,infoNew} from 'services/client'
import {
  priceGroupSave,
  priceGroupRemove,
  chargeInterfaceList,
  chargeInterfaceSave,
  chargeInterfaceUpdate,
  chargeInterfaceRemove,
  priceSettingEnable,
  priceSettingDisable,
  priceSettingUpdate
} from 'services/price'

import { pageModel } from 'utils/model'

const { prefix } = config

export default modelExtend(pageModel, {
  namespace: 'client',

  state: {
    currentItem: {},
    visibleDetails: false,
    currentInfo: {
      basicInfo: {},
      fileList: [],
      partyList: []
    },

    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
    treeCheckedKeys: [],
    isMotion: window.localStorage.getItem(`${prefix}userIsMotion`) === 'true',

    priceGroupList: [],
    modelVisibleGroup: false,
    interfaceList: [],
    visibleCreateApi: false,
    currentApiItem: {},
    currentMethod: '',
    showAddPerson: false,
    showEdit: false,
    isNew: false,
    keywords: '',
    risk:'',
  },

  subscriptions: {

    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/app/client') {
          dispatch({
            type: 'query',
            payload: location.query,
          })
        }
      })
    },
  },

  effects: {

    * query({ payload = {} }, { call, put }) {
      const data = yield call(list, payload)
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount
            },
          },
        })
      }
    },
    * changePage({ payload }, { call, put }) {
      const data = yield call(list, {page:payload.page,pageSize:payload.pageSize,keywords:payload.keywords,risk:payload.risk})
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount
            },
          },
        })
      }
    },
    * queryKeyWords({ payload }, { call, put }) {
      const data = yield call(list, { ...payload })
      if (data) {
        yield put({
          type: 'currentMethod',
          payload: {
            keywords:payload.keywords,
            risk:payload.risk,
            list: data.page.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.page.totalCount
            },
          },
        })
      }
    },
    * info({ payload }, { call, put }) {
      const data = yield call(info, { id: payload.currentItem.idCard });
      if (data.success) {
        yield put({
          type: 'showDetails',
          payload: {
            currentItem: payload.currentItem,
            visibleDetails: payload.visibleDetails,
            currentInfo: {
              appPartyUser: data.appPartyUser,
              comparisonRecordList: [],
              partyFile: data.partyFile,
            }
          }
        })
      } else {
        throw data
      }
    },
    * getEdit({ payload }, { call, put }) {
      const data = yield call(infoNew, { idCard: payload.currentItem.idCard });
      if (data.success) {
        yield put({
          type: 'showDetails',
          payload: {
            currentItem: payload.currentItem,
            showEdit: true,
            isNew:false,
            currentInfo: {
              appPartyUser: data.appPartyUser,
              comparisonRecordList: data.comparisonRecordList,
              partyFile: data.partyFile,
            }
          }
        })
      } else {
        throw data
      }
    },
    * newNaturalPerson({ payload }, { call, put }) {
        yield put({
          type: 'showDetails',
          payload: {
            isNew:true,
            showEdit: true,
            currentInfo: {
              appPartyUser:{},
              comparisonRecordList:[],
              partyFile: [],
            }
          }
        })
    },
    // deleteImg
    * deleteImgModels({ payload }, { call, put }) {
      const data = yield call(deleteImg, payload);
  },
    * addpartyUser({ payload }, { call, put }) {
      const data = yield call(add, payload.data);
      if (payload.deleteString !='') {
        const datas = yield call(deleteImg, {fileId:payload.deleteString})
      }
      const dataList = yield call(list, {})
      const { code, msg } = data
      if (code) {
        message.error(msg)
        return
      }
      yield put({
        type: 'showDetails',
        payload: {
          list: dataList.page.list,
          pagination: {
            current: Number(payload.page) || 1,
            pageSize: Number(payload.pageSize) || 10,
            total: dataList.page.totalCount
          },
          isNew:true,
          showEdit: false,
          currentInfo: {
            appPartyUser:{},
            comparisonRecordList:[],
            partyFile: [],
          }
        }
      })
    },

    * showPriceGroup({ payload }, { call, put }) {
      yield put({
        type: 'showModal',
        payload: {
          modalType: payload.modalType,
          modelVisibleGroup: true
        },
      })
    },

    * createPriceGroup({ payload }, { call, put }) {
      const data = yield call(priceGroupSave, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * removePriceGroup({ payload }, { call, put }) {
      const data = yield call(priceGroupRemove, { priceGroupId: payload.priceGroupId })
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * interfaceList({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceList)
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            interfaceList: data.interfaceList,
            modalType: payload.modalType
          },
        })
      } else {
        throw data
      }
    },

    * interfaceListNew({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceList)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            interfaceList: data.interfaceList
          },
        })
      } else {
        throw data
      }
    },

    //禁用
    * settingDisable({ payload }, { call, put }) {
      const data = yield call(priceSettingDisable, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    //启用
    * settingEnable({ payload }, { call, put }) {
      const data = yield call(priceSettingEnable, payload)
      if (data.success) {
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * create({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceSave, payload)
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ type: 'hideCreateApi' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * deleteInterface({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceRemove, { interfaceIds: payload })
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({ type: 'hideCreateApi' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * showUpdateApi({ payload }, { call, put }) {
      yield put({
        type: 'showCreateApi',
        payload: {
          currentItem: payload.currentItem ? payload.currentItem : {},
          currentApiItem: payload.currentApiItem ? payload.currentApiItem : {},
          modalType: payload.modalType
        }
      })
    },

    * updateInterface({ payload }, { call, put }) {
      const data = yield call(chargeInterfaceUpdate, payload.data)
      if (data.success) {
        yield put({ type: 'interfaceListNew' })
        yield put({
          type: 'hideCreateApi',
          payload: {
            modalType: payload.modalType
          }
        })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update({ payload }, { call, put }) {
      const data = yield call(priceSettingUpdate, payload)
      if (data.success) {
        yield put({ type: 'query' })
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
  },

  reducers: {

    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false, modelVisibleGroup: false, visibleCreateApi: false }
    },

    switchIsMotion(state) {
      window.localStorage.setItem(`${prefix}userIsMotion`, !state.isMotion)
      return { ...state, isMotion: !state.isMotion }
    },

    priceGroupListSuccess(state, { payload }) {
      return { ...state, ...payload }
    },

    showCreateApi(state, { payload }) {
      return { ...state, ...payload, modalVisible: true, visibleCreateApi: true }
    },
    hideCreateApi(state, { payload }) {
      return { ...state, ...payload, modalVisible: true, visibleCreateApi: false }
    },

    currentMethod(state, { payload }) {
      return { ...state, ...payload }
    },

    //client method
    showDetails(state, { payload }) {
      return { ...state, ...payload }
    },
    hideDetails(state, { payload }) {
      return { ...state, ...payload }
    },
  },
})
