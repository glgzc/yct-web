import modelExtend from 'dva-model-extend'
import { query, info, save, remove, update } from 'services/manager'
import { query as roleSelect } from 'services/role'
import { pageModel } from 'utils/model'

export default modelExtend(pageModel, {

  namespace: 'manager',
  state: {
    roles: [],
    userRoles: [],
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
  },
  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/sys/manager') {
          dispatch({ type: 'query',
            payload: {
              ...location.query,
            } })
        }else{
          dispatch({ type: 'clear'})
        }
      })
    },
  },

  effects: {
    * query ({
      payload={},
    }, { call, put }) {
      payload.type=0
      const data = yield call(query, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.page.list,
            pagination: {
              current: Number(data.page.current) || 1,
              pageSize: Number(data.page.pageSize) || 10,
              total: data.page.totalCount,
            },
          },
        })
      } else {
        throw data
      }
    },
    * delete ({ payload }, { call, put, select }) {
      const userIds = [payload.id]

      const data = yield call(remove, { userIds })
      const { selectedRowKeys } = yield select(_ => _.manager)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: selectedRowKeys.filter(_ => _ !== payload) } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * multiDelete ({ payload }, { call, put }) {
      const data = yield call(remove, payload)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: [] } })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
    * add ({ payload }, { call, put }) {
      const data = yield call(roleSelect, { page: 1, pageSize: 100 })
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            roles: data.page.list,
            userRoles: [],
            modalType: 'create',
          },
        })
      } else {
        throw data
      }
    },
    * edit ({ payload }, { call, put }) {
      const currentItem = yield call(info, payload)
      const data = yield call(roleSelect, { page: 1, pageSize: 100 })
      const user = currentItem.user
      const userRoles = []
      if (user.roleIdList != undefined && user.roleIdList != null) {
        user.roleIdList.map((v) => {
          userRoles.push(String(v))
        })
      }
      if (data.success) {
        yield put({
          type: 'showModal',
          payload: {
            currentItem: user,
            userRoles,
            roles: data.page.list,
            modalType: 'update',
          },
        })
      } else {
        throw data
      }
    },
    * create ({ payload }, { call, put }) {
      const data = yield call(save, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },

    * update ({ payload }, { select, call, put }) {
      const userId = yield select(({ manager }) => manager.currentItem.userId)
      const newManager = { ...payload, userId }
      const data = yield call(update, newManager)
      if (data.success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query' })
      } else {
        throw data
      }
    },
  },
})
