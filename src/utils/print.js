var printAreaCount = 0;
export function printArea (dom) {
    var idPrefix = "printArea_";
    removePrintArea(idPrefix + printAreaCount);
    printAreaCount++;
    // var iframeId = idPrefix + printAreaCount;
    var iframeStyle = 'position:absolute;width:0px;height:0px;left:-500px;top:-500px;';
    iframe = document.createElement('IFRAME');
    iframe.setAttribute({
        style: iframeStyle,
        display: 'none'
    });
    document.body.appendChild(iframe);
    var doc = iframe.contentWindow.document;
    $(document).find("link").filter(function () {
        return $(this).attr("rel").toLowerCase() == "stylesheet";
    }).each(
            function () {
                doc.write('<link type="text/css" rel="stylesheet" href="'
                        + $(this).attr("href") + '" >');
            });
    doc.write('<div class="' + $(ele).attr("class") + '">' + $(ele).html()
            + '</div>');
    doc.close();
    var frameWindow = iframe.contentWindow;
    frameWindow.close();
    frameWindow.focus();
    frameWindow.print();
}
var removePrintArea = function (id) {
    $("iframe#" + id).remove();
};