//测试
//const HOST = process.env.NODE_ENV === 'development' ? 'http://47.108.87.76:8080/easy_to_adopt_backend/' : 'http://47.108.87.76:8080/easy_to_adopt_backend/' //测试/
//const fileSourcePro = HOST + '/easy_to_adopt_backend/appCollectionFile/' //测试
//const fileSourceDev = HOST + 'appCollectionFile/'//测试

// 国力
//const HOST = process.env.NODE_ENV === 'development' ? 'http://47.108.87.76:8080/easy_to_adopt_backend/' : 'http://47.112.192.151:8080/easy_to_adopt_backend/'; //国力生产
//const fileSourcePro='http://47.112.192.151:8082/' //国力生产
const HOST='http://47.108.87.76:8080/easy_to_adopt_backend/'
const fileSourcePro = HOST + '/appCollectionFile/'

//后端本地
// const fileSourceDev=HOST //后端本地
// const HOST = process.env.NODE_ENV === 'development' ? 'http://192.168.2.111:8080' : 'http://192.168.2.111:8080' //后端本地

// const fileSourceDev = HOST + '/easy_to_adopt_backend/appCollectionFile/'//测试1

// 德阳生产
// const HOST = process.env.NODE_ENV === 'development' ? 'http://47.107.51.211:8080/easy_to_adopt_backend/' : 'http://47.107.51.211:8080/easy_to_adopt_backend/'; //德阳生产
// const fileSourcePro='http://47.107.51.211:8082/' //德阳生产
module.exports = {
  name: '易采通',
  prefix: 'antdAdmin',
  footerText: '易采通  © 2021  V3.1.0',
  logo: '/logo.png',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',

  // //测试
  //fileSource: process.env.NODE_ENV === 'development' ? fileSourceDev : fileSourceDev, //测试
  //playFileSourceDev: HOST, //测试
  //showVideotURL: "http://47.108.87.76:8080/easy_to_adopt_backend/", //测试
  //faceHost: 'http://47.108.87.76:8080' + '/easy_to_adopt_app_backend',// 测试

  //国力
   playFileSourceDev: "http://47.112.192.151:8080/easy_to_adopt_backend/", //国力生产
   fileSource: process.env.NODE_ENV === 'development' ? fileSourcePro : fileSourcePro,//国力生产
   showVideotURL: "http://47.112.192.151:8080/easy_to_adopt_backend/",//国力生产
   faceHost: 'http://47.112.192.151:8080' + '/easy_to_adopt_app_backend', //国力生产

  // showVideotURL: "http://47.108.87.76:8080", //本地

  // 德阳生产
  // playFileSourceDev: "http://47.107.51.211:8080/easy_to_adopt_backend/", //德阳生产
  //  fileSource: process.env.NODE_ENV === 'development' ? fileSourcePro : fileSourcePro,//德阳生产
  // showVideotURL: "http://47.107.51.211:8080/easy_to_adopt_backend/",//德阳生产
  //   faceHost:'http://47.107.51.211:8080'+'/easy_to_adopt_app_backend', //德阳生产
  host: HOST,//本地 测试  所有生产 
  YQL: ['http://www.zuimeitianqi.com'],
  CORS: [HOST],
  openPages: ['/login'],
  apiPrefix: '/api/v1',
  api: {
    //client
    clientList: '/business/partyUser/list',
    clientInfo: "/business/partyUser/info",
    //org
    orgList: "/business/partyOrganization/list",
    orgInfo: "/business/partyOrganization/info",
    orgOCR: "/businesslicense/orgOCR",
    //auth
    userLogin: '/sys/user/signin',
    userLogout: '/sys/logout',
    userState: '/sys/state',
    refreshToken: '/sys/user/refreshToken',
    //user
    users: '/sys/user/list',
    userInfo: '/sys/user/info',
    userRemove: '/sys/user/delete',
    userSave: '/sys/user/save',
    userUpdate: '/sys/user/update',
    userRole: '/sys/role/select',
    // log
    logs: '/sys/log/list',

    // menu
    nav: '/sys/menu/nav',
    menus: '/sys/menu/list',
    menuSelect: '/sys/menu/select',
    menuInfo: '/sys/menu/info',
    menuSave: '/sys/menu/save',
    menuUpdate: '/sys/menu/update',
    menuRemove: '/sys/menu/delete',

    // role
    roles: '/sys/role/list',
    roleInfo: '/sys/role/info',
    roleRemove: '/sys/role/delete',
    roleSave: '/sys/role/save',
    roleUpdate: '/sys/role/update',
    roleListByOrganization: '/sys/role/listByOrganization',

    // roleListByOrganization: '/sys/organization/roles',

    // manager
    managers: '/sys/user/list',
    managerInfo: '/sys/user/info',
    managerRemove: '/sys/user/delete',
    managerSave: '/sys/user/save',
    managerUpdate: '/sys/user/update',

    // organization
    organizations: '/sys/organization/list',
    organizationSave: '/sys/organization/save',
    organizationUpdate: '/sys/organization/update',
    organizationRemove: '/sys/organization/delete',
    organizationInfo: '/sys/organization/info',
    // organizationSelect: '/sys/organization/select',
    organizationSelect: '/sys/organization/allList',


    organizationAllList: '/sys/organization/allList',
    organizationPriceGroup: '/business/organization_pricegroup/setup',
    organizationRecharge: '/business/recharge_record/recharge',
    organizationRechargeList: '/business/recharge_record/list',
    interfaceCallRecordList: '/business/interface_call_record/recordList',
    organizationRecordList: '/business/interface_call_record/recordList',

    //price
    groupList: '/business/price_group/list',  //获取价格组列表
    groupPriceList: '/business/price_group/priceList',  //获取价格组列表及价格配置信息
    groupSave: '/business/price_group/save',  //添加价格组信息
    groupRemove: '/business/price_group/remove',  //移除价格组
    interfaceList: '/business/charge_intergace/list',  //获取收费接口列表
    interfaceSave: '/business/charge_intergace/save',  //添加收费接口信息
    interfaceUpdate: '/business/charge_intergace/update',  //修改收费接口信息
    interfaceRemove: '/business/charge_intergace/remove',  //移除收费接口信息
    settingEnable: '/business/price_setting/enable',  //启动价格组对应的收费接口
    settingDisable: '/business/price_setting/disable',  //关闭价格组对应的收费接口
    settingUpdate: '/business/price_setting/update', //修改价格组对应的收费接口

    //clearing
    comparisonList: '/business/appComparisonRecord/list',
    clearingDelete:'/business/appComparisonRecord/batchDelete',
    //acceptance
    acceptanceList: '/business/appCollectionMatters/list', //获取待受理数据列表
    acceptanceInfo: '/business/appCollectionMatters/info', //获取待受理数据详情,

    //file
    fileDownload: '/appCollectionFile', //图片、文件
    fileUpload: '/appCollectionFile/upload',
    fileDeleteURL: "/appCollectionFile/delFile",//删除文件
    //standingBook
    standingBookList: '/business/matterReport/list', //获取台账列表
    // 添加当事人自然人
    addpartyUser: '/business/partyUser/add',
    // 添加当事人机构
    partyOrganization: '/business/partyOrganization/add',
    // 获取自然人
    partyUserList: '/business/partyUser/userlist',
    //人证比对
    faceCompare: '/faceplusplus/compare',
    //公安比对
    faceIDCompare: '/faceID/compare',
    //身份证ocr
    cardOCR: '/business/faceID/ocrByFielId',
    // 获取批次列表，及批次里面包含的事项
    brtechList: "/business/appCollection/brtechList",
    // 获取为分页的所有机构列表
    allMORGList: "/business/partyOrganization/orglist",
    // 新增修改批次
    appCollectionAddAndEdit: '/business/appCollection/add',
    // 备注列表
    memoTagListURL: '/business/appRemarksConfigure/list',
    //预设信息
    collectionBasicInfoSave: "/business/collectionBasicInfo/save",
    collectionBasicInfoUpdate: "/business/collectionBasicInfo/update",
    collectionBasicInfo: "/business/collectionBasicInfo/info",
    collectionBasicInfoList: "/business/collectionBasicInfo/list",
    tagListURL: "business/appBasicinfoTag/list",
    addDefaultItemsURL: "business/collectionBasicInfo/save",
    // 台账
    matterReportURL: '/business/matterReport/download',
    matterReportUpload: '/business/matterReport/upload',
    // 部门管理
    getDepartmentList: '/sys/department/list',
    saveOrUpdateDepartment: '/sys/department/saveOrUpdate',
    deleteDepartment: '/sys/department/del',
    // 职级管理
    getRankList: '/sys/position/list',
    saveOrUpdateRank: '/sys/position/saveOrUpdate',
    deleteRank: '/sys/position/del',
    // 多人视频录像
    getVideoList: '/business/appCollection/videoList',
    downVideoList: '/business/appCollection/downloadVideo',
    deleteVideo: '/business/appCollection/delVideo',
    // 获取批次事项详情
    getMatterDetailsListURL: "/business/appCollection/betchInfo",
    // 公证书模版详情查看
    getWordInfoURL: '/business/word/getWordInfo',
    // 保存公证书模版内容
    saveWord: '/business/word/save',
    // 下载公证书模版
    downloadWordURL: '/business/word/downloadWord',
    // 自然人编辑获取数据
    editPeople: "/business/partyUser/basicInfo",
    // 修改密码
    changePassWordURL: '/sys/user/password',
    // 获取推送记录列表
    getPushDataURL: '/business/batchPushRecord/listPage',
    //手段推送接口
    pushBatchDataURL:'/business/batchPushRecord/pushBatchData',
  },
}
