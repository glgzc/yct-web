import { message } from 'antd'
import { routerRedux } from 'dva/router'
import queryString from 'query-string'

export default {
  onError (e,dispatch) {
     
    e.preventDefault()

    if(!e.message){
        return
    }
    console.log(e)
    try{
        const json=JSON.parse(e.message)
        
        if(json.code==401||json.code==403){
            //清除token
            sessionStorage.removeItem("token")
            dispatch(routerRedux.push({
            pathname: '/login',
            search: queryString.stringify({
                from: '/',
            }),
            }));
        }else{
            message.error(json.msg)
        }

    }catch(err){
        console.log(e.message)
    }
    
  },
}