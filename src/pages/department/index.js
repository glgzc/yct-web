import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'

const Department = ({ location, dispatch, department, loading }) => {

  const { list, pagination, currentItem, modalVisible, modalType, isMotion, selectedRowKeys, treeSelectData } = department
  const { pageSize } = pagination
  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['department/update'],
    title: `${modalType === 'create' ? '添加部门' : '编辑部门信息'}`,
    wrapClassName: 'vertical-center-modal',
    treeSelectData,
    onOk (data) {
      dispatch({
        type:"department/create",
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'department/hideModal',
      })
    },
    onChange(data) {
      dispatch ({
        type: 'department/roleList',
        payload: data
      })
    }
  }

  const listProps = {
    pagination:false,
    dataSource: list,
    loading: loading.effects['department/query'],
    location,
    isMotion,
    onChange (page) {
      const { query, pathname } = location
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }))
    },
    onDeleteItem (id) {
      dispatch({
        type: 'department/delete',
        payload: id,
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'department/modal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'department/queryKeyWords',
        payload: {
          keywords: value.keywords,
        },
      })
    },
    onSearch (fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/department',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/department',
      }))
    },
    onAdd () {
      dispatch({
        type: 'department/modal',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion () {
      dispatch({ type: 'department/switchIsMotion' })
    },
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'department/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }

  return (
    <div className="content-inner">
      <Filter {...filterProps} />
      {
        selectedRowKeys.length > 0 &&
        <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
          <Col>
            {`Selected ${selectedRowKeys.length} items `}
            <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
              <Button type="primary" style={{ marginLeft: 8 }}>Remove</Button>
            </Popconfirm>
          </Col>
        </Row>
      }
      <List {...listProps} />
      {modalVisible && <Modal {...modalProps} />}
    </div>
  )
}

export default connect(({ department, loading }) => ({ department, loading }))(Department)
