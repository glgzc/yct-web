import React from 'react';
import { connect } from 'dva';
import InformationConfiguration from './index';
import { Modal } from 'antd';
class ComplementaryRemarks extends InformationConfiguration {
    constructor(props) {
        super(props);
        this.state = {
            dataType: '补充资料',
        }
    }
    handelModalSubmit() {
        this.refs.configForm.validateFields((errors, value) => {
            if (errors) return;
            if (!(value.scope && value.scope.length)) {
                delete value.scope;
            } else {
                value.organizationId = Number(value.scope[0])
                delete value.scope;
            }
            value.remarksContents = value.name;
            delete value.name;
            if (this.state.modalTitle === '编辑') {
                value.id = this.state.modalContentTarget.id;
            }
            this.props.dispatch({
                type: 'infoConfig/setRemarksItem',
                item: value
            }).then(res => {
                this.setState({
                    modalVisiable: false,
                })
            });

        });
    }
    handleSearch(keywords) {
        this.props.dispatch({
            type: 'infoConfig/queryRemarksList',
            payload: {
                keywords: keywords
            }
        })
    }
    handleDeleteItem = (id) => {
        Modal.confirm({
            content: '确定删除该项？',
            okText: '删除',
            onOk: () => {
                this.props.dispatch({
                    type: 'infoConfig/deleteRemarks',
                    id: id
                })
            }
        })
    }

}

export default connect(({ infoConfig }) => ({ infoConfig }))(ComplementaryRemarks);