import React from 'react';
import {Row, Col, Button, Input} from 'antd';
const styles={
    layout: {
        marginBottom: 12,
        paddingRight: 12
    },
    selectWidth: {
        width: '100%'
    }
}
export default class InfoConfigFilter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            keywords: ''
        }
    }
    handleInputChange = (e) => {
        this.setState({
            keywords: e.target.value
        })
    }
    handleSearch = () => {
        this.props.onSearch(this.state.keywords);
    }
    handleAddMatter = () => {
        this.props.onAdd();
    }
    render() {
        return <div style={styles.layout}>
            <Row gutter={24} type="flex" justify="space-between">
                    <Col md={6}>
                        <Input style={styles.selectWidth} placeholder="关键词" onChange={this.handleInputChange}></Input>
                    </Col>
                    <Col md={2}>
                        <Button type="primary" onClick={this.handleSearch}>查询</Button>
                    </Col>
                    <Col md={2} offset={14}>
                        <Button type="ghost" icon="plus" onClick={this.handleAddMatter}>添加</Button>
                    </Col>
                </Row>
        </div>
    }
}
