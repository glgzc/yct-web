import React from 'react';
import { Form, Input, Tree } from 'antd';
import {isAdmin} from 'utils/util';
const { TreeNode } = Tree;

const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
};
class ConfigForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expandedKeys: [],
            selectedKeys: []
        }
        
    }
    componentDidMount(){
        if(isAdmin()) {
            this.props.form.setFieldsValue({ scope: this.props.organization });
        }
    }
    onExpand = (expandedKeys) => {
        this.setState({
            expandedKeys,
            autoExpandParent: false,
        });
    }

    onSelect = (selectedKeys, info) => {
        this.setState({ selectedKeys });
        this.props.form.setFieldsValue({ scope: selectedKeys });
    }
    renderTreeNodes = data => data.map((item) => {
        if (item.children) {
            return (
                <TreeNode title={item.name} key={item.id} dataRef={item}>
                    {this.renderTreeNodes(item.children)}
                </TreeNode>
            );
        }
        return <TreeNode title={item.name} key={item.id} />;
    })

    render() {
        const { dataType, editContent } = this.props;
        const { getFieldDecorator } = this.props.form;
        const nameLabel = dataType === '公证预设信息' ? "其他信息标签名称" : "备注信息";
        const text = editContent ? (dataType === '公证预设信息' ? editContent.tagName : editContent.remarksContents) : '';
        return (
            <Form>
                <Form.Item {...formItemLayout} label={nameLabel}>
                    {getFieldDecorator('name', {
                        initialValue: text,
                        rules: [{ required: true, message: '必填项' },
                        { max: 6, message: '仅限6个字符' }],
                    })(
                        <Input placeholder={`请输入${nameLabel}`} />
                    )}
                </Form.Item>
                {
                    isAdmin() && <Form.Item {...formItemLayout} label="机构配置">
                        {getFieldDecorator('scope')(
                            <Tree
                                showLine
                                multiple={false}
                                onExpand={this.onExpand}
                                expandedKeys={this.state.expandedKeys}
                                defaultExpandParent
                                onSelect={this.onSelect}
                            >
                                {this.renderTreeNodes(this.props.organization)}
                            </Tree>
                        )}
                    </Form.Item>
                }

            </Form>
        )
    }
}

export default Form.create()(ConfigForm);