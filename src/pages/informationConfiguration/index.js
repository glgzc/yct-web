import React from 'react';
import moment from 'moment';
import InfoConfigFilter from './Filter';
import ConfigConfirm from './ConfigForm';
import { Table, Tag, Modal } from 'antd';
import './index.less';

const { Column } = Table;

class InformationConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataType: '公证预设信息',
            modalVisiable: false,
            modalTitle: '新增',
            modalContentTarget: {}
        }

        this.handleModalEdit = this.handleModalEdit.bind(this);
        this.handleModalAdd = this.handleModalAdd.bind(this);
        this.handelModalSubmit = this.handelModalSubmit.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }
    handleModalEdit(item) {
        this.setState({
            modalTitle: '编辑',
            modalContentTarget: item,
            modalVisiable: true
        })
    }
    handleModalAdd() {
        this.setState({
            modalTitle: '新增',
            modalVisiable: true
        })
    }
    handleModalClose() {
        this.setState({
            modalVisiable: false
        })

    }
    handelModalSubmit() {
        // 执行操作
    }
    handleSearch(keywords) {
        console.log(keywords);
        // 关键字搜索
    }
    handleDeleteItem = (id) => {
        console.log(id);
        // 删除该项
    }
    onChange = (page) => {
        console.log(page)
        // dispatch({
        //   type: 'log/changePage',
        //   payload: {
        //     page: page.current,
        //     pageSize: page.pageSize,
        //   }
        // })
      }
    render() {
        
        const { data, organization, dataLoading } = this.props.infoConfig;
        const { dataType, modalTitle,modalVisiable,  modalContentTarget } = this.state;
        const name = this.state.dataType === '公证预设信息' ? 'tagName' : 'remarksContents';
        return (
            <div className="content-inner" >
                <InfoConfigFilter dataType={dataType} onAdd={this.handleModalAdd} onSearch={this.handleSearch} />
                <div>
                    <Table
                        dataSource={data}
                        rowKey={row => row.id}
                        bordered
                        loading={dataLoading}
                        // onChange= {this.onChange}
                    >
                        <Column
                            title="创建时间"
                            dataIndex="createdTime"
                            key="createdTime"
                            width="20%"
                            render={value => moment(value).format("YYYY-MM-DD HH:mm")}
                        />
                        <Column
                            title="补充资料备注名称"
                            dataIndex={name}
                            key={name}
                        />
                        <Column
                            title="可见机构"
                            key="organization"
                            render={item => item.organizationName? <Tag>{item.organizationName}</Tag>: "无"}
                        />
                        <Column
                            title="操作"
                            key="operation"
                            render={item => <span><a onClick={this.handleModalEdit.bind(this, item)}>编辑</a>  |  <a onClick={this.handleDeleteItem.bind(this, item.id)}>删除</a></span>}
                        />
                    </Table>
                </div>
                <Modal
                    title={modalTitle}
                    visible={modalVisiable}
                    onOk={this.handelModalSubmit}
                    onCancel={this.handleModalClose}
                >
                    <ConfigConfirm ref="configForm" dataType={dataType} organization={organization} editContent={modalContentTarget} />
                </Modal>
            </div>
        )
    }
}

export default InformationConfiguration;