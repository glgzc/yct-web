import React from 'react'
import { Table, Icon, Tag, Modal } from 'antd'
import { DropOption } from 'components'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onEditItem, onDeleteItem, ...tableProps }) => {
  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该条记录?',
        onOk () {
          onDeleteItem(record.userId)
        },
      })
    }
  }
  const columns = [
    {
      title: 'ID',
      dataIndex: 'userId',
    }, {
      title: '用户名',
      dataIndex: 'username',
    }, {
      title: 'Email',
      dataIndex: 'email',
    }, {
      title: '电话',
      dataIndex: 'mobile',
    }, {
      title: '创建时间',
      dataIndex: 'createTime',
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (<DropOption
          dropdownProps={{
            trigger: ['click'],
          }}
          onMenuClick={e => handleMenuClick(record, e)}
          menuOptions={[{ key: '1', name: '修改' }, { key: '2', name: '删除' }]}
        />)
      },
    },
  ]

  return (
    <Table
      {...tableProps}
      bordered
      columns={columns}
      className={styles.table}
      simple
      rowKey={record => record.userId}
    />
  )
}

export default List
