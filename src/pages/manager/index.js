import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Tabs } from 'antd'
import { routerRedux } from 'dva/router'
import Filter from './Filter'
import List from './List'
import Modal from './Modal'

const Manager = ({ manager, dispatch, loading, location }) => {
  const { list, pagination, roles, userRoles, currentItem, modalVisible, modalType, selectedRowKeys } = manager
  const { query = {}, pathname } = location
  const modalProps = {
    modalType,
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    roles,
    userRoles,
    maskClosable: false,
    confirmLoading: loading.effects['role/update'],
    title: `${modalType === 'create' ? 'Create Manager' : 'Update Manager'}`,
    wrapClassName: 'vertical-center-modal',

    onOk (data) {
      dispatch({
        type: `manager/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'manager/hideModal',
      })
    },
  }
  const listProps = {
    pagination,
    dataSource: list,
    loading: loading.effects['manager/query'],
    onChange (page) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }))
    },
    onDeleteItem (id) {
      dispatch({
        type: 'manager/delete',
        payload: {
          id,
        },
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'manager/edit',
        payload: {
          id: item.userId,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'manager/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }
  const filterProps = {
    onAdd () {
      dispatch({
        type: 'manager/add',
        payload: {
          modalType: 'create',
        },
      })
    },
  }


  return (<div className="content-inner">
    <Filter {...filterProps} />
    <List {...listProps} />
    {modalVisible && <Modal {...modalProps} />}
  </div>)
}

Manager.propTypes = {
  manager: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default connect(({ manager, loading }) => ({ manager, loading }))(Manager)
