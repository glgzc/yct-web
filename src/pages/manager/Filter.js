import React from 'react'
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Switch } from 'antd'

const Filter = ({
  onAdd,
}) => {
  return (
    <Row gutter={24}>

      <Col xl={{ span: 10 }} md={{ span: 24 }} sm={{ span: 24 }}>
        <Button type="ghost" onClick={onAdd}>Create</Button>
      </Col>
    </Row>
  )
}

export default Filter
