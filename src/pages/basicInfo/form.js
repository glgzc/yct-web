import React from 'react'
import { Form, Input, Radio, Modal, Row, Col, Checkbox } from 'antd'

const FormItem = Form.Item
const RadioGroup = Radio.Group
const CheckboxGroup = Checkbox.Group;

const BasicInfoForm = ({
  item = {},
  roles,
  userRoles,
  onOk,
  tagList,
  getTagCheckBoxArry,
  tagNameArry,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  modalType,
  ...modalProps
}) => {
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const addData = {
        ...getFieldsValue()
      }
      // 获取data [{"id":6,"tagName":"置业顾问","c":"不会刚刚"},{"id":7,"tagName":"房产信息","c":"aaa"}]
      let data = []
      for (let y = 0; y < tagNameArry.length; y++) {
        let itemData = {}
        for (let index = 0; index < tagList.length; index++) {
          if (tagList[index].tagName == tagNameArry[y]) {
            itemData.tagName = tagList[index].tagName
            itemData.id = tagList[index].id
            for (const key in addData) {
              if (key == tagNameArry[y]) {
                itemData.c = addData[key]
              }
            }
            data.push(itemData)
          } else {

          }
        }
      }
      addData.data = JSON.stringify(data)
      onOk(addData)
    })
  }
  function onChange(checkedValues) {
    getTagCheckBoxArry(checkedValues)
  }
  const modalOpts = {
    ...modalProps,
    onOk: handleOk,
  }
  const checkBoxTag = () => {
    if (tagList != undefined && tagList != [] && tagList != null) {
      let result = tagList.map((item) => {
        return { label: item.tagName, value: item.tagName, id: item.id }
      })
      return (
        <CheckboxGroup options={result} onChange={onChange} />
      )
    } else {
      return null
    }
  }
  const checkBoxTagNote = () => {
    if (tagNameArry != undefined && tagNameArry != [] && tagNameArry != null) {
      let result = tagNameArry.map((item) => {
        return (
          <Col md={{ span: 12 }}>
            <FormItem label={item} >
              {getFieldDecorator(item, {
              })(<Input maxLength="24" placeholder="请输入不超过24字数内容" />)}
            </FormItem>
          </Col>
        )
      })
      return result
    } else {
      return null
    }
  }
  return (
    <Modal {...modalOpts}>
      <Form layout="vertical">
        <Row gutter={24}>
          <Col md={{ span: 12 }}>
            <FormItem label="名称" hasFeedback>
              {getFieldDecorator('title', {
                // initialValue: item.username,
                rules: [
                  {
                    required: true,
                    message: '名称不能为空!',
                  },
                ],
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="公证人" hasFeedback>
              {getFieldDecorator('notary', {
                initialValue: '',
                rules: [
                  {
                    required: true,
                    message: '公证人不能为空!',
                  },
                ],
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="助理" hasFeedback>
              {getFieldDecorator('assistant', {
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="使用地" hasFeedback>
              {getFieldDecorator('siteOfUse', {
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="办理时间" hasFeedback>
              {getFieldDecorator('processingTime', {
              })(<Input />)}
            </FormItem>
            <div style={{ overflow: 'hidden' }}>
              <span style={{ float: 'left' }}>需要寄送副本:</span>
              <Form.Item label="" style={{ float: 'right' }}>
                {getFieldDecorator('send', {
                  initialValue: 0,
                })(
                  <RadioGroup
                    style={{ marginLeft: 10, marginRight: 30 }}
                  >
                    <Radio value={1}>是</Radio>
                    <Radio value={0}>否</Radio>
                  </RadioGroup>
                )}
              </Form.Item>
            </div>
            <div style={{ overflow: 'hidden' }}>
              <span style={{ float: 'left' }}>急件:</span>
              <Form.Item label="" style={{ float: 'right' }}>
                {getFieldDecorator('urgentItems', {
                  initialValue: 0,
                })(
                  <RadioGroup
                    style={{ marginLeft: 10, marginRight: 30 }}
                  >
                    <Radio value={1}>是</Radio>
                    <Radio value={0}>否</Radio>
                  </RadioGroup>
                )}
              </Form.Item>
            </div>
            <div style={{ overflow: 'hidden' }}>
              <span style={{ float: 'left' }}>需要认证:</span>
              <Form.Item label="" style={{ float: 'right' }}>
                {getFieldDecorator('authentication', {
                  initialValue: 0,
                })(
                  <RadioGroup
                    style={{ marginLeft: 10, marginRight: 30 }}
                  >
                    <Radio value={1}>是</Radio>
                    <Radio value={0}>否</Radio>
                  </RadioGroup>
                )}
              </Form.Item>
            </div>
          </Col>
          <Col md={{ span: 12 }}>
            <FormItem label="部门" hasFeedback>
              {getFieldDecorator('organization', {
                rules: [
                  {
                    required: true,
                    message: '部门不能为空!',
                  },
                ],
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="协办人" hasFeedback>
              {getFieldDecorator('coOrganizer', {
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="领证人" hasFeedback>
              {getFieldDecorator('certificationHolder', {
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
            <FormItem label="用途" hasFeedback>
              {getFieldDecorator('purpose', {
              })(<Input maxLength="12" placeholder="请输入不超过12字数内容" />)}
            </FormItem>
          </Col>
        </Row>
        <span>
          <h3>其他信息</h3>
        </span>
        <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
      </Form>
      <span>
        <h4>后台配置的标签名称</h4>
      </span>
      {checkBoxTag()}
      {tagNameArry.length > 0 ? <h3 style={{ marginTop: 15 }}>添加标签备注</h3> : null}
      <Form layout="vertical">
        <Row gutter={24} style={{}}>
          {checkBoxTagNote()}
        </Row>
      </Form>
    </Modal>
  )
}

export default Form.create()(BasicInfoForm)
