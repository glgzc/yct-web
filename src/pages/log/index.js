import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import List from './List'

const Log = ({ log, dispatch, loading, location }) => {
  const { list, pagination } = log
  const { query = {}, pathname } = location

  const listProps = {
    pagination,
    dataSource: list,
    loading: loading.effects['manager/query'],
    onChange(page) {
      dispatch({
        type: 'log/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
        }
      })
    },
  }

  return (<div className="content-inner">

    <List {...listProps} />
  </div>)
}

Log.propTypes = {
  log: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default connect(({ log, loading }) => ({ log, loading }))(Log)
