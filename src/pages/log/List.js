import React from 'react'
import { Table, Icon, Tag, Modal } from 'antd'
import { DropOption } from 'components'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onEditItem, onDeleteItem, ...tableProps }) => {
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
    }, {
      title: '用户名',
      dataIndex: 'username',
    }, {
      title: '操作',
      dataIndex: 'operation',
    }, {
      title: '方法',
      dataIndex: 'method',
    }, {
      title: '参数',
      dataIndex: 'params',
      width:300
    }, {
      title: 'IP',
      dataIndex: 'ip',
    }, {
      title: '创建时间',
      dataIndex: 'createDate',
    },
  ]

  return (
    <Table
      {...tableProps}
      scroll={{x:true}}
      bordered
      columns={columns}
      className={styles.table}
      simple
      rowKey={record => record.id}
    />
  )
}

export default List
