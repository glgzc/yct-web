import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import List from './List'
import Filter from './Filter'
import Modal from './detailsModal'
import Details from './Details'
import EditView from './form'
const Client = ({ location, dispatch, client, loading }) => {
  const {
    visibleDetailsModal,detailsInfo,list,pagination,currentItem,visibleDetails,currentInfo,modalVisible,modalType,
    isMotion, modelVisibleGroup, interfaceList, visibleCreateApi, currentApiItem, currentMethod, showAddPerson, showEdit,
    keywords,risk,
  } = client
  const { pageSize } = pagination
  const modalProps = {
    currentApiItem,
    modalType,
    currentMethod: currentMethod === '' ? currentItem.method : currentMethod,
    item: modalType === 'update' ? currentItem : {},
    visible: modalVisible,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    maskClosable: false,
    confirmLoading: loading.effects['client/update'],
    title: `${modalType === 'create' ? (visibleCreateApi ? '添加服务接口' : '服务接口') : (modalType === 'update' ? '编辑服务接口----' + currentItem.interfaceName : (modalType === 'updateApi' ? '编辑服务接口' : '添加价格组'))}`,
    wrapClassName: 'vertical-center-modal',
    onOk(data) {
      dispatch({
        type: `client/${modalType}`,
        payload: data,
      })
    },
    onCancel() {
      dispatch({
        type: 'client/hideModal',
      })
    },
    createOk() {
      dispatch({
        type: 'client/showCreateApi',
        payload: {
          currentApiItem: {},
          modalType: 'create'
        }
      })
    },
    createCancel() {
      dispatch({
        type: 'client/hideCreateApi',
        payload: {
          modalType: 'create'
        }
      })
    },
    onDeleteApi(data) {
      dispatch({
        type: 'client/deleteInterface',
        payload: data
      })
    },
    onEdit(data) {
      dispatch({
        type: 'client/showUpdateApi',
        payload: {
          currentApiItem: data,
          modalType: 'updateApi'
        }
      })
    },
    onEditOk(data) {
      dispatch({
        type: 'client/updateInterface',
        payload: {
          modalType: 'create',
          data
        }
      })
    },
    onChangeMethod(value) {
      dispatch({
        type: 'client/currentMethod',
        payload: {
          currentMethod: value
        }
      })
    }
  }
  const listProps = {
    list,
    loading: loading.effects['client/query'],
    pagination,
    location,
    isMotion,
    dispatch,
    onChange(page) {
      dispatch({
        type: 'client/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
          keywords: keywords,
          risk:risk,
        }
      })
    },
    onRowClick(record) {
      dispatch({
        type: 'client/info',
        payload: {
          currentItem: record,
          visibleDetails: true
        }
      })
    },
    getEditData(record) {
      dispatch({
        type: 'client/getEdit',
        payload: {
          currentItem: record,
        }
      },
      )
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'client/queryKeyWords',
        payload: {
         ...value
        },
      })
    },
    addNaturalPerson() {
      dispatch({
        type: 'client/newNaturalPerson',
        payload: {
         
        }
      },
      )
    },
    onSearch(fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/client',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/client',
      }))
    },
    switchIsMotion() {
      dispatch({ type: 'client/switchIsMotion' })
    }
  }

  const detailsProps = {
    currentItem,
    currentInfo,
    onHideDetails() {
      dispatch({
        type: 'client/hideDetails',
        payload: {
          visibleDetails: false
        }
      })
    },
    showDetailModal(record) {
      dispatch({
        type: 'client/showDetailsModal',
        payload: {
          ...record,
          visibleDetailsModal: true
        }
      })
    },
    hideDetailModal() {
      dispatch({
        type: 'client/hideDetailsModal',
        payload: {
          visibleDetailsModal: false
        }
      })
    }
  }
  const detailsModalProps = {
    visibleDetailsModal,
    detailsInfo,
    title: <div style={{ textAlign: "center" }}>当事人信息详情</div>,
    okText: "收起",
    footer: null,
    onOk() {
      dispatch({
        type: "client/hideDetailsModal"
      })
    }
  }
  
  return (
    <div style={{ overflow: 'auto' }}>
   {showEdit? <EditView /> :<div className='content'>
        <div className='content-left'>
          <Filter {...filterProps} />

          <List {...listProps} />

        </div>
        {
         showAddPerson?null: visibleDetails ?
            // 查看当事人详情
            <div className='content-right'>
              <Details {...detailsProps} />
            </div>
            : null
        }
        {visibleDetailsModal ?
          <Modal {...detailsModalProps} /> : null
        }
      </div>} 
    </div>
  )
}

Client.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ client, loading }) => ({ client, loading }))(Client)
