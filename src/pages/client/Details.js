import React from 'react'
import { Select, Card, Icon, Button } from 'antd'
import $ from "jquery"
import Lightbox from 'react-images'
import { config } from 'utils';
import riskImg from '../../assets/风险提示.png';
import styles from "./index.less";
const { fileSource } = config;
const Option = Select.Option;


class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      value: '',
      bottons: '查看',
      previewVisible: false,
      previewImage: [],
      previewIndex: 0,
      rotate: 0,

    };
  }
  componentDidMount() {

  }

  imgClick = (rotate) => {
    this.setState({
      rotate: rotate + 90
    })
    var pic = document.getElementsByTagName('img')
    var r = rotate + 90
    $("img").eq(pic.length - 1).css('transform', 'rotate(' + r + 'deg)');
  }
  handlePreview = (url) => {
    // if(this.props.currentInfo.partyFile === undefined){
    //   return
    // }
    const { partyFile } = this.props.currentInfo
    const arr = [{ src: `${fileSource}/${url}?width=400&height=400&mode=fit`, caption: '单击旋转' }]
    // for(let i=0;i<5;i++)
    // {
    // partyFile[0].map((item)=>
    // arr.push({src:`${fileSource}/${item}?width=400&height=400&mode=fit`,caption: '单击旋转,滚动缩放图片'})
    // )
    // }
    this.setState({
      previewImage: arr,
      previewVisible: true,
    })
  };
  handleHideDetails() {
    this.props.onHideDetails()
  }
  render() {
    const {
      currentItem,
      currentInfo,
      showDetailModal
    } = this.props
    // console.log(appPartyUser.autographUrl,'appPartyUser.autographUrl')
    let currentTime = '&timestamp=' + new Date().getTime()
    const { appPartyUser, partyFile, comparisonRecordList } = currentInfo;
    const { previewVisible, previewImage, previewIndex } = this.state
    let otherInfo = ''
    if (appPartyUser.data != null && appPartyUser.data != '') {
      const arr = JSON.parse(appPartyUser.data)
      arr.map((item, index) => {
        if (index === arr.length - 1) {
          return otherInfo = otherInfo + item.tagName + "-" + item.c
        }
        else return otherInfo = otherInfo + item.tagName + "-" + item.c + "/"
      }
      )
    }
    const showImage = (partyFile) => {
      if (!partyFile) {
        return
      }
      let i = 0
      return <div className='fileWrap'>
        {
          partyFile.length !== 0 ?
            partyFile.map((item, index) => {
              if (!item) {
                return null
              }
              i++
              return (
                <Card
                  key={index}
                  title={`材料${i}`}
                  style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
                >
                  <img style={{ width: 160, height: 180 }} onClick={this.handlePreview.bind(this, item)} src={`${fileSource}/${item}?width=400&height=400&mode=fit` + currentTime} className='fileImage' />
                </Card>
              )
            })
            : '无'
        }
      </div>
    }
    const RecordList = (data) => {
  
      return data.map((item, index) => {
        return (
          <div className="text" key={index}>
            <h4 style={{ marginTop: 16, marginBottom: 16 }}>人证信息比对情况{index + 1}</h4>
            <div><span>比对发起公证人：</span><span>{unNull(item.userName)}</span></div>
            <div><span>发起时间：</span><span>{unNull(item.createdTime)}</span></div>
            <div><span>对比类型：</span><span>{unNull(item.title)}</span></div>
            <div><span>比对得分：</span><span>{unNull(item.coincidenceDegree)}</span></div>
            <div><span>金额：</span><span>{unNull(item.price)}</span></div>
            {showImage([item.image2])}
          </div>)
      })
    }
    let currentTimeNow = '&timestamp=' + new Date().getTime()
    return (
      <div className='acceptanceDetails'>
        <a onClick={this.handleHideDetails.bind(this)}><Icon type="close" className='closeIcon' /></a>
        <div className='detailsHeader'>
          <h2>查看当事人详情</h2>
        </div>
        <div className={appPartyUser.riskPeople && styles.riskbackground}>
          <div className="text marginTop">
            <div>
              <span>姓名：</span><span>{unNull(appPartyUser.name)}</span>
            </div>
            <div>
              <span>身份证号：</span><span>{unNull(appPartyUser.idCard)}</span>
            </div>
            <div>
              <span>民族：</span><span>{unNull(appPartyUser.famousRace)}</span>
            </div>
            <div>
              <span>住所：</span><span>{unNull(appPartyUser.address)}</span>
            </div>
            <div>
              <span>性别：</span><span>{unNull(appPartyUser.sex)}</span>
            </div>
            <div>
              <span>签名机关：</span><span>{unNull(appPartyUser.lssueOffice)}</span>
            </div>
            <div>
              <span>出生日期：</span><span>{unNull(appPartyUser.birth)}</span>
            </div>
            <div>
              <span>有效期：</span><span>{unNull(appPartyUser.validityPeriod)}</span>
            </div>
            <div>
              <span>电话：</span><span>{unNull(appPartyUser.mobile)}</span>
            </div>
            <div>
              <span>邮箱：</span><span>{unNull(appPartyUser.email)}</span>
            </div>
            <div>
              <span>比对得分：</span><span>{unNull(appPartyUser.recognitionResult)}</span>
            </div>
            <div>
              <span>风险控制：</span><span>{appPartyUser.riskPeople ? "是" : "否"}</span>
            </div>
            <div>
              <span>风险备注：</span><span>{unNull(appPartyUser.riskPeopleReason)}</span>
            </div>
          </div>
        </div>
        <div className='marginTop'>
          <h3 style={{ marginBottom: 16 }}>补充资料</h3>
          <h4 style={{ marginBottom: 16 }}>身份资料</h4>
          <div className='fileWrap'>
            {partyFile[5][0] ? <Card
              key={1}
              title={`材料${1}`}
              style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
            >
              <img style={{ width: 160, height: 180 }} onClick={this.handlePreview.bind(this, partyFile[5][0])} src={`${fileSource}/${partyFile[5][0]}?width=400&height=400&mode=fit` + currentTimeNow} className='fileImage' />
            </Card>
              : '无'}
               {partyFile[6][0] ? <Card
              key={1}
              title={`材料${2}`}
              style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
            >
              <img style={{ width: 160, height: 180 }} onClick={this.handlePreview.bind(this, partyFile[6][0])} src={`${fileSource}/${partyFile[6][0]}?width=400&height=400&mode=fit` + currentTimeNow} className='fileImage' />
            </Card>
              : '无'}
      </div>
          <h4 style={{ marginTop: 16, marginBottom: 16 }}>指纹信息</h4>
          {appPartyUser.fingerprintUrl != "" ?
            <Card
              title="材料"
              style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
            >
              <img style={{ width: 160, height: 180 }} src={`data:image/png;base64,${appPartyUser.fingerprintUrl}`} />
            </Card> : <p>无</p>}
          <h4 style={{ marginTop: 16, marginBottom: 16 }}>签名信息</h4>
          {appPartyUser.autographUrl != "" && appPartyUser.autographUrl != undefined && appPartyUser.autographUrl != null && appPartyUser.autographUrl != 'null' ?
            <Card
              title="材料"
              style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
            >
              <img style={{ width: 160, height: 180 }} src={`data:image/png;base64,${appPartyUser.autographUrl}`} />
            </Card> : <p>无</p>}
          <h4 style={{ marginTop: 16 }}>婚姻状况说明</h4>
          {showImage(partyFile[1])}
          <h4 style={{ marginTop: 16 }}>户口信息</h4>
          {showImage(partyFile[2])}
          <h4 style={{ marginTop: 16 }}>资产信息</h4>
          {showImage(partyFile[3])}
          {RecordList(comparisonRecordList)}
          <div style={{ marginTop: 16 }}><Icon type="pushpin-o" style={{ float: "left", fontSize: 16, color: '#08c' }} /><h4>备注</h4></div>
        </div>
        <Lightbox
          onClickImage={this.imgClick.bind(this, this.state.rotate)}
          showThumbnails={true}
          backdropClosesModal={true}
          currentImage={previewIndex}
          images={previewImage}
          isOpen={previewVisible}
          onClickPrev={() => {
            this.setState({
              previewIndex: previewIndex - 1,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClickThumbnail={(index) => {
            this.setState({
              previewIndex: index,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClose={() => { this.setState({ previewVisible: false }) }}
        />
      </div>
    )
  }
}

function unNull(value, replace = '--') {
  return value ? value : replace;
}
export default Details;
