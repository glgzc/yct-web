import React from 'react';
import { Form, Button, Input, Radio, Row, Col, Icon } from 'antd';
import DisplayInformation from './displayInformation';
import defaultImg from '../../../assets/defaultImg.png';
import OCRImg from '../../../assets/OCRImg.png';
import NFCImg from '../../../assets/NFCImg.png';

const { TextArea } = Input;
const RadioGroup = Radio.Group;
const AddNaturalPersonModal = ({
  form,
  fileList,
  fingerLoading,
  addThePartUser,
  people,
  idcard00,
  idcard01,
  readCard,
  onCardOCR,
  onUploadChange,
  onReadFinger,
  onRemoveFinger,
  onFaceCheck,
  onTakePicture,
  showEdit,
  hiddenEdit, autographUrl,
  onSwitchChange, fullState, resetData, isNew,currentInfo,
}) => {
  // console.log(isNew, 'isNew')
  let sider
  const { validateFields, getFieldDecorator, getFieldsValue, resetFields } = form;
  const reset = () => {
    resetFields();
    resetData()
  };
  const goToBack = () => {
    if (showEdit) {
      hiddenEdit()
    } else {
      history.back();
    }
  };

  const saveData = () => {
    let timestamp = new Date().getTime()
    if (!validateFields()) {
      return
    }
    const data = {
      ...getFieldsValue(),
      ...fullState
    };
    const localId = sessionStorage.getItem('localId');
    data.localId = localId;
    if (data.idCard == '' || data.idCard == undefined) {
      return;
    }
    const signData = sider.getSignData()
    if (signData != '' && signData != 'null' && signData != null && signData != undefined && signData != 'undefined') {
      data.autographUrl = signData
    } else {
      data.autographUrl = autographUrl
    }
    data.recognitionResult = people.recognitionResult || 0
    data.fingerprintUrl = people.fingerprintUrl || ""
    data.clientUpdateAt = timestamp
    data.assetsInfoState = parseInt(data.assetsInfoState)
    data.fingerprintCode = ''
    if (people.base64 =='null' || people.base64==null) {
      data.cardPositiveFileUrl = undefined
    } else {
      data.cardPositiveFileUrl = people.base64
    }
    let deleteFileIdArry = []
    let fileListArry = []
    let deleteString = ''
    if (fileList['5']&&fileList['5'].length == 1) {
      deleteFileIdArry = currentInfo.partyFile["5"]
      fileListArry = deleteFileIdArry
    } else if (fileList['5']&&fileList['5'].length >1) {
      deleteFileIdArry = currentInfo.partyFile["5"]
      fileList['5'].splice(fileList['5'].length - 1, 1)
      let fiveArry = []
      fileList['5'].map((Item) => {
        fiveArry.push(Item.url.substring(Item.url.lastIndexOf("\/") + 1,Item.url.length))
      })
      fileListArry = deleteFileIdArry.concat(fiveArry)
    } else {
      if (currentInfo.partyFile["5"]) {
        deleteFileIdArry =  currentInfo.partyFile["5"]
        deleteFileIdArry.splice(deleteFileIdArry.length - 1, 1)
        fileListArry = deleteFileIdArry
      }
    }
    // 背面
    let deleteFileIdArrys = []
    let fileListArrys = []
    if (fileList['6']&&fileList['6'].length == 1) {
      deleteFileIdArrys = currentInfo.partyFile["6"]
      fileListArrys = deleteFileIdArrys
    } else if (fileList['6']&&fileList['6'].length >1) {
      deleteFileIdArrys = currentInfo.partyFile["6"]
      fileList['6'].splice(fileList['6'].length - 1, 1)
      let sixArry = []
      fileList['6'].map((Item) => {
        sixArry.push(Item.url.substring(Item.url.lastIndexOf("\/") + 1,Item.url.length))
      })
      fileListArrys = deleteFileIdArrys.concat(sixArry)
    } else {
      if (currentInfo.partyFile["6"]) {
        deleteFileIdArrys =  currentInfo.partyFile["6"]
        deleteFileIdArrys.splice(deleteFileIdArrys.length - 1, 1)
        fileListArrys = deleteFileIdArrys
      }
    }
    // 合并数组
    let contantArry = []
    if (fileListArrys !=undefined || fileListArry !=undefined) {
      contantArry = fileListArrys.concat(fileListArry)
    }
    console.log(data,'data')
    deleteString = contantArry.join('#') 
    addThePartUser(data,deleteString);
  };
  return (
    <div style={{ backgroundColor: '#F8F8F8', overflow: 'hidden' }}>
      {/* 头部 */}
      <div
        style={{
          overflow: 'hidden',
          background: 'white',
          padding: 15,
          marginBottom: 20,
        }}
      >
        <div style={{ float: 'left' }}>
          <Button
            style={{ fontSize: 14, display: 'block', border: 0, paddingLeft: 0 }}
            onClick={goToBack}
          >
            <Icon type="left" />
            <span>返回</span>
          </Button>
        </div>
        <div style={{ float: 'right', overflow: 'hidden' }}>
          <Button className="margin-right" onClick={reset}>
            重置
          </Button>
          <Button type="primary" onClick={saveData}>
            保存
          </Button>
        </div>
      </div>
      {/* 内容 */}
      <Row gutter={24}>
        {/* 基本信息 */}

        <Col md={{ span: 12 }}>
          <div
            style={{
              backgroundColor: 'white',
              paddingLeft: 20,
              paddingRight: 20,
              paddingBottom: 20,
              borderRadius: 6,
            }}
          >
            <div style={{ borderBottom: '1px solid #f0f0f0', padding: '15px 0', marginBottom: 20 }}>
              <h2 style={{ margin: 0 }}>基本信息</h2>
            </div>

            {/* 信息录入 */}
            {isNew ? <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
              <a
                href="javascript:;"
                id="NFCIdentify"
                style={{
                  display: 'block',
                  textAlign: 'center',
                  width: 165,
                  height: 110,
                  backgroundColor: '	#f5f5f5',
                  marginRight: 15,
                  borderRadius: 6,
                }}
              >
                <p>
                  <img
                    style={{ width: 82, height: 58, marginTop: 10, display: 'inline-block' }}
                    src={NFCImg}
                  />
                </p>
                <p style={{ marginTop: 10 }}>NFC识别</p>
              </a>
              <a
                href="javascript:;"
                style={{
                  display: 'block',
                  textAlign: 'center',
                  width: 165,
                  height: 110,
                  backgroundColor: '	#f5f5f5',
                  marginRight: 15,
                  borderRadius: 6,
                }}
                onClick={() => onCardOCR()}
              >
                <p>
                  <img
                    style={{ width: 82, height: 58, marginTop: 10, display: 'inline-block' }}
                    src={OCRImg}
                  />
                </p>
                <p style={{ marginTop: 10 }}>OCR识别</p>
              </a>
            </div> : null}
            {/* 材料 */}
            <div
              style={{
                width: 166,
                marginTop: 20,
                borderWidth: 1,
                borderColor: '#ddd',
                marginBottom: 12,
                borderStyle: 'solid',
              }}
            >
              <div style={{ backgroundColor: '#F5F5F5' }}>
                <p style={{ textAlign: 'center', padding: 15 }}>材料1 (自然人正面照)</p>
              </div>
              <div
                style={{
                  margin: '14px auto',
                  backgroundColor: '#f3f3f3',
                  height: 116,
                  width: 90,
                  display: 'flex',
                }}
              >
                <img
                  style={{ width: 51, height: 63, margin: 'auto' }}
                  src={people.base64!=null&&people.base64!='null'&&people.base64!=''&&people.base64!=undefined? 'data:image/png;base64,' + people.base64 : defaultImg}
                />
              </div>
            </div>
            {/* 表单 */}
            <div>
              <Form layout={'vertical'}>
                {/* onSubmit={handleSubmit} */}
                <Form.Item label="姓名" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('name', {
                    initialValue: people.name || '',
                  })(<Input placeholder="尚未识别有效信息" disabled={!isNew} />)}
                </Form.Item>
                <Form.Item label="民族" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('famousRace', {
                    initialValue: people.nativePlace || '',
                  })(<Input placeholder="尚未识别有效信息" disabled={!isNew} />)}
                </Form.Item>
                <Form.Item label="性别" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('sex', {
                    initialValue: people.sex || '',
                  })(
                    <Input
                      disabled={!isNew}
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="出生日期" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('birth', {
                    initialValue: people.dateOfBirth || '',
                  })(
                    <Input
                      disabled={!isNew}
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="住址" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('address', {
                    initialValue: people.address || '',
                  })(
                    <Input
                      disabled={!isNew}
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="签发机关" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('lssueOffice', {
                    initialValue: people.authority || '',
                  })(
                    <Input
                      disabled={!isNew}
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="有效期" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('validityPeriod', {
                    initialValue: people.validStart
                      ? people.validStart
                      : '',
                  })(
                    <Input
                      disabled={!isNew}
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="身份证号" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('idCard', {
                    initialValue: people.cardID || '',
                    rules: [{ required: true, message: '请输入身份证!' }],
                  })(
                    <Input
                      disabled={!isNew}
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="手机号" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('mobile', {
                    initialValue: people.mobile || '',
                  })(
                    <Input
                      // disabled={true}
                      placeholder="请输入手机号"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="邮箱" style={{ width: 294, marginRight: 30 }}>
                  {getFieldDecorator('email', {
                    initialValue: people.email || '',
                  })(
                    <Input

                      placeholder="请输入邮箱"
                    />,
                  )}
                </Form.Item>
                <div
                  style={{ borderBottom: '1px solid #f0f0f0', padding: '15px 0', marginBottom: 20 }}
                >
                  <h2 style={{ margin: 0 }}>基本信息</h2>
                </div>
                <div style={{ overflow: 'hidden' }}>
                  <span style={{ float: 'left' }}>是否是风险人员:</span>
                  <Form.Item label="" style={{ float: 'left' }}>
                    {getFieldDecorator('riskPeople', {
                      initialValue: people.riskPeople || 0,
                    })(
                      <RadioGroup
                        // onChange={this.onChange} value={this.state.value}
                        style={{ marginLeft: 10, width: 294, marginRight: 30 }}
                      >
                        <Radio value={1}>是</Radio>
                        <Radio value={0}>否</Radio>
                      </RadioGroup>,
                    )}
                  </Form.Item>
                </div>

                <Form.Item label=" 备注:" style={{}}>
                  {getFieldDecorator('riskPeopleReason', {
                    initialValue: people.riskPeopleReason || '',
                  })(<TextArea rows={4} />)}
                </Form.Item>
              </Form>
            </div>

            <div style={{ marginTop: 40 }}>
              <h2 style={{ color: '	#000000' }}>人证对比信息</h2>
            </div>
            <span
              style={{
                display: 'block',
                width: '100%',
                height: 1,
                backgroundColor: '#e2e2e2',
                marginBottom: 25,
                marginTop: 14,
              }}
            />
            <div style={{ overflow: 'hidden', marginBottom: 20 }}>
              <div style={{ float: "left", fontSize: 50, lineHeight: "50px", color: '#EB544F', marginRight: 6 }}>{people.recognitionResult || "--"}</div>
              <div style={{ float: "left", fontSize: 14, lineHeight: "50px", color: '#191F25' }}>(人证比对)</div>
            </div>
            <div style={{ marginBottom: 54 }}>
              <Button type="primary" ghost style={{ marginRight: 24 }}>
                指纹对验
              </Button>
              <Button
                type="primary"
                ghost
                style={{ marginRight: 24 }}
                onClick={() => onFaceCheck(1)}
              >
                公安核验
              </Button>
              <Button
                type="primary"
                ghost
                style={{ marginRight: 24 }}
                onClick={() => onFaceCheck(0)}
              >
                人证比对
              </Button>
            </div>
          </div>
        </Col>

        <Col md={{ span: 12 }}>
          <DisplayInformation
            ref={el => sider = el}
            fileList={fileList}
            fingerLoading={fingerLoading}
            fingerData={people.fingerprintUrl}
            idcard00={idcard00}
            idcard01={idcard01}
            fullState={fullState}
            autographUrls={autographUrl}
            onReadFinger={() => onReadFinger()}
            onRemoveFinger={() => onRemoveFinger()}
            onTakePicture={(type, takeOnce) => onTakePicture(type, takeOnce)}
            onSwitchChange={(val, key) => onSwitchChange(val, key)}
            onUploadChange={({ file, fileList, key }) => onUploadChange({ file, fileList, key })}
          />
        </Col>
      </Row>
    </div>
  );
};

export default Form.create()(AddNaturalPersonModal);
