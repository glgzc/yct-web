import React from 'react'
import { Switch, Icon, Button, Upload, Row, Col, Card, Modal } from 'antd'
import SignViewer from "../../../components/SignViewer/index"
import { config } from 'utils'
import $ from "jquery"
import Lightbox from 'react-images'
const { host, api, fileSource } = config
const { fileUpload } = api

const idcard0Defaut = require('../../../assets/idcard0.png')
const idcard1Defaut = require('../../../assets/idcard1.png')
const defaultImg = require('../../../assets/defaultImg.png')
class DisplayInformation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      signData: '',
      previewVisible: false,
      previewImage: [],
      previewIndex: 0,
      rotate: 0,

    }
  }
  getSignData() {
    return this.state.signData
  }
  onSignDone() {
    const signData = this.refs.signViewer.getSignBase64()
    this.setState({
      signData,
      modalVisible: false
    })
  }
  imgClick = (rotate) => {
    this.setState({
      rotate: rotate + 90
    })
    var pic = document.getElementsByTagName('img')
    var r = rotate + 90
    $("img").eq(pic.length - 1).css('transform', 'rotate(' + r + 'deg)');
  }
  handlePreview = (file) => {
    const arr = [{ src: file.url || file.thumbUrl, caption: '单击旋转' }]
    this.setState({
      previewImage: arr,
      previewVisible: true,
    })
  };
  componentDidMount() {

  }
  // onSwitchChange(val,key) {
  //   const { onSwitchChange } = this.props
  //   onSwitchChange((val, key))
  // }
  render() {
    const { fileList, fingerData, fingerLoading, onUploadChange, onReadFinger, onRemoveFinger, onTakePicture, idcard00, idcard01, autographUrls, onSwitchChange,
      fullState, } = this.props
    const { modalVisible, signData, previewVisible, previewImage, previewIndex } = this.state
    const localId = sessionStorage.getItem("localId")
    const userId = sessionStorage.getItem('userId')
    const token = sessionStorage.getItem('token')
    let timestamp = new Date().getTime()
    const uploadData = {
      localId,
      userId,
      partyNumber: 0,
      type: 0,
      fileMd5: "1",
      clientUpdateAt: timestamp,
    }
    let idcard0 = ""
    let idcard1 = ""
    if (idcard00 == undefined || idcard00 == '') {
      if (fileList["5"] && fileList["5"].length > 0) {
        idcard0 = fileList["5"][fileList["5"].length - 1].url
      }
    } else {
      if (idcard00 != '') {
        if (fileList["5"] && fileList["5"].length > 0) {
          idcard0 = fileList["5"][fileList["5"].length - 1].url
        } else {
          idcard0 = fileSource + idcard00
        }
      }
    }
    if (idcard01 == undefined || idcard01 == '') {
      if (fileList["6"] && fileList["6"].length > 0) {
        idcard1 = fileList["6"][fileList["6"].length - 1].url
      }
    } else {
      if (fileList["6"] && fileList["6"].length > 0) {
        idcard1 = fileList["6"][fileList["6"].length - 1].url
      } else {
        idcard1 = fileSource + idcard01
      }
    }

    //const action="http://192.168.2.153:8080/appCollectionFile/upload"
    const action = host + fileUpload + "?token=" + token
    const uploadProps = {
      action,
      listType: "picture-card",
    }
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传文件</div>
      </div>
    );
    return (
      <div style={{ backgroundColor: 'white', borderRadius: 6 }}>
        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }} id="idText">身份证信息</h2>
        </div>
        <Row gutter={24} style={{ padding: 15 }}>
          <Col md={{ span: 12 }}>
            <div
              // id="IDLeftImg"
              style={{ overflow: "hidden" }}
              className="ant-upload ant-upload-drag"
              onClick={() => onTakePicture(5, true)}
              >
              {
                idcard0 != '' ?
                  <img src={idcard0} style={{ width: '100%', height: 'auto' }} /> :
                  <span tabIndex="0" className="ant-upload ant-upload-btn" role="button">

                    <div className="ant-upload-drag-container">
                      <p className="ant-upload-drag-icon">
                        <img src={idcard0Defaut} style={{ width: '100%', height: 'auto' }} />
                      </p>
                      <p className="ant-upload-text">身份证正面</p>
                    </div>
                  </span>
              }

            </div>
          </Col>
          <Col md={{ span: 12 }}>
            <div
              // id="IDRightImg"
              style={{ overflow: "hidden" }}
              className="ant-upload ant-upload-drag"
              onClick={() => onTakePicture(6, true)}
              >
              {
                idcard1 ?
                  <img src={idcard1} style={{ width: '100%', height: 'auto' }} /> :
                  <span tabIndex="0" className="ant-upload ant-upload-btn" role="button">

                    <div className="ant-upload-drag-container">
                      <p className="ant-upload-drag-icon">
                        <img src={idcard1Defaut} style={{ width: '100%', height: 'auto' }} />
                      </p>
                      <p className="ant-upload-text">身份证反面</p>
                    </div>
                  </span>
              }
            </div>
          </Col>
        </Row>
        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>指纹信息</h2>
        </div>
        <Row gutter={24} style={{ padding: 15 }}>
          <Col md={{ span: 24 }}>
            <Card hoverable style={{ textAlign: 'center' }}>
              <img src={fingerData != '' && fingerData != null && fingerData != undefined ? "data:image/png;base64," + fingerData : defaultImg} style={{ width: 'auto', height: 60 }} />
              <p style={{ margin: 20 }}>请将手指放置到设备上,然后点击获取指纹</p>
              <div>
                <Button className="margin-right" loading={fingerLoading} type="primary" shape="round" onClick={() => onReadFinger()}>获取指纹信息</Button>
                <Button shape="round" onClick={() => onRemoveFinger()}>清除指纹信息</Button>
              </div>
            </Card>

          </Col>

        </Row>
        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>签名信息</h2>
        </div>
        <div style={{ overflow: "hidden", textAlign: 'center' }}>
          <a href="javascript:;"
            style={{ display: 'block', width: 362, height: 202, margin: "15px auto", border: "1px solid #ddd" }}
            onClick={() => this.setState({ modalVisible: true })}
          >
            {
              signData != '' ? <img src={"data:image/png;base64," + signData} style={{ width: 360, height: 200, margin: 1 }} /> : autographUrls != '' && autographUrls != undefined && autographUrls != 'null' && autographUrls != null ? <img src={"data:image/png;base64," + autographUrls} style={{ width: 360, height: 200, margin: 1 }} /> :
                <img src={defaultImg} style={{ width: 'auto', height: 60, marginTop: 70 }} />
            }
          </a>
          <p style={{ marginTop: 20 }}>点击方框区域进行签名或者修改签名</p>
          <Button shape="round" onClick={() => {
            this.setState({ signData: "" })
          }}>清除签名</Button>

        </div>

        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>婚姻状况证明资料</h2>
          <div style={{ float: 'right' }} >
            资料不完整/资料完整 &nbsp;&nbsp;&nbsp;&nbsp;<Switch onChange={(val) => onSwitchChange(val, "marriagestatusInfoState")} checked={fullState['marriagestatusInfoState']} />
            <Button style={{ paddingRight: 0, border: 0, marginLeft: 15, outline: 0 }} onClick={() => onTakePicture(0)}>
              <span>拍照上传</span>
              <Icon type="camera" />
            </Button>
          </div>
        </div>
        <div className="clearfix" style={{ padding: 15 }}>
          <Upload {...uploadProps}
            data={{
              ...uploadData,
              dataType: 0,
            }}
            onChange={({ file, fileList }) => onUploadChange({ file, fileList, key: "0" })}
            fileList={
              fileList["0"]
            }
            onPreview={this.handlePreview}
          >
            {uploadButton}
          </Upload>
        </div>

        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>户口信息资料</h2>
          <div style={{ float: 'right' }} >
            资料不完整/资料完整 &nbsp;&nbsp;&nbsp;&nbsp;<Switch onChange={(val) => onSwitchChange(val, "registeredResidenceInfoState")} checked={fullState['registeredResidenceInfoState']} />
            <Button style={{ paddingRight: 0, border: 0, marginLeft: 15, outline: 0 }} onClick={() => onTakePicture(1)}>
              <span>拍照上传</span>
              <Icon type="camera" />
            </Button>
          </div>
        </div>
        <div className="clearfix" style={{ padding: 15 }}>
          <Upload {...uploadProps}
            data={{
              ...uploadData,
              dataType: 1,
            }}
            onChange={({ file, fileList }) => onUploadChange({ file, fileList, key: "1" })}
            onPreview={this.handlePreview}
            fileList={fileList["1"]}>
            {uploadButton}
          </Upload>
        </div>
        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>资产信息</h2>
          <div style={{ float: 'right' }} >
            资料不完整/资料完整 &nbsp;&nbsp;&nbsp;&nbsp; <Switch onChange={(val) => onSwitchChange(val, "assetsInfoState")} checked={parseInt(fullState['assetsInfoState'])} />
            <Button style={{ paddingRight: 0, border: 0, marginLeft: 15, outline: 0 }} onClick={() => onTakePicture(7)}>
              <span>拍照上传</span>
              <Icon type="camera" />
            </Button>
          </div>
        </div>
        <div className="clearfix" style={{ padding: 15 }}>
          <Upload {...uploadProps}
            data={{
              ...uploadData,
              dataType: 7,
            }}
            onChange={({ file, fileList }) => onUploadChange({ file, fileList, key: "7" })}
            onPreview={this.handlePreview}
            fileList={fileList["7"]}>
            {uploadButton}
          </Upload>
        </div>

        <Modal
          visible={modalVisible}
          title="请在下方空白区域签名"
          cancelText="取消"
          okText="确认"
          onCancel={() => this.setState({ modalVisible: false })}
          onOk={() => this.onSignDone()}
          width={840}
        >
          <SignViewer ref="signViewer" style={{ width: 800, height: 660 }} />
        </Modal>
        <Lightbox
          onClickImage={this.imgClick.bind(this, this.state.rotate)}
          showThumbnails={true}
          backdropClosesModal={true}
          currentImage={previewIndex}
          images={previewImage}
          isOpen={previewVisible}
          onClickPrev={() => {
            this.setState({
              previewIndex: previewIndex - 1,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')

          }}
          onClickThumbnail={(index) => {
            this.setState({
              previewIndex: index,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClose={() => { this.setState({ previewVisible: false }) }}
        />
      </div>
    )
  }
}

export default DisplayInformation;