import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Form, Button, Row, Col, Input, Select } from 'antd'

const Search = Input.Search
const Option = Select.Option;
const ColProps = {
  xs: 24,
  sm: 12,
  style: {
    marginBottom: 16,
  },
}
const TwoColProps = {
  ...ColProps,
  xl: 40,
}
const ThreeColProps = {
  ...ColProps,
  xl: 96,
}

const Filter = ({
  onFilterChange,
  addNaturalPerson,
  filter,
  form,
}) => {
  const { validateFields, getFieldDecorator, getFieldsValue } = form;
  const handleFields = (fields) => {
    // const { createTime } = fields
    // if (createTime.length) {
    //   fields.createTime = [createTime[0].format('YYYY-MM-DD'), createTime[1].format('YYYY-MM-DD')]
    // }
    return fields
  }

  const handleSubmit = () => {
    validateFields((errors, values) => {
      if (errors) {
        return;
      }
      if (!values.risk || values.risk === 'all') {
        delete values.risk;
      } else {
        values.risk = values.risk === '1' ? 1 : 0
      }
      if (!values.keywords) {
        delete values.keywords;
      }
      onFilterChange(values)
    })
  }
  const addPerson = () => {
    addNaturalPerson(true)
  }
  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const handleChange = (key, values) => {
    let fields = getFieldsValue()
    fields[key] = values
    fields = handleFields(fields)
    onFilterChange(fields)
  }
  const { name, address } = filter

  let initialCreateTime = []
  if (filter.createTime && filter.createTime[0]) {
    initialCreateTime[0] = moment(filter.createTime[0])
  }
  if (filter.createTime && filter.createTime[1]) {
    initialCreateTime[1] = moment(filter.createTime[1])
  }

  return (
    <Row gutter={24} style={{ marginBottom: '15px' }}>
      <Col {...ColProps} md={6} sm={6}>
        {getFieldDecorator('keywords', { initialValue: name })(<Search placeholder="姓名、手机号" onSearch={handleSubmit} />)}
      </Col>
      <Col md={4} style={{ height: 32, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        {getFieldDecorator('risk')(
          <Select placeholder="风险控制" style={{ width: '100%' }}>
            <Option key="all">所有人</Option>
            <Option key="1">已标记</Option>
            <Option key="0">未标记</Option>
          </Select>)}
      </Col>
      <Col {...TwoColProps} md={4} >
        <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
          <div >
            <Button type="primary" className="margin-right" onClick={handleSubmit}>查询</Button>
          </div>
        </div>
      </Col>
      {/* <Col md={4} {...ThreeColProps}> */}
      <div style={{ textAlign: 'right', marginRight: 12 }}>
        <Button type="primary" onClick={addPerson}>添加自然人</Button>
      </div>
      {/* </Col> */}
    </Row>
  )
}

Filter.propTypes = {
  isMotion: PropTypes.bool,
  switchIsMotion: PropTypes.func,
  form: PropTypes.object,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func,
}

export default Form.create()(Filter)
