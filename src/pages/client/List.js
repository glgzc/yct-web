import React from 'react'
import { Table, Modal, Popconfirm, Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'

import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'
import '../../utils/index'

const confirm = Modal.confirm

const List = ({ list, onRowClick,getEditData, ...tableProps }) => {
  const handleMenuClick = (record, e) => {
    getEditData(record)
  };
  const toOnRowClick = (record, e) => {
    onRowClick(record)
  };
  const columns = [
    {
      title: '编号',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '身份证号',
      dataIndex: 'idCard',
      key: 'idCard'
    }, {
      title: '电话',
      dataIndex: 'mobile',
      key: 'mobile'
    }, {
      title: '录入时间',
      dataIndex: 'timeCreate',
      key: 'timeCreate'
    }, {
      title: '风险控制',
      dataIndex: 'riskPeople',
      key: 'riskPeople',
      render: (text) => text ? '是' : '否'
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (
          <div style={{overflow:'hidden'}}>
            <a onClick={e => handleMenuClick(record, e)} style={{display:'inline-block'}}>编辑</a>
            <a onClick={e => toOnRowClick(record, e)} style={{display:'inline-block',marginTop:10}}>查看详情</a>
          </div>
        )
      },
    },
  ]
  return (
    <div style={{ marginTop: '25px' }}>
      <Table
        dataSource={list}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={row => row.id}
        pagination={tableProps.pagination}
        onChange={tableProps.onChange}
      />
    </div>
  )
}

export default List
