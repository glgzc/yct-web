import React from 'react'
import PropTypes from 'prop-types'
import { Form, Tree, Modal, Icon, Button} from 'antd'
import styles from './detailsModal.less'

const FormItem = Form.Item
const TreeNode = Tree.TreeNode

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const detailsModal = ({
  visibleDetailsModal,
  detailsInfo,
  item = {},
  onOk,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  dispatch,
  ...modalProps
}) => {
  const imgList=(fileList)=>{
     if(fileList.length !== 0)
      fileList.map((item, index) => {
        return (
          <Card
            key={index}
            title={`材料${index + 1}`}
            style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
          >
            <img src={`${fileSource}/${item}?width=400&height=400&mode=fit`} className='fileImage' />
          </Card>
        )
      })
    else return "无"
  }
  return (
    <Modal visible={visibleDetailsModal} onOk={()=>onOk()} {...modalProps}>
    <div className="test">
      <div>
        <span>名称：</span>
      </div>
      <div>
        <span>统一社会信用代码：</span>
      </div>
      <div>
        <span>类型：</span>
      </div>
      <div>
        <span>住所：</span>
      </div>
      <div>
        <span>法人：</span>
      </div>
      <div>
        <span>注册资本：</span>
      </div>
      <div>
        <span>成立时间：</span>
      </div>
      <div>
        <span>营业期限：</span>
      </div>
      <div>
        <span>经营范围：</span>
      </div>
      <div>
        <span>联系电话：</span>
      </div>
    </div>

    <div className="image">
    <div>
    <span>统一社会信用代码</span>

    </div>
    <div>
    <span>法人/委托人资料</span>

    </div>
    <div>
    <span>其他信息</span>
    
    </div>
    </div>
    <Button type='primary' style={{width:"100%",height:40}} onClick={()=>onOk()}>收起</Button>
    </Modal>
  )
}

detailsModal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(detailsModal)
