import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, InputNumber, Radio, Modal, Tree } from 'antd'

const TreeNode = Tree.TreeNode
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const modal = ({
  item = {},
  treeData,
  treeCheckedKeys,
  onTreeCheck,
  onOk,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  ...modalProps
}) => {
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: item.key,
      }
      data.menuIdList = treeCheckedKeys
      if (data.remark == undefined) {
        data.remark = ''
      }
      onOk(data)
    })
  }

  const modalOpts = {
    ...modalProps,
    onOk: handleOk,
  }
  const onCheck = (checkedKeys, e) => {
    onTreeCheck(checkedKeys)
  }
  const loop = data => data.map((item) => {
    if (item.children) {
      return (
        <TreeNode key={item.menuId} title={item.name} >
          {loop(item.children)}
        </TreeNode>
      )
    }
    return <TreeNode key={item.menuId} title={item.name} />
  })

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem label="角色名称" hasFeedback {...formItemLayout}>
          {getFieldDecorator('roleName', {
            initialValue: item.roleName,
            rules: [
              {
                required: true,
              },
            ],
          })(<Input />)}
        </FormItem>
        <FormItem label="备注" hasFeedback {...formItemLayout}>
          {getFieldDecorator('remark', {
            initialValue: item.remark,
          })(<Input />)}
        </FormItem>
        <Tree
          checkedKeys={treeCheckedKeys}
          onCheck={onCheck}
          defaultExpandAll
          checkable
        >
          {loop(treeData)}
        </Tree>
      </Form>
    </Modal>
  )
}

modal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(modal)
