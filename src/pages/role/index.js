import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Tabs } from 'antd'
import { routerRedux } from 'dva/router'
import Filter from './Filter'
import List from './List'
import Modal from './Modal'

const Role = ({ role, dispatch, loading, location }) => {
  const { list, pagination, treeData, treeCheckedKeys, currentItem, modalVisible, modalType, selectedRowKeys } = role
  const { query = {}, pathname } = location

  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    treeData,
    treeCheckedKeys,
    maskClosable: false,
    confirmLoading: loading.effects['role/update'],
    title: `${modalType === 'create' ? 'Create role' : 'Update role'}`,
    wrapClassName: 'vertical-center-modal',
    onTreeCheck (checkedKeys) {
      dispatch({
        type: 'role/updateState',
        payload: {
          treeCheckedKeys: checkedKeys,
        },
      })
    },
    onOk (data) {
      dispatch({
        type: `role/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'role/hideModal',
      })
    },
  }

  const listProps = {
    pagination,
    dataSource: list,
    loading: loading.effects['role/query'],
    onChange (page) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }))
    },
    onDeleteItem (id) {
      dispatch({
        type: 'role/delete',
        payload: {
          id,
        },
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'role/edit',
        payload: {
          id: item.roleId,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'role/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }
  const filterProps = {
    onAdd () {
      dispatch({
        type: 'role/add',
        payload: {
          modalType: 'create',
        },
      })
    },
  }


  return (<div className="content-inner">
    <Filter {...filterProps} />
    <List {...listProps} />
    {modalVisible && <Modal {...modalProps} />}
  </div>)
}

Role.propTypes = {
  role: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default connect(({ role, loading }) => ({ role, loading }))(Role)
