import React from 'react'
import { Select, Card, Icon, Button } from 'antd'
import $ from "jquery"
import Lightbox from 'react-images'
import { config } from 'utils';
const { fileSource } = config;
const Option = Select.Option;


class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      value: '',
      bottons: '查看',
      previewVisible: false,
      previewImage: [],
      previewIndex: 0,
      rotate: 0
    };
  }
  componentDidMount() {

  }
  imgClick = (rotate) => {
    this.setState({
      rotate: rotate + 90
    })
    var pic = document.getElementsByTagName('img')
    var r = rotate + 90
    $("img").eq(pic.length - 1).css('transform', 'rotate(' + r + 'deg)');
  }
  handlePreview = url => {
    if (this.props.currentInfo.partyFile === undefined) {
      return
    }
    const { partyFile } = this.props.currentInfo
    const arr = [{ src: `${fileSource}/${url}?width=400&height=400&mode=fit`, caption: '单击旋转' }]
    this.setState({
      previewImage: arr,
      previewVisible: true,
    })
  };
  handleHideDetails() {
    this.props.onHideDetails()
  }
  render() {
    const {
      currentItem,
      currentInfo,
      showDetailModal
    } = this.props
    let currentTime ='?'+ new Date().getTime()
    const { appPartyUser, partyFile } = currentInfo;
    const { previewVisible, previewImage, previewIndex } = this.state
    let otherInfo = ''
    if (appPartyUser.data != null && appPartyUser.data != '') {
      const arr = JSON.parse(appPartyUser.data)
      arr.map((item, index) => {
        if (index === arr.length - 1) {
          return otherInfo = otherInfo + item.tagName + "-" + item.c
        }
        else return otherInfo = otherInfo + item.tagName + "-" + item.c + "/"
      }
      )
    }
    const PersonListProps = {

    }
    const showImage = (partyFile) => {
      if (partyFile === undefined) {
        return
      }
      return <div className='fileWrap'>
        {
          partyFile.length !== 0 ?
            partyFile.map((item, index) => {
              return (
                <Card
                  key={index}
                  title={`材料${index + 1}`}
                  style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
                >
                  <img style={{ width: 160, height: 180 }} onClick={this.handlePreview.bind(this, item)} src={`${fileSource}/${item}?width=400&height=400&mode=fit`+currentTime} className='fileImage' />
                </Card>
              )
            })
            : '无'
        }
      </div>
    }
    const showMainImage = (url) => {
      let index = 0
      if (url == undefined || url==null||url=='null'||url=='undefined'||url=='') {
        return  <div className='fileWrap'>无</div>
      } else {
        return <div className='fileWrap'>
                <Card
                  key={index}
                  title={`材料${index + 1}`}
                  style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
                >
                  <img style={{ width: 160, height: 180 }} onClick={this.handlePreview.bind(this, url)} src={`${fileSource}/${url}?width=400&height=400&mode=fit`+currentTime} className='fileImage' />
                </Card>
      </div>
      }
    }
    return (
      <div className='acceptanceDetails'>
        <a onClick={this.handleHideDetails.bind(this)}><Icon type="close" className='closeIcon' /></a>
        <div className='detailsHeader'>
          <h2>查看当事人详情</h2>
        </div>
        <div className='marginTop'>
          <div className="text" >
            <div>
              <span>名称：</span><span>{unNull(appPartyUser.name)}</span>
            </div>
            <div>
              <span>统一社会信用代码：</span><span>{unNull(appPartyUser.creditCode)}</span>
            </div>
            <div>
              <span>类型：</span><span>{unNull(appPartyUser.type)}</span>
            </div>
            <div>
              <span>住所：</span><span>{unNull(appPartyUser.address)}</span>
            </div>
            <div>
              <span>法人：</span><span>{unNull(appPartyUser.legalPerson)}</span>
            </div>
            <div>
              <span>委托人：</span><span>{unNull(appPartyUser.clientName)}({unNull(appPartyUser.clientId)})</span>
            </div>
            <div>
              <span>注册资本：</span><span>{unNull(appPartyUser.registeredCapital)}</span>
            </div>
            <div>
              <span>成立时间：</span><span>{unNull(appPartyUser.establishmentTime)}</span>
            </div>
            <div>
              <span>营业期限：</span><span>{unNull(appPartyUser.businessTerm)}</span>
            </div>
            <div style={{height:60}}>
              <span>经营范围：</span><span>{unNull(appPartyUser.businessScope)}</span>
            </div>
            <div>
              <span>邮箱：</span><span>{unNull(appPartyUser.email)}</span>
            </div>
          </div>
        </div>
        <div className='marginTop'>
          <h3>补充资料</h3>
          <h4 style={{ marginTop: 16 }}>营业执照</h4>
          {showMainImage(appPartyUser.businessLicenseFileUrl)}
          <h4 style={{ marginTop: 16 }}>资产信息</h4>
          {showImage(partyFile[1])}
          <h4 style={{ marginTop: 16 }}>其他信息</h4>
          {showImage(partyFile[2])}

        </div>
        <Lightbox
          onClickImage={this.imgClick.bind(this, this.state.rotate)}
          showThumbnails={true}
          backdropClosesModal={true}
          currentImage={previewIndex}
          images={previewImage}
          isOpen={previewVisible}
          onClickPrev={() => {
            this.setState({
              previewIndex: previewIndex - 1,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClickThumbnail={(index) => {
            this.setState({
              previewIndex: index,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClose={() => { this.setState({ previewVisible: false }) }}
        />
      </div>
    )
  }
}

function unNull(value, replace = '--') {
  return value ? value : replace;
}
export default Details;
