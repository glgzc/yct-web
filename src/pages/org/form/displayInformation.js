import React from 'react'
import { Switch, Icon, Button, Upload } from 'antd'
import { config } from 'utils'
import $ from "jquery"
import Lightbox from 'react-images'
const { host, api, fileSource } = config
const { fileUpload } = api

class DisplayInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      previewVisible: false,
      previewImage: [],
      previewIndex: 0,
      rotate: 0
    };
  }
  componentDidMount() {

  }
  imgClick = (rotate) => {
    this.setState({
      rotate: rotate + 90
    })
    var pic = document.getElementsByTagName('img')
    var r = rotate + 90
    $("img").eq(pic.length - 1).css('transform', 'rotate(' + r + 'deg)');
  }
  handlePreview = (file) => {
    const arr = [{ src: file.url || file.thumbUrl, caption: '单击旋转' }]
    this.setState({
      previewImage: arr,
      previewVisible: true,
    })
  };
  render() {

    const { fileList, onTakePicture, onSwitchChange, onUploadChange,fullState,isNew} = this.props
    const { previewVisible, previewImage, previewIndex } = this.state
    const localId = sessionStorage.getItem("localId")
    const userId = sessionStorage.getItem('userId')
    const token = sessionStorage.getItem('token')
    let timestamp = new Date().getTime()
    const uploadData = {
      localId,
      userId,
      partyNumber: 0,
      type: 1,
      fileMd5: "1",
      clientUpdateAt:timestamp,
    }
    const action = host + fileUpload + "?token=" + token
    const uploadProps = {
      action,
      listType: "picture-card",
    }
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传文件</div>
      </div>
    );
    return (
      <div style={{ backgroundColor: 'white', borderRadius: 6 }}>
        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>资产信息</h2>
          <div style={{ float: 'right' }} >
          资料不完整/资料完整 &nbsp;&nbsp;&nbsp;&nbsp; <Switch onChange={(val) => onSwitchChange(val, "assetsInfoState")}  checked={fullState['assetsInfoState']}/>
            <Button style={{ paddingRight: 0, border: 0, marginLeft: 15, outline: 0 }} onClick={() => onTakePicture(1)}>
              <span>拍照上传</span>
              <Icon type="camera" />
            </Button>
          </div>
        </div>
        <div className="clearfix" style={{ padding: 15 }}>
          <Upload {...uploadProps}
            data={{
              ...uploadData,
              dataType: 1,
            }}
            onPreview={this.handlePreview}
            onChange={({ file, fileList }) => onUploadChange({ file, fileList, key: "1" })}
            fileList={fileList["1"]}>
            {uploadButton}
          </Upload>
        </div>

        <div style={{ padding: 15, borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h2 style={{ float: 'left', margin: 0 }}>其他信息</h2>
          <div style={{ float: 'right' }} >
          资料不完整/资料完整 &nbsp;&nbsp;&nbsp;&nbsp;  <Switch onChange={(val) => onSwitchChange(val, 'otherInfoState')} checked={fullState['otherInfoState']}/>
            <Button style={{ paddingRight: 0, border: 0, marginLeft: 15, outline: 0 }} onClick={() => onTakePicture(2)}>
              <span>拍照上传</span>
              <Icon type="camera" />
            </Button>
          </div>
        </div>
        <div className="clearfix" style={{ padding: 15 }}>
          <Upload {...uploadProps}
            data={{
              ...uploadData,
              dataType: 2,
            }}
            onPreview={this.handlePreview}
            onChange={({ file, fileList }) => onUploadChange({ file, fileList, key: "2" })}
            fileList={fileList["2"]}>
            {uploadButton}
          </Upload>
        </div>
        <Lightbox
          onClickImage={this.imgClick.bind(this, this.state.rotate)}
          showThumbnails={true}
          backdropClosesModal={true}
          currentImage={previewIndex}
          images={previewImage}
          isOpen={previewVisible}
          onClickPrev={() => {
            this.setState({
              previewIndex: previewIndex - 1,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')

          }}
          onClickThumbnail={(index) => {
            this.setState({
              previewIndex: index,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClose={() => { this.setState({ previewVisible: false }) }}
        />
      </div>
    )
  }
}

export default DisplayInformation;
