import React from 'react';
import { message,Form, Row, Col, Button, Input, Icon, Select } from 'antd';

import DisplayInformation from './displayInformation';

import defaultImg from '../../../assets/defaultImg.png';
import OCRImg from '../../../assets/OCRImg.png';
import NFCImg from '../../../assets/erweima.png';
const Option = Select.Option;

const { TextArea } = Input;

const AddNaturalPersonModal = ({
  form,
  fileList,
  addThePartORG,
  naturePeople,
  orgData,
  pushToPeople,
  readBarcode,
  onTakePictureBase64,
  onTakePicture,
  onUploadChange,
  toBack,
  fullState,onSwitchChange,resetData,isNew,
}) => {
  const { validateFields,getFieldDecorator, getFieldsValue, resetFields } = form;
  const goToBack = () => {
    toBack()
  };
  const reset = () => {
    resetData()
    resetFields()
  };
  const saveData = () => {
    let timestamp = new Date().getTime()
    validateFields(
      (err) => {
        if (err) {
          return
        }
        const data = {
          ...getFieldsValue(),
        };
        if(!data.legalPersonId&&!data.clientId){
          message.error("法人和委托人不能都为空",5)
          return
        }
        const localId = sessionStorage.getItem('localId');
        data.localId = localId;
        data.assetsInfoState = fullState['assetsInfoState'];
        data.otherInfoState = fullState['otherInfoState'];
        data.clientUpdateAt = timestamp
        addThePartORG(data);
      });
  };
  const toThePeople = () => {
    pushToPeople();
  };
  return (
    <div style={{ backgroundColor: '#F8F8F8', overflow: 'hidden' }}>
      {/* 头部 */}
      <div
        style={{
          overflow: 'hidden',
          background: 'white',
          padding: 15,
          marginBottom: 20,
        }}
      >
        <div style={{ float: 'left' }}>
          <Button
            style={{ fontSize: 14, display: 'block', border: 0, paddingLeft: 0 }}
            onClick={goToBack}
          >
            <Icon type="left" />
            <span>返回</span>
          </Button>
        </div>
        <div style={{ float: 'right', overflow: 'hidden' }}>
          <Button className="margin-right" onClick={reset}>
            重置
          </Button>
          <Button type="primary" onClick={saveData}>
            保存
          </Button>
        </div>
      </div>
      {/* 内容 */}
      <Row gutter={24} style={{ backgroundColor: '#F8F8F8' }}>
        {/* 基本信息 */}
        <Col md={{ span: 12 }}>
          <div
            style={{
              backgroundColor: 'white',
              padding: 15,
              borderRadius: 6,
            }}
          >
            <span>
              <h2 style={{ color: '	#000000' }}>基本信息</h2>
            </span>
            <span
              style={{
                display: 'block',

                height: 1,
                backgroundColor: '#e2e2e2',
                marginBottom: 25,
                marginTop: 14,
              }}
            />
            {/* 信息录入 */}
            <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
              <a
                href="javascript:;"
                onClick={() => readBarcode()}
                style={{
                  display: 'block',
                  textAlign: 'center',
                  width: 165,
                  height: 110,
                  backgroundColor: '	#f5f5f5',
                  marginRight: 15,
                  borderRadius: 6,
                }}
              >
                <p>
                  <img
                    style={{ width: 82, height: 58, marginTop: 10, display: 'inline-block' }}
                    src={NFCImg}
                  />
                </p>
                <p style={{ marginTop: 10 }}>二维码识别</p>
              </a>
              <a
                href="javascript:;"
                style={{
                  display: 'block',
                  textAlign: 'center',
                  width: 165,
                  height: 110,
                  backgroundColor: '	#f5f5f5',
                  marginRight: 15,
                  borderRadius: 6,
                }}
                onClick={()=>onTakePictureBase64()}
              >
                <p>
                  <img
                    style={{ width: 82, height: 58, marginTop: 10, display: 'inline-block' }}
                    src={OCRImg}
                  />
                </p>
                <p style={{ marginTop: 10 }}>OCR识别</p>
              </a>
            </div>
            {/* 材料 */}
            <p style={{ color: '	#000000', marginTop: 24 }}>统一社会信用代码</p>
            <div
               style={{
                width: 166,
                marginTop: 20,
                borderWidth: 1,
                borderColor: '#ddd',
                marginBottom: 12,
                borderStyle: 'solid',
              }}
            >
              <div style={{ backgroundColor: '#F5F5F5' }}>
                <p style={{ textAlign: 'center', padding: 15 }}>材料1 (营业执照正面照)</p>
              </div>
              
              <div
                style={{
                  margin: '14px auto',
                  backgroundColor: '#f3f3f3',
                  height: 116,
                  width: 90,
                  display: 'flex',
                }}
              >
                <img
                  style={{ width: 51, height: 63, margin: 'auto' }}
                  src={ orgData.img?orgData.img:defaultImg}
                />
              </div>
            </div>
            {/* 表单 */}
            <div>
              <Form layout={'vertical'}>
                <Form.Item label="名称">
                  {getFieldDecorator('name', {
                    initialValue: orgData.name || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="统一社会信用代码">
                  {getFieldDecorator('creditCode', {
                    initialValue: orgData.creditCode || '',
                    rules: [{ required: true, message: '请输入统一社会信用代码!' },  {
                      pattern:new RegExp(/^[0-9a-zA-Z]*$/g),
                      message:'统一社会代码填写有误，请重新填写'
                    }
                    ],
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="类型">
                  {getFieldDecorator('type', {
                    initialValue: orgData.type || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="住所">
                  {getFieldDecorator('address', {
                    initialValue: orgData.address || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="法人">
                  {getFieldDecorator('legalPerson', {
                    initialValue: orgData.legalPerson || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="注册资本">
                  {getFieldDecorator('registeredCapital', {
                    initialValue: orgData.registeredCapital || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="成立日期">
                  {getFieldDecorator('establishmentTime', {
                    initialValue: orgData.establishmentTime || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="营业期限">
                  {getFieldDecorator('businessTerm', {
                    initialValue: orgData.businessTerm || '',
                  })(
                    <Input
                      placeholder="尚未识别有效信息"
                    />,
                  )}
                </Form.Item>
                <Form.Item label=" 经营范围">
                  {getFieldDecorator('businessScope', {
                    initialValue: orgData.businessScope || '',
                  })(<TextArea rows={4} />)}
                </Form.Item>
                <Form.Item label="联系电话">
                  {getFieldDecorator('mobile', {
                    initialValue: orgData.mobile || '',
                  })(
                    <Input
                      placeholder="请输入联系电话"
                    />,
                  )}
                </Form.Item>
                <Form.Item label="邮箱">
                  {getFieldDecorator('email', {
                    initialValue: orgData.email || '',
                  })(
                    <Input
                      // disabled={true}
                      placeholder="请输入邮箱"
                    />,
                  )}
                </Form.Item>
              </Form>
            </div>
            <div style={{ overflow: 'hidden' }}>
              <div style={{ overflow: 'hidden', float: 'left' }}>
                <h2 style={{ color: '	#000000' }}>关联人资料</h2>
                <span style={{ color: '	#000000', opacity: 0.25, marginLeft: 8 }}>
                  {' '}
                  (信息二选一)
                </span>
              </div>
              <Button type="primary" onClick={toThePeople} style={{ float: 'right' }}>
                新增
              </Button>
            </div>
            <span
              style={{
                display: 'block',

                height: 1,
                backgroundColor: '#e2e2e2',
                marginBottom: 25,
                marginTop: 14,
              }}
            />
            <Form
              layout={'horizontal'}
              style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}
            >
              <Form.Item label=" 选择法人:">
                {getFieldDecorator('legalPersonId', {
                  initialValue: orgData.legalPersonId?orgData.legalPersonId+'': '',
                })(
                  <Select style={{ width: 120 }}>
                    {naturePeople.map(item => (
                      <Option value={item.idCard}>{item.name}</Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Form>
            <Form
              layout={'horizontal'}
              style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}
            >
              <Form.Item label=" 选择委托人:">
                {getFieldDecorator('clientId', {
                initialValue:orgData.clientId? orgData.clientId+'': '',
                })(
                  <Select style={{ width: 120 }}>
                    {naturePeople.map(item => (
                      <Option value={item.idCard}>{item.name}</Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Form>
          </div>
        </Col>
        <Col md={{ span: 12 }}>
          <DisplayInformation
            onSwitchChange={(val, type) => onSwitchChange(val, type)}
            fullState={fullState}
            fileList={fileList}
            isNew={isNew}
            onTakePicture={type => onTakePicture(type)}
            onUploadChange={({ file, fileList, key }) => onUploadChange({ file, fileList, key })}
          />
        </Col>
      </Row>
    </div>
  );
};

export default Form.create()(AddNaturalPersonModal);
