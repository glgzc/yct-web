import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Form, Button, Row, Col, Input } from 'antd'

const Search = Input.Search

const ColProps = {
  xs: 24,
  sm: 12,
  style: {
    marginBottom: 16,
  },
}

const TwoColProps = {
  ...ColProps,
  xl: 36,
}
const ThreeColProps = {
  ...ColProps,
  xl: 96,
}
const Filter = ({
  isMotion,
  switchIsMotion,
  onFilterChange,
  filter,
  addToORG,
  form: {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
  },
}) => {
  const handleFields = (fields) => {
    // const { createTime } = fields
    // if (createTime.length) {
    //   fields.createTime = [createTime[0].format('YYYY-MM-DD'), createTime[1].format('YYYY-MM-DD')]
    // }
    return fields
  }

  const handleSubmit = () => {
    let fields = getFieldsValue()
    fields = handleFields(fields)
    onFilterChange(fields)
  }

  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const handleChange = (key, values) => {
    let fields = getFieldsValue()
    fields[key] = values
    fields = handleFields(fields)
    onFilterChange(fields)
  }
  const { name, address } = filter

  let initialCreateTime = []
  if (filter.createTime && filter.createTime[0]) {
    initialCreateTime[0] = moment(filter.createTime[0])
  }
  if (filter.createTime && filter.createTime[1]) {
    initialCreateTime[1] = moment(filter.createTime[1])
  }
  const addORG = () => {
    addToORG()
  }
  return (
    <Row gutter={24} style={{ marginBottom: '15px' }}>
      <Col {...ColProps} md={8}>
        {getFieldDecorator('keywords', { initialValue: name })(<Search placeholder="机构名称或识别码" onSearch={handleSubmit} />)}
      </Col>

      <Col {...TwoColProps} md={6} >
        <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
          <div >
            <Button type="primary" className="margin-right" onClick={handleSubmit}>查询</Button>
          </div>
        </div>
      </Col>
      {/* <Col {...ThreeColProps} md={4} > */}
      <div style={{ textAlign: 'right', marginRight: 12 }}>
        <Button type="primary" onClick={addORG}>添加机构</Button>
      </div>
      {/* </Col> */}
    </Row>

  )
}

Filter.propTypes = {
  isMotion: PropTypes.bool,
  switchIsMotion: PropTypes.func,
  form: PropTypes.object,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func,
}

export default Form.create()(Filter)
