import React from 'react';
import PropTypes from 'prop-types';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import { Tabs } from 'antd';
import List from './List';
import Filter from './Filter';
import Modal from './detailsModal';
import Details from './Details';
import EditView from './form'
const Org = ({ location, dispatch, org, loading }) => {
  const {
    visibleDetailsModal,
    detailsInfo,
    list,
    pagination,
    currentItem,
    visibleDetails,
    currentInfo,
    modalVisible,
    modalType,
    isMotion,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    currentApiItem,
    currentMethod,
    showAddORG,
    naturePeople,
    showEdit,
    keywords
  } = org;
  const { pageSize } = pagination;
  const modalProps = {
    currentApiItem,
    modalType,
    currentMethod: currentMethod === '' ? currentItem.method : currentMethod,
    item: modalType === 'update' ? currentItem : {},
    visible: modalVisible,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    maskClosable: false,
    confirmLoading: loading.effects['org/update'],
    title: `${
      modalType === 'create'
        ? visibleCreateApi
          ? '添加服务接口'
          : '服务接口'
        : modalType === 'update'
          ? '编辑服务接口----' + currentItem.interfaceName
          : modalType === 'updateApi'
            ? '编辑服务接口'
            : '添加价格组'
      }`,
    wrapClassName: 'vertical-center-modal',
    onOk(data) {
      dispatch({
        type: `org/${modalType}`,
        payload: data,
      });
    },
    onCancel() {
      dispatch({
        type: 'org/hideModal',
      });
    },
    createOk() {
      dispatch({
        type: 'org/showCreateApi',
        payload: {
          currentApiItem: {},
          modalType: 'create',
        },
      });
    },
    createCancel() {
      dispatch({
        type: 'org/hideCreateApi',
        payload: {
          modalType: 'create',
        },
      });
    },
    onDeleteApi(data) {
      dispatch({
        type: 'org/deleteInterface',
        payload: data,
      });
    },
    onEdit(data) {
      dispatch({
        type: 'org/showUpdateApi',
        payload: {
          currentApiItem: data,
          modalType: 'updateApi',
        },
      });
    },
    onEditOk(data) {
      dispatch({
        type: 'org/updateInterface',
        payload: {
          modalType: 'create',
          data,
        },
      });
    },
    onChangeMethod(value) {
      dispatch({
        type: 'org/currentMethod',
        payload: {
          currentMethod: value,
        },
      });
    },
  };
  const listProps = {
    list,
    loading: loading.effects['org/query'],
    pagination,
    location,
    isMotion,
    dispatch,
    onChange(page) {
      dispatch({
        type: 'org/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
          keywords:keywords,
        }
      })
    },
    onRowClick(record) {
      dispatch({
        type: 'org/info',
        payload: {
          currentItem: record,
          visibleDetails: true,
        },
      });
    },
    getEditData(record) {
      dispatch({
        type: 'org/getEdit',
        payload: {
          currentItem: record,
        }
      },
      )
    },
  };

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'org/queryKeyWords',
        payload: {
         ...value
        },
      })
    },
    onSearch(fieldsValue) {
      fieldsValue.keyword.length
        ? dispatch(
          routerRedux.push({
            pathname: '/org',
            query: {
              field: fieldsValue.field,
              keyword: fieldsValue.keyword,
            },
          }),
        )
        : dispatch(
          routerRedux.push({
            pathname: '/org',
          }),
        );
    },
    switchIsMotion() {
      dispatch({ type: 'org/switchIsMotion' });
    },
    addToORG() {
      dispatch({
        type: 'org/newORG',
        payload: {
        }
      },
      )
    },
  };

  const detailsProps = {
    currentItem,
    currentInfo,
    onHideDetails() {
      dispatch({
        type: 'org/hideDetails',
        payload: {
          visibleDetails: false,
        },
      });
    },
    showDetailModal(record) {
      dispatch({
        type: 'org/showDetailsModal',
        payload: {
          ...record,
          visibleDetailsModal: true,
        },
      });
    },
    hideDetailModal() {
      dispatch({
        type: 'org/hideDetailsModal',
        payload: {
          visibleDetailsModal: false,
        },
      });
    },
  };
  const detailsModalProps = {
    visibleDetailsModal,
    detailsInfo,
    title: <div style={{ textAlign: 'center' }}>当事人信息详情</div>,
    okText: '收起',
    footer: null,
    onOk() {
      dispatch({
        type: 'org/hideDetailsModal',
      });
    },
  };

  return (
    <div style={{ overflow: 'auto' }}>
      {showEdit ? <EditView /> : <div className="content">
        <div className="content-left">
          <Filter {...filterProps} />
          <List {...listProps} />
        </div>
        {visibleDetails ? (
          <div className="content-right">
            <Details {...detailsProps} />
          </div>
        ) : null}
        {visibleDetailsModal ? <Modal {...detailsModalProps} /> : null}
      </div>}
    </div>
  );
};

Org.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
};

export default connect(({ org, loading }) => ({ org, loading }))(Org);
