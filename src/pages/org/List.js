import React from 'react'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'

import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'
import '../../utils/index'

const confirm = Modal.confirm

const List = ({ list, onRowClick, getEditData,...tableProps }) => {
  const handleMenuClick = (record, e) => {
    getEditData(record)
  };
  const onRowClicks = (record, e) => {
    onRowClick(record)
  }
  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
    }, 
    {
      title: '统一社会信用代码',
      dataIndex: 'creditCode',
      key: 'creditCode',
    }, 
    {
      title: '法人',
      dataIndex: 'legalPerson',
      key: 'legalPerson'
    }, {
      title: '电话',
      dataIndex: 'mobile',
      key: 'mobile'
    }, {
      title: '注册资本',
      dataIndex: 'registeredCapital',
      key: 'registeredCapital'
    }, 
    {
      title: '类型',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (
          <div style={{overflow:'hidden'}}>
            <a onClick={e => handleMenuClick(record, e)} style={{display:'inline-block'}}>编辑</a>
            <a onClick={e => onRowClicks(record, e)} style={{display:'inline-block',marginTop:10}}>查看详情</a>
          </div>
        )
      },
    },
  ]
  return (
    <div style={{marginTop:'25px'}}>
      <Table
        dataSource={list}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={row => row.id}
        pagination={tableProps.pagination}
        onChange={tableProps.onChange}
      />
    </div>
  )
}

export default List
