import React from 'react'
import { Table, Icon, Tag, Modal } from 'antd'
import { DropOption } from 'components'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onEditItem, onDeleteItem, ...tableProps }) => {
  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该条记录?',
        onOk () {
          onDeleteItem(record.menuId)
        },
      })
    }
  }
  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
      className: styles.left,
      width: '20%',
    }, {
      title: '排序',
      dataIndex: 'orderNum',
      width: '30px',
    }, {
      title: '图标',
      dataIndex: 'icon',
      render: text => <Icon type={text} />,
      width: '60px',
    }, {
      title: '类型',
      dataIndex: 'type',
      render: (text) => {
        if (text == 0) {
          return <Tag color="blue">目录</Tag>
        }
        if (text == 1) {
          return <Tag color="green">菜单</Tag>
        }
        if (text == 2) {
          return <Tag color="orange">按钮</Tag>
        }
      },
      width: '60px',
    }, {
      title: '路由',
      dataIndex: 'url',
      width: '20%',

    }, {
      title: '授权标识',
      dataIndex: 'perms',
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (<DropOption
          dropdownProps={{
            trigger: ['click'],
          }}
          onMenuClick={e => handleMenuClick(record, e)}
          menuOptions={[{ key: '1', name: '修改' }, { key: '2', name: '删除' }]}
        />)
      },
    },
  ]

  return (
    <Table
      {...tableProps}
      bordered
      columns={columns}
      className={styles.table}
      simple
      rowKey={record => record.menuId}
    />
  )
}

export default List
