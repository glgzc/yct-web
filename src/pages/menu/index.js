import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Tabs } from 'antd'
import { routerRedux } from 'dva/router'
import Filter from './Filter'
import List from './List'
import Modal from './Modal'

const Menu = ({ menu, dispatch, loading, location }) => {
  const { list, pagination, selectMenus, currentItem, modalVisible, modalType, selectedRowKeys } = menu
  const { query = {}, pathname } = location
  const modalProps = {
    modalType,
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    selectMenus,
    maskClosable: false,
    confirmLoading: loading.effects['role/update'],
    title: `${modalType === 'create' ? '创建菜单' : '编辑菜单'}`,
    wrapClassName: 'vertical-center-modal',

    onOk (data) {
      dispatch({
        type: `menu/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'menu/hideModal',
      })
    },
  }
  const listProps = {
    pagination: false,
    dataSource: list,
    loading: loading.effects['menu/query'],
    onChange (page) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }))
    },
    onDeleteItem (id) {
      dispatch({
        type: 'menu/delete',
        payload: {
          id,
        },
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'menu/edit',
        payload: {
          id: item.menuId,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'menu/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }
  const filterProps = {
    onAdd () {
      dispatch({
        type: 'menu/add',
        payload: {
          modalType: 'create',
        },
      })
    },
  }


  return (<div className="content-inner">
    <Filter {...filterProps} />
    <List {...listProps} />
    {modalVisible && <Modal {...modalProps} />}
  </div>)
}

Menu.propTypes = {
  menu: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default connect(({ menu, loading }) => ({ menu, loading }))(Menu)
