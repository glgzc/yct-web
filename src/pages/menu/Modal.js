import React from 'react'
import PropTypes from 'prop-types'
import lodash from 'lodash'
import { Form, Input, InputNumber, Radio, Modal, Tree, Select, TreeSelect } from 'antd'

const TreeNode = Tree.TreeNode
const FormItem = Form.Item
const RadioGroup = Radio.Group

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const modal = ({
  item = {},
  selectMenus,
  onOk,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  modalType,
  ...modalProps
}) => {
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: item.key,
      }
      onOk(data)
    })
  }

  const modalOpts = {
    ...modalProps,
    onOk: handleOk,
  }

  const arrayToTree = (array, id = 'id', pid = 'pid', children = 'children') => {
    let data = lodash.cloneDeep(array)
    let result = []
    let hash = {}
    data.forEach((item, index) => {
      hash[data[index][id]] = data[index]
    })

    data.forEach((d) => {
      // d.value=d.menuId
      // d.label=d.name
      if (item.menuId == d.menuId) {
        return
      }
      let hashVP = hash[d[pid]]
      if (hashVP) {
        !hashVP[children] && (hashVP[children] = [])
        hashVP[children].push(d)
      } else {
        result.push(d)
      }
    })
    return result
  }
  const options = arrayToTree(selectMenus, 'menuId', 'parentId')
  const loop = data => data.map((d) => {
    if (d.children) {
      return (
        <TreeNode key={String(d.menuId)} title={d.name} value={String(d.menuId)}>
          {loop(d.children)}
        </TreeNode>
      )
    }
    return <TreeNode key={String(d.menuId)} title={d.name} value={String(d.menuId)} />
  })
  return (
    <Modal {...modalOpts}>

      <Form layout="horizontal">
        <FormItem
          {...formItemLayout}
          label="类型"
        >
          {getFieldDecorator('type', {
            initialValue: item.type == undefined ? '1' : String(item.type),
          })(
            <RadioGroup>
              <Radio value="0">目录</Radio>
              <Radio value="1">菜单</Radio>
              <Radio value="2">按钮</Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem label="菜单名称" hasFeedback {...formItemLayout}>
          {getFieldDecorator('name', {
            initialValue: item.name,
            rules: [
              {
                required: true,
              },
            ],
          })(<Input />)}
        </FormItem>

        <FormItem label="上级菜单" hasFeedback {...formItemLayout}>
          {getFieldDecorator('parentId', {
            initialValue: item.parentId == undefined ? null : String(item.parentId),
            rules: [
              {
                required: true,
              },
            ],
          })(
            <TreeSelect
              dropdownStyle={{ maxHeight: 250, overflow: 'auto' }}
              placeholder="请选择父级菜单"
              allowClear
            >
              <TreeNode value="0" title="根节点" key="0">
                {loop(options)}
              </TreeNode>
            </TreeSelect>
          )}
        </FormItem>

        <FormItem label="菜单URL" hasFeedback {...formItemLayout}>
          {getFieldDecorator('url', {
            initialValue: item.url,
            rules: [
              {
                required: getFieldsValue().type == 1,
              },
            ],
          })(<Input />)}
        </FormItem>
        <FormItem label="授权标识" hasFeedback {...formItemLayout}>
          {getFieldDecorator('perms', {
            initialValue: item.perms,
            rules: [
              {
                required: getFieldsValue().type == 2,
              },
            ],
          })(<Input />)}
        </FormItem>
        <FormItem label="排序号" hasFeedback {...formItemLayout}>
          {getFieldDecorator('orderNum', {
            initialValue: item.orderNum,
            rules: [
              {
                required: true,
              },
            ],
          })(<Input />)}
        </FormItem>
        <FormItem label="图标" hasFeedback {...formItemLayout}>
          {getFieldDecorator('icon', {
            initialValue: item.icon,
            rules: [
              {
                required: getFieldsValue().type !=2,
              },
            ],
          })(<Input />)}
        </FormItem>
      </Form>
    </Modal>
  )
}

modal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(modal)
