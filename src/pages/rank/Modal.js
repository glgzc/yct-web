
import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Radio, Modal, Select } from 'antd';
import {IEVersion} from 'utils/util';
const FormItem = Form.Item
const Option = Select.Option

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const modal = ({
  item = {},
  onOk,
  onChange,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  treeSelectData,
  ...modalProps
}) => {
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: item.key,
      }
      if (!data) return;
      for (const key in data) {
        if (data[key] === undefined) {
          data[key] = ''
        }
      }
        data.id = item.id 
      onOk(data)
    })
  }

  const handleChange = (value) => {
    onChange(value)
  }

  const modalOpts = {
    ...modalProps,
    onOk: handleOk,
  };
  return (
    <Modal {...modalOpts} style={IEVersion()?{right:"30%"}:{}}>
      <Form layout="horizontal">
        <FormItem label="职级名称" hasFeedback {...formItemLayout}>
          {getFieldDecorator('position', {
            initialValue: item.position,
            rules: [
              {
                required: true,
                message: '请输入职级名称'
              },
            ],
          })(<Input autoComplete={false} />)}
        </FormItem>
        <FormItem label="所属机构" hasFeedback {...formItemLayout}>
          {getFieldDecorator('orgId', {
            initialValue: item.orgId ? item.orgId + '': '',
            rules: [
              {
                required: true,
                message: '请选择所属机构'
              },
            ],
          })(
            <Select
              onChange={handleChange}
            >
              {
                (treeSelectData && treeSelectData.length) && treeSelectData.map(element => {
                  return <Option key={element.id+''} type='option'>{element.name}</Option>
                })
              }
            </Select>
          )}
        </FormItem>
      </Form>
    </Modal>
  )
}

modal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(modal)
