import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'

const Rank = ({ location, dispatch, rank, loading }) => {

  const { list, pagination, currentItem, modalVisible, modalType, isMotion, selectedRowKeys, treeSelectData } = rank
  const { pageSize } = pagination
  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['rank/update'],
    title: `${modalType === 'create' ? '添加职级' : '编辑职级信息'}`,
    wrapClassName: 'vertical-center-modal',
    treeSelectData,
    onOk (data) {
      dispatch({
        type:"rank/create",
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'rank/hideModal',
      })
    },
    onChange(data) {
      dispatch ({
        type: 'rank/roleList',
        payload: data
      })
    }
  }

  const listProps = {
    pagination:false,
    dataSource: list,
    loading: loading.effects['rank/query'],
    location,
    isMotion,
    onChange (page) {
      const { query, pathname } = location
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }))
    },
    onDeleteItem (id) {
      dispatch({
        type: 'rank/delete',
        payload: id,
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'rank/modal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'rank/queryKeyWords',
        payload: {
          keywords: value.keywords,
        },
      })
    },
    onSearch (fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/rank',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/rank',
      }))
    },
    onAdd () {
      dispatch({
        type: 'rank/modal',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion () {
      dispatch({ type: 'rank/switchIsMotion' })
    },
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'rank/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }

  return (
    <div className="content-inner">
      <Filter {...filterProps} />
      {
        selectedRowKeys.length > 0 &&
        <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
          <Col>
            {`Selected ${selectedRowKeys.length} items `}
            <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
              <Button type="primary" style={{ marginLeft: 8 }}>Remove</Button>
            </Popconfirm>
          </Col>
        </Row>
      }
      <List {...listProps} />
      {modalVisible && <Modal {...modalProps} />}
    </div>
  )
}

export default connect(({ rank, loading }) => ({ rank, loading }))(Rank)
