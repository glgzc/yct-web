import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form,Tag } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onDeleteItem, onEditItem,editingKey, ...tableProps }) => {

  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该用户吗?',
        onOk () {
          onDeleteItem(record.userId)
        },
      })
    }
  };
  const columns = [
    {
      title: '所属机构',
      dataIndex: 'organizationName',
      key: 'organizationName'
    }, {
      title: '用户名',
      dataIndex: 'username',
      key: 'username'
    }, {
      title: '角色',
      dataIndex: 'roleList',
      key: 'roleList',
      width: '20%',
      render: list=>{
        return (
          <div>
            {
              list && list.map((item)=><Tag color='blue' style={{marginTop: '5px'}} key={item.roleId}>{item.roleName}</Tag>)
            }
          </div>
        )
      }
    }, {
      title: '手机号码',
      dataIndex: 'mobile',
      key: 'mobile'
    }, {
      title: '真实姓名',
      dataIndex: 'realName',
      key: 'realName'
    }, {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        if(text==1){
          return (
            <div>
              <span className={styles.status} style={{backgroundColor: '#00a854'}}></span>
              正常
            </div>
          )
        }
        return (
          <div>
              <span className={styles.status} style={{backgroundColor: 'red'}}></span>
              禁用
            </div>
        )
      },
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return <DropOption onMenuClick={e => handleMenuClick(record, e)} menuOptions={[{ key: '1', name: '编辑' }, { key: '2', name: '删除' }]} />
      },
    },
  ]


  return (
    <div>
      <Table
        {...tableProps}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={record => record.userId}
      />
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
}

export default List
