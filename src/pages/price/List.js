import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onSetting, onEditItem, group, ...tableProps }) => {
  if(!tableProps.list[group]){
    return null
  }
  const datas = tableProps.list[group].priceSettingEntityList
  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      let title = ''
      if(record.id&&record.id!==0){
        title = '确定要禁用'+record.interfaceName+'吗？'
      }else{
        title = '确定要启用'+record.interfaceName+'吗？'        
      }
      
      confirm({
        title,
        onOk () {
          onSetting(record)
        },
      })
    }
  }

  const columns = [
    {
      title: '名称',
      dataIndex: 'interfaceName',
      key: 'interfaceName'
    }, {
      title: '收费方式',
      dataIndex: 'method',
      key: 'method',
      render: (text)=>{
        if(text===0){
          return '按调用'
        }else if(text===1){
          return '包断'
        }
      }
    }, {
      title: '价格',
      dataIndex: 'price',
      key: 'price',
      render: (text)=>{
        return text/100+' 元'
      }
    },{
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        let status = ''
        let visible = ''
        if(record.id&&record.id!==0){
          status='禁用'
          visible = false
        }else{
          status='启用'
          visible = true
        }
        return <DropOption onMenuClick={e => handleMenuClick(record, e)} menuOptions={[{ key: '1', name: '编辑', disabled: visible }, {key:'2',name:status}]} />
      },
    },
  ]


  return (
    <div style={{marginTop:'25px'}}>
      <Table
        {...tableProps}
        dataSource={datas}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowClassName={record=>record.id && record.id!==0 ? 'enableStyle' : 'disableStyle'}
        rowKey={record => record.roleId}
        rowSelection={false}
      />
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
  key: PropTypes.number,
}

export default List
