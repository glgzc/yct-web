import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm, Tabs, Modal as AntdModal  } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'
import {isAdmin} from 'utils/util';
const confirm = AntdModal.confirm

const Price = ({ location, dispatch, price, loading }) => {
  const { 
    list, 
    pagination, 
    currentItem,
    modalVisible, 
    modalType, 
    isMotion, 
    selectedRowKeys,

    priceGroupList,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    currentApiItem,
    currentMethod
  } = price

  const { pageSize } = pagination

  const modalProps = {
    currentApiItem,
    modalType,
    currentMethod: currentMethod === '' ? currentItem.method : currentMethod,
    item: modalType === 'update' ? currentItem : {},
    visible: modalVisible,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    maskClosable: false,
    confirmLoading: loading.effects['price/update'],
    title: `${modalType === 'create' ? (visibleCreateApi ? '添加服务接口' : '服务接口') : (modalType==='update'?'编辑服务接口----'+currentItem.interfaceName: (modalType==='updateApi'? '编辑服务接口' : '添加价格组' ))}`,
    wrapClassName: 'vertical-center-modal',
    onOk (data) {
      dispatch({
        type: `price/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'price/hideModal',
      })
    },
    createOk() {
      dispatch({
        type: 'price/showCreateApi',
        payload: {
          currentApiItem: {},
          modalType: 'create'
        }
      })
    },
    createCancel() {
      dispatch({
        type: 'price/hideCreateApi',
        payload: {
          modalType: 'create'
        }
      })
    },
    onDeleteApi(data) {
      dispatch({
        type: 'price/deleteInterface',
        payload: data
      })
    },
    onEdit(data) {
      dispatch({
        type: 'price/showUpdateApi',
        payload: {
          currentApiItem: data,
          modalType: 'updateApi'
        }
      })
    },
    onEditOk(data){
      dispatch({
        type: 'price/updateInterface',
        payload: {
          modalType: 'create',
          data
        }
      })
    },
    onChangeMethod(value){
      dispatch({
        type: 'price/currentMethod',
        payload: {
          currentMethod: value
        }
      })
    }
  }

  const listProps = {
    list,
    loading: loading.effects['price/query'],
    pagination,
    location,
    isMotion,
    dispatch,
    onChange(page) {
      dispatch({
        type: 'price/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
        }
      })
    },
    onSetting (record) {
      let settingType = ''
      let data = {}
      if(record.id && record.id!==0){
        settingType = 'settingDisable'
        data = {
          id: record.id
        }
      }else{
        settingType = 'settingEnable'
        data = {
          method: record.method,
          price: record.price,
          interfaceId: record.interfaceId,
          priceGroupId: record.priceGroupId
        }
      }
      dispatch({
        type: `price/${settingType}`,
        payload: data,
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'price/showUpdateApi',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'price/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange (value) {
      dispatch(routerRedux.push({
        pathname: location.pathname,
        query: {
          ...value,
          page: 1,
          pageSize,
        },
      }))
    },
    onSearch (fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/price',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/price',
      }))
    },
    onAdd () {
      dispatch({
        type: 'price/interfaceList',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion () {
      dispatch({ type: 'price/switchIsMotion' })
    },
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'price/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }

  const editPriceGroup=(targetKey, action)=>{
    if(action==='remove'){
      confirm({
        title: '确认删除该价格组吗？？',
        onOk () {
          dispatch({
            type: 'price/removePriceGroup',
            payload: {
              priceGroupId: parseInt(targetKey),
            },
          })
        },
      })
    }else if(action==='add'){
      dispatch({
        type: 'price/showPriceGroup',
        payload: {
          modalType: 'createPriceGroup',
        },
      })
    }
  }

  return (
    <div className="content-inner">
      <Filter {...filterProps} />
      <Tabs
      type='editable-card'
      defaultActiveKey='1'
      onEdit={editPriceGroup}>
          {priceGroupList.map((priceGroup,index) => (
            <Tabs.TabPane tab={priceGroup.title} key={priceGroup.priceGroupId} closable={priceGroup.isRemove===1?true:false}>
              <List {...listProps} group={index} />
              {modalVisible && <Modal {...modalProps} />}
            </Tabs.TabPane>
          ))}
        </Tabs>
    </div>
  )
}

Price.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ price, loading }) => ({ price, loading }))(Price)
