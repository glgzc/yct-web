import React from 'react';
import { connect } from 'dva'
import ClearingFilter from './Filter';
import ClearingList from './List';
import ClearingReport from './Report';
import './index.less';
import { message,Modal } from 'antd';
class ClearingManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            reportTarget: null,
            filter: {}
        }
        this.handleFilter = this.handleFilter.bind(this);
        this.handleCheckReport = this.handleCheckReport.bind(this);
        this.handleCloseReport = this.handleCloseReport.bind(this);
    }
    // shouldComponentUpdate(nextProps, nextState) {
    //     return nextProps.clearing.list.length !== this.props.clearing.list.length
    //         || nextProps.clearing.pageIndex !== this.props.clearing.pageIndex
    //         || nextState.modalVisible !== this.state.modalVisible;
    // }
    handleCheckReport = (target) => {
        !this.state.modalVisible && this.setState({ modalVisible: true, reportTarget: target });
    }
    handleCloseReport = () => {
        this.state.modalVisible && this.setState({ modalVisible: false, reportTarget: null });
    }
    handleFilter = (params) => {
        this.setState({
            filter: params
        })
        this.props.dispatch({
            type: 'clearing/load',
            params: params,
        })
    }
    handleTurnPage = (index,pageSize) => {
        const params = Object.assign({}, this.state.filter);
        params.page = index;
        params.pageSize=pageSize||10
        this.props.dispatch({
            type: 'clearing/load',
            params: params
        })
    }
    handleRemove=()=>{
        Modal.confirm({
            title: '提示',
            content: '条目删除后无法恢复，确认删除？',
            okText: '确认',
            cancelText: '取消',
            onOk:()=>{
                const ids=this.refs.list.selectedRowKeys||[]
                if(ids.length===0){
                    message.error('请选择要删除的条目')
                    return
                }
                this.props.dispatch({
                    type: 'clearing/delete',
                    payload: {
                        id:ids
                    }
                })
            }
        });
        
    }
    handleRemoveAll=(params)=>{
        Modal.confirm({
            title: '提示',
            content: '条目删除后无法恢复，确认删除？',
            okText: '确认',
            cancelText: '取消',
            onOk:()=>{
                if(!params.startTime && !params.endTime){
                    message.error('请选择时间段')
                    return
                }
                this.props.dispatch({
                    type: 'clearing/delete',
                    payload:{
                        ...params
                    }
                })
            }
        });
        
    }
    render() {
        const { clearing } = this.props;
        return (
            <div className="content-inner" >
                <ClearingFilter 
                    typeOptions={clearing.type} 
                    onFilter={this.handleFilter} 
                    onRemove={this.handleRemove}
                    onRemoveAll={this.handleRemoveAll}
                />
                <ClearingList 
                    ref='list'
                    list={clearing.list} 
                    checkReport={this.handleCheckReport} 
                    loading={clearing.loading}
                    total={clearing.total}
                    pageIndex={clearing.pageIndex}
                    turnPage={this.handleTurnPage}/>
                <ClearingReport visible={this.state.modalVisible} target={this.state.reportTarget} closeReport={this.handleCloseReport} />
            </div>
        )
    }
}
export default connect(({ clearing, loading}) => ({ clearing }))(ClearingManager);
