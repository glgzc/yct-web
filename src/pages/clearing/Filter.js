import React from 'react';
import { Row, Col, Input, DatePicker, Select, Button } from 'antd';
import moment from 'moment';

const Search = Input.Search;
const RangePicker = DatePicker.RangePicker;
const Option = Select.Option;
class ClearingFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keywords: null,
            type: null,
            startTime: null,
            endTime: null,
        };
        this.handleKeyWordChange = this.handleKeyWordChange.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    // shouldComponentUpdate(nextProps) {
    //     return nextProps.typeOptions.length !== this.props.typeOptions.length;
    // }
    handleKeyWordChange = (e) => {
        this.setState({
            keywords: e.target.value
        })
    }
    handleTypeChange = (typeId) => {
        this.setState({
            type: parseInt(typeId)
        })
    }
    handleDateChange = (date, dateString) => {
        this.setState({
            startTime: dateString[0],
            endTime: dateString[1]
        });
    }
    handleSearch = () => {
        const params = {};
        for(let item in this.state) {
            if(this.state[item]) params[item] = this.state[item];
        }
        this.props.onFilter(params);
    }
    handleSearchKeyWordOnly = (keywords) => {
        this.props.onFilter({ keywords });
    }
    handleRemove=()=>{
        this.props.onRemove && this.props.onRemove()
    }
    handleRemoveAll=()=>{
        const {startTime,endTime}=this.state
        this.props.onRemoveAll && this.props.onRemoveAll({
            startTime,
            endTime
        })
    }
    render() {
        const styles = {
            layout: {
                marginBottom: 12,
                paddingRight: 12
            },
            selectWidth: {
                width: '100%'
            }
        };
        return (
            <div style={styles.layout}>
                <Row gutter={24}>
                    <Col md={6}>
                        <Search placeholder="当事人、负责人"
                            onChange={this.handleKeyWordChange}
                            onSearch={this.handleSearchKeyWordOnly}></Search>
                    </Col>
                    <Col md={4}>
                        <Select
                            showSearch
                           
                            style={styles.selectWidth}
                            placeholder="全部"
                            allowClear
                            optionFilterProp="children"
                            onChange={this.handleTypeChange}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {
                                this.props.typeOptions.map(item => <Option key={String(item.interfaceId)} value={String(item.interfaceId)}>
                                    {item.interfaceName}
                                </Option>)
                            }
                        </Select>
                    </Col>
                    <Col md={6}>
                        <RangePicker
                           
                            ranges={{
                                '今天': [moment(), moment()],
                                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                '近一周': [moment().subtract(6, 'days'), moment()],
                                '近一月': [moment().subtract(29, 'days'), moment()],
                                '当月': [moment().startOf('month'), moment().endOf('month')],
                                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            }}
                            onChange={this.handleDateChange}
                        />
                    </Col>
                    <Col md={8}>
                        <Button type="primary" icon="search"
                            onClick={this.handleSearch}>查询</Button>
                            &nbsp; &nbsp;
                            <Button type="primary" 
                            onClick={this.handleRemove}>删除所选</Button>
                            &nbsp; &nbsp;
                            <Button type="danger"
                            onClick={this.handleRemoveAll}>删除全部</Button>
                    </Col>
                    <Col md={2} offset={4}>
                        {/* 暂时无该功能 */}
                        {/* <Button type="ghost" icon="file">导出</Button> */}
                    </Col>
                </Row>
            </div>
        )
    }
}
export default ClearingFilter;