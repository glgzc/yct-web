import React from 'react';
import { Modal, Card, Button, Form, Row, Col } from 'antd';
import PrintProvider, { Print, NoPrint } from 'react-easy-print';
import { config } from 'utils';
import { IEVersion } from 'utils/util';
const { fileSource } = config;
class ClearingReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalConfirmLoading: false
        };
        this.handlePrint = this.handlePrint.bind(this);
    }
    handlePrint = () => {
        // if (!!window.ActiveXObject || "ActiveXObject" in window) {
        //     document.body.className += ' ext-ie';
        //     document.execCommand('print', false, null);
        // } else {
            window.print();
        // }
    }
    renderLocation = (jsonLocation) => {
        const location = jsonLocation ? JSON.parse(jsonLocation) : {};
        return <div style={{ textAlign: 'right' }}>
            <div style={styles.locationTag}>采集地址: {location.lat ? (location.lat + ',' + location.lng) : '--'} </div>
            <span style={styles.locationTag}>{location.location ? location.location + '附近' : ''}</span>
        </div>
    }
    render() {
        const { visible, target, closeReport } = this.props;
        let currentTime = '&' + new Date().getTime()
        if (!target) return null;
        // const date = new Date(target.createdTime);
        const footer = <NoPrint force>
            <div style={styles.footer} className="noprint">
                <Button type='primary' style={styles.footerButton} onClick={this.handlePrint}>打印</Button>
            </div>
        </NoPrint>
        return (

            <Modal
                visible={visible}
                width={768}
                footer={footer}
                onCancel={closeReport}
                destroyOnClose
                mask
                maskClosable={true}
            >
                <PrintProvider>
                    <Print single name="report">
                        <div style={styles.layout}>
                            <div style={styles.title}><h1>当事人 <b>{target.partyUserName}</b> 的人证比对报告</h1></div>
                            {this.renderLocation(target.location)}
                            <Card title="当事人身份证现场采集信息" style={styles.reportCard}>
                                <div style={styles.photoRow}>
                                    <div style={styles.info}>
                                        <InfoLayout info={target} />
                                    </div>
                                    <img style={styles.avatar} src={target.cardPositiveFileUrl.indexOf(',')!= -1?`${fileSource}/${target.cardPositiveFileUrl}?width=300&height=336&mode=fit` + currentTime:`data:image/png;base64,${target.cardPositiveFileUrl}`} />
                                </div>
                            </Card>
                            <Card title="当事人现场照片信息" style={styles.reportCard}>
                                <div style={styles.photoRow}>
                                    <img style={
                                        IEVersion() ? styles.photoNow :
                                            styles.photoNows} src={`${fileSource}/${target.image2}?width=300&height=336&mode=fit` + currentTime} />
                                    <div style={styles.analyzeReport}>
                                        <h2>本次人工智能对比置信度</h2>
                                        <br />
                                        <div style={styles.score}>{target.coincidenceDegree}</div>
                                        <br />
                                        <div style={styles.explain}>
                                            <h3>置信度参考说明: </h3>
                                            <br />
                                            <p>置信度分数 &gt; 90：相似度极高；</p>
                                            <br />
                                            <p>90 &lt; 置信度分数 &gt; 80：相似度很高；</p>
                                            <br />
                                            <p>80 &gt; 置信度分数 &gt; 70：相似度高；</p>
                                            <br />
                                            <p>置信度分数 &lt; 70：相似度低，请重新比对或通过其他方式核实；</p>
                                        </div>
                                    </div>
                                </div>
                            </Card>
                            <h3 style={styles.oath}>
                                公证员为本人在申办公证业务时录入的所有信息，均经本人同意进行采集，并现场核对，确认信息属实无误。
                            </h3>
                            <div style={styles.sign}>

                                <div style={{ flex: '1', display: 'flex', alignItems: 'center' }}>
                                    <h3>签名: </h3>
                                    {
                                        target.autograph ? <img src={target.autograph.indexOf(',')!= -1?`${fileSource}/${target.autograph}?width=300&height=336&mode=fit` + currentTime:confirmBase64(target.autograph)} style={{ marginLeft: 30, width: 'auto', maxWidth: '50%', height: 80}}></img>
                                            : <div style={{ marginLeft: 30 }}>-</div>
                                    }
                                </div>

                                <div style={{ flex: '1' }}>
                                    <h3>当事人电话: {target.phone}</h3>
                                    <h3>日期:
                                    {/* {`${date.getFullYear()}年${date.getMonth() + 1}月${date.getDate()}日`} */}
                                        {target.createdTime}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </Print>
                </PrintProvider>
            </Modal>

        )
    }
}

function InfoLayout({ info }) {
    const { partyUserName, birth, sex, idcard, famousRace, lssueOffice, address, validityPeriod } = info;

    return (
        <Form layout="inline">
            <Row gutter={24}>
                <Col span={8}>
                    <Form.Item
                        label="姓名"
                    >
                        <b>{partyUserName}</b>
                    </Form.Item>
                </Col>
                <Col span={16}>
                    <Form.Item
                        label="出生日期"
                    >
                        <b>{birth}</b>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col span={8}>
                    <Form.Item
                        label="性别"
                    >
                        <b>{sex}</b>
                    </Form.Item>
                </Col>
                <Col span={16}>
                    <Form.Item
                        label="身份证号"
                    >
                        <b>{idcard}</b>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col span={8}>
                    <Form.Item
                        label="民族"
                    >
                        <b>{famousRace}</b>
                    </Form.Item>
                </Col>
                <Col span={16}>
                    <Form.Item
                        label="签发机关"
                    >
                        <b>{lssueOffice}</b>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col span={24}>
                    <Form.Item
                        label="住址"
                    >
                        <b>{address}</b>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col span={24}>
                    <Form.Item
                        label="证件有效期间"
                    >
                        <b>{validityPeriod}</b>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    )
}

function confirmBase64(url) {
    const base64Flag = 'data:image/png;base64,'
    return url.indexOf(base64Flag) > -1 ? url : base64Flag + url;
}

const styles = {
    layout: {
        borderWidth: 0,
        marginTop: 32,
        width: '100%',
    },
    title: {
        textAlign: 'center',
        marginBottom: 24,
    },
    reportCard: {
        width: '100%',
        borderWidth: 1,
        borderColor: "#000"
    },
    photoRow: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        vertical: 'middle',
        // vertical-align: middle;
        lineHeight: '100%'
    },
    info: {
        width: 500,
        padding: '0 20px'
    },
    avatar: {
        marginRight: 30,
        width: 102,
        height: 126
    },
    photoNow: {
        width: 'auto',
        height: 'auto',
        maxWidth: 350,
        maxHeight: 600,
    },
    photoNows: {
        vertical: 'middle',
        width: 300,
        height: 300,
    },
    analyzeReport: {
        width: 365,
        textAlign: 'center'
    },
    explain: {
        width: 300,
        margin: '10px auto',
        textAlign: 'left',
    },
    score: {
        fontSize: 42,
        lineHeight: '70px',
        fontWeight: '600',
        color: '#000',
        padding: '0 20px'
    },
    oath: {
        padding: 20,
        clear: 'both'
    },
    sign: {
        padding: '5px 20px',
        display: 'flex',
        alignItems: "center",
        border: 'none'
    },
    footer: {
        width: '100%',
        height: 32
    },
    footerButton: {
        float: 'right',
        marginLeft: 8
    },
    locationTag: {
        fontSize: 14,
        marginRight: 4
    }
}

export default ClearingReport;