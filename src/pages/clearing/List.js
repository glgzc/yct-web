import React from 'react';
import { Table, Tag, Button, Pagination } from 'antd';
import {config} from 'utils';

const {fileSource} = config;

const styles = {
    layout: {

    },
    resultArea: {
        // textAlign: 'center',
        margin: '0 12px'
    },
    photo: {
        height: 'auto',
        width: 'auto',
        maxWidth: 120,
        maxHeight: 120
    }
};
class ClearingList extends React.Component {
    constructor(props) {
        super(props);
        this.selectedRowKeys=[]
    }
    handleSizeChange=(current, pageSize)=>{
        this.props.turnPage(current,pageSize);
    }
    handleTableChange = (page, pageSize) => {
        this.props.turnPage(page, pageSize);
    }
    handleSelectChange=(selectedRowKeys)=>{
        this.selectedRowKeys=selectedRowKeys||[]
    }
    render() {
        let currentTime ='&'+ new Date().getTime()
        const { list, loading, total, pageIndex } = this.props;
        const columnProps = [
            {
                title: '时间',
                dataIndex: 'createdTime',
                key: 'time'
            }, {
                title: "业务类型",
                dataIndex: 'title',
                key: 'type',
            }, {
                title: '当事人',
                dataIndex: 'partyUserName',
                key: 'partyUserName',
            }, {
                title: '身份证号',
                dataIndex: 'idcard',
                key: 'idcard',
            },
            {
                title: '负责人',
                key: 'inCharge',
                render: (row) => inChargeMan(row.organizationName, row.userName)
            },
            {
                title: '证照',
                dataIndex: 'image2',
                key: 'image',
                render: (url) => 
                        <img style={styles.photo} src={`${fileSource}${url}?width=120&height=200&mode=fit`+currentTime} />
                 
            }, 
            {
                title: '识别结果',
                key: 'result',
                render: (result) => {
                    return <div style={styles.resultArea}>
                        {result.coincidenceDegree && <div>
                            <Tag color={result.coincidenceDegree > 70 ? "#2db7f5" : "#f50"} style={{margin: 5}}>吻合度：{result.coincidenceDegree}</Tag>
                            <Button size="small" type="primary" color="#108ee9" style={{margin: 5}} onClick={this.props.checkReport.bind(this, result)}>查看报告</Button>
                        </div>}
                        {/* {result.name && <p>姓名: <b>{result.name}</b></p>}
                        {result.gender && <p>性别: {result.gender}</p>}
                        {result.birthday && <p>生日: {result.birthday}</p>}
                        {result.race && <p>民族: {result.race}</p>}
                        {result.agency && <p>颁发机构: {result.agency}</p>}
                        {(result.valid_start || result.valid_end) && <p>有效日期: <b>{result.valid_start}</b> 至 <b>{result.valid_end}</b></p>}
                        {result.num && <p>身份证号: <b>{result.num}</b></p>}
                        {result.address && <p>地址: {result.address}</p>} */}
                    </div>
                }
            }, {
                title: '付费',
                dataIndex: 'price',
                key: 'price',
                width: 80,
                render: (price) => price ? `￥ ${parseInt(price) / 100} 元` : '￥0元'
            }
        ];
        const pageProps = {
            showSizeChanger:true,
            total: total,
            current: pageIndex,
            onShowSizeChange:this.handleSizeChange,
            onChange: this.handleTableChange
        }
        return (
            <div style={styles.layout}>
                <Table
                    simple
                    loading={loading}
                    columns={columnProps}
                    dataSource={list}
                    rowKey={(row) => row.id}
                    pagination={pageProps}
                    rowSelection={{
                        onChange:this.handleSelectChange
                    }}
                />
            </div>
        )
    }
}

function inChargeMan(organization, user) {
    return (
        <div>
            {organization && <Tag color="magenta" style={{margin: 5}}>{organization}</Tag>}
            {user && <Tag color="red" style={{margin: 5}}>{user}</Tag>}
        </div>
    )
}


export default ClearingList;