import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal, Popconfirm, Form, Tag ,Icon} from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onDeleteItem, onEditItem, editingKey,pushDataFunction,batchNumbers,agreePass,refusePass, cancelBatch,...tableProps }) => {

  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该用户吗?',
        onOk() {
          onDeleteItem(record.userId)
        },
      })
    }
  };
  const columns = [
    {
      title: '序号',
      dataIndex: 'ss',
      key: 'batchNumbers',
      render: (value, row, index) => {
        return (
          <p>{index + 1}</p>
        )
      }
    }, {
      title: '所属批次号',
      dataIndex: 'batchNumber',
      key: 'batchNumber'
    }, {
      title: '首次推送时间',
      dataIndex: 'createTime',
      key: 'createTime',
    }, {
      title: '最后推送时间',
      dataIndex: 'updateTime',
      key: 'updateTime'
    }, {
      title: '推送者账号',
      dataIndex: 'userName',
      key: 'userName'
    },
    {
      title: '推送者名称',
      dataIndex: 'realName',
      key: 'realName'
    },
    {
      title: '推送者所属机构',
      dataIndex: 'orgName',
      key: 'orgName'
    }, {
      title: '推送状态',
      dataIndex: 'state',
      key: 'state',
      render: text => {
        if (text == 0) {
          return (
            <div>
              <span className={styles.status} style={{ backgroundColor: '#00a854' }}></span>
              推送成功
            </div>
          )
        }
        return (
          <div>
            <span className={styles.status} style={{ backgroundColor: 'red' }}></span>
            推送失败
            </div>
        )
      },
    },
    {
      title: '接口反馈',
      dataIndex: 'msg',
      key: 'msg'
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return <a onClick={()=>pushDataFunction(record.batchNumber)}>推送</a>
      },
    },
  ]

  return (
    <div>
      <div style={{ width: '100%', height: 40, borderRadius: 4, backgroundColor: '#E6F7FF', marginBottom: 14 }}>
						{/* <img src={shape} style={{ width: 14, height: 14, marginTop: 13, float: 'left', marginLeft: 16 }}></img> */}
						<div style={{ fontSize: 14, marginTop: 10, float: 'left', marginLeft: 8, }}>
							已选择&nbsp;{batchNumbers.length}&nbsp;项
									</div>
						{batchNumbers.length > 0 ?
							<div>
								<Popconfirm
									// width={400}
									title="批量推送选中的批次号"
									okText='确定'
									cancelText='取消'
									onCancel={refusePass}
									onConfirm={()=>pushDataFunction(batchNumbers)}
									icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
								>
									<a style={{ fontSize: 14, marginTop: 10, float: 'left', marginLeft: 27, color: '#1890FF' }}
									>
										批量推送
									</a>
								</Popconfirm>
								<a style={{ fontSize: 14, marginTop: 10, float: 'left', marginLeft: 16, color: '#1890FF' }}
									onClick={cancelBatch}>
									取消
									</a>
							</div> : null}
					</div>
      <Table
        {...tableProps}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={record => record.batchNumber}
        rowSelection={{
          onChange: tableProps.selectBatch,
          selectedRowKeys:batchNumbers,
        }}
      />
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
}

export default List
