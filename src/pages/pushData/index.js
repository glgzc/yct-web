import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'

const PushData = ({ location, dispatch, pushData, loading }) => {

  const { list, pagination, currentItem, modalVisible, modalType, isMotion, selectedRowKeys, treeSelectData, listByOrganization, departmentList, rankList, keywords,
    batchNumbers } = pushData
  console.log(batchNumbers,'batchNumbers')
  const { pageSize } = pagination
  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['pushData/update'],
    title: `${modalType === 'create' ? '添加用户' : '编辑用户信息'}`,
    wrapClassName: 'vertical-center-modal',
    treeSelectData,
    listByOrganization,
    departmentList,
    rankList,
    onOk(data) {
      dispatch({
        type: `pushData/${modalType}`,
        payload: data,
      })
    },
    onCancel() {
      dispatch({
        type: 'pushData/hideModal',
      })
    },
    onChange(data) {
      dispatch({
        type: 'pushData/roleList',
        payload: data
      })
    }
  }
  const listProps = {
    dataSource: list,
    loading: loading.effects['pushData/query'],
    pagination,
    location,
    batchNumbers,
    isMotion,
    onChange(page) {
      dispatch({
        type: 'pushData/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
          keywords: keywords
        }
      })
    },
    selectBatch(selectedRowKeys, selectedRows) {
      dispatch({
        type: 'pushData/currentMethod',
        payload: {
          batchNumbers:selectedRowKeys,
        }
      })
    },
    pushDataFunction(record) {
      dispatch({
        type: 'pushData/pushDataModals',
        payload: {
          batchNumbers:record
        }
      })
    },
    onDeleteItem(id) {
      dispatch({
        type: 'pushData/delete',
        payload: id,
      })
    },
    onEditItem(item) {
      dispatch({
        type: 'pushData/modal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
    agreePass() {
      console.log('agree')
    },
    refusePass() {
      console.log('refuse')
    },
    cancelBatch() {
      dispatch({
        type: 'pushData/currentMethod',
        payload: {
          batchNumbers:[],
        }
      })
    }
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'pushData/queryKeyWords',
        payload: {
          keywords: value.keywords,
        },
      })
    },
    onSearch(fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/pushData',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/pushData',
      }))
    },
    onAdd() {
      dispatch({
        type: 'pushData/modal',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion() {
      dispatch({ type: 'pushData/switchIsMotion' })
    },
    Searchs(keywords) {
      dispatch({
        type: 'pushData/query',
        payload: {
          ...keywords
        }
      })
    }
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'pushData/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }

  return (
    <div className="content-inner">
      <Filter {...filterProps} />
      {
        selectedRowKeys.length > 0 &&
        <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
          <Col>
            {`Selected ${selectedRowKeys.length} items `}
            <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
              <Button type="primary" style={{ marginLeft: 8 }}>Remove</Button>
            </Popconfirm>
          </Col>
        </Row>
      }
      <List {...listProps} />
      {modalVisible && <Modal {...modalProps} />}
    </div>
  )
}

export default connect(({ pushData, loading }) => ({ pushData, loading }))(PushData)
