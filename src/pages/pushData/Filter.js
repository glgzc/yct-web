import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { FilterItem } from 'components'
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Switch, Select, } from 'antd'
const FormItem = Form.Item;
const { Option } = Select
const partiesConcernedformItemLayout = {
  labelCol: { span: 4, offset: 0 },
  wrapperCol: { span: 19, offset: 1 },
};
const timeLayout = {
  labelCol: { span: 9, offset: 0 },
  wrapperCol: { span: 14, offset: 1 },
}
const pusStatusLayout = {
  labelCol: { span: 4, offset: 10 },
  wrapperCol: { span: 9, offset: 1 },
}
const Search = Input.Search
const { RangePicker } = DatePicker

const ColProps = {
  xs: 24,
  sm: 12,
  style: {
    marginBottom: 16,
  },
}

const TwoColProps = {
  ...ColProps,
  xl: 96,
}

const Filter = ({
  onAdd,
  isMotion,
  switchIsMotion,
  onFilterChange,
  filter,
  Searchs,
  form: {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields
  },
}) => {
  const handleFields = (fields) => {
    // const { createTime } = fields
    // if (createTime.length) {
    //   fields.createTime = [createTime[0].format('YYYY-MM-DD'), createTime[1].format('YYYY-MM-DD')]
    // }
    return fields
  }

  const handleSubmit = () => {
    let fields = getFieldsValue()
    fields = handleFields(fields)
    onFilterChange(fields)
  }

  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const handleChange = (key, values) => {
    let fields = getFieldsValue()
    fields[key] = values
    fields = handleFields(fields)
    onFilterChange(fields)
  }
  const { name, address } = filter

  let initialCreateTime = []
  if (filter.createTime && filter.createTime[0]) {
    initialCreateTime[0] = moment(filter.createTime[0])
  }
  if (filter.createTime && filter.createTime[1]) {
    initialCreateTime[1] = moment(filter.createTime[1])
  }
  const getSearch = () => {
    let fields = getFieldsValue()
    let data = {}
    console.log(fields, 'fields')
    if (fields.startTime) {
      data.startTime = moment(fields.startTime[0]).format('YYYY-MM-DD HH:mm:ss')
      data.endTime = moment(fields.startTime[1]).format('YYYY-MM-DD HH:mm:ss')
    }
    data.state = fields.state
    data.keywords = fields.keywords
    Searchs(data)
  }
  const resetSearchData = () => {
    resetFields()
  }
  const preciseSearch = () => {
    let fields = getFieldsValue()
    Searchs({
      keywords: fields.keywords
    })
  }
  return (
    <Form layout='horizontal'>
      <Row gutter={24}>
        <Col {...ColProps} xl={{ span: 6 }} md={{ span: 6 }} style={{ marginBottom: 0 }}>
          <FormItem label="精确搜索:" {...partiesConcernedformItemLayout}>
            {getFieldDecorator('keywords', { initialValue: '' })(<Search placeholder="可输入机构,推送者名称,批次号" onSearch={preciseSearch} />)}
          </FormItem>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col {...ColProps} xl={{ span: 6 }} md={{ span: 6 }}>
          <FormItem label="其他筛选:&emsp;&emsp;时间筛选:" {...timeLayout}>
            {getFieldDecorator('startTime')(<RangePicker renderExtraFooter={() => 'extra footer'} showTime />)}
          </FormItem>
        </Col>
        <Col {...TwoColProps} xl={{ span: 6 }} md={{ span: 6 }} sm={{ span: 24 }}>
          {/* <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
            <div >
              <Button type="primary" className="margin-right" onClick={handleSubmit}>查询</Button>
            </div>
            <div>
              <Button type="ghost" onClick={onAdd}>+ 添加用户</Button>
            </div>
          </div> */}
          <FormItem label="推送状态:" {...pusStatusLayout}>
            {getFieldDecorator('state')(<Select placeholder="请选择" onChange={handleSubmit}>
              <Option key={'0'} value={'0'}>推送成功</Option>
              <Option key={'1'} value={'1'}>推送失败</Option>
            </Select>)}
          </FormItem>
        </Col>
        <Col {...TwoColProps} xl={{ span: 6 }} md={{ span: 6 }} sm={{ span: 24 }}>
          <Button style={{ marginLeft: 16, marginRight: 16, marginTop: 4 }} onClick={resetSearchData}>清空</Button>
          <Button type='primary' onClick={getSearch}>确定</Button>
        </Col>
      </Row>
    </Form>

  )
}

Filter.propTypes = {
  onAdd: PropTypes.func,
  isMotion: PropTypes.bool,
  switchIsMotion: PropTypes.func,
  form: PropTypes.object,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func,
}

export default Form.create()(Filter)
