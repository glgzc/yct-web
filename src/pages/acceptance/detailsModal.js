import React from 'react'
import PropTypes from 'prop-types'
import { Form, Tree, Modal, Card, Button } from 'antd'
import styles from './detailsModal.less'
import { config } from 'utils';
import {IEVersion} from 'utils/util';
const { fileSource } = config;


const detailsModal = ({
  currentDetailInfo,
  visibleDetailsModal,
  detailsInfo,
  item = {},
  onOk,
  dispatch,
  urrentDetailedIMG, showTheCurrentDetailedIMG, changeImg,
  ...modalProps
}) => {
  let currentTime ='&timestamp='+ new Date().getTime()
  const handlePreview = (url) => {
    window.open(`${fileSource}/${url}?width=400&height=400&mode=fit`+currentTime,"_blank");
  };
  const imgList = (fileList) => {
    if (fileList === undefined) {
      return
    }
    return <div className='fileWrap'>
      {
        fileList.length !== 0 ?
          fileList.map((item, index) => {
            return (
              <Card
                key={index}
                title={`材料${index + 1}`}
                style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
              >
                <img onClick={()=>handlePreview(item)} style={{ width: 160, height: 180 }} src={`${fileSource}/${item}?width=160&height=160&mode=fit`+currentTime} className='fileImage' />
              </Card>
            )
          })
          : '无'
      }
    </div>
  }

  const { appPartyUser, type, partyFile, comparisonRecordList } = currentDetailInfo
  const RecordList = (data) => {
    return data.map((item, index) => {
      return (
        <div className="text">
          <h4 style={{ marginTop: 16, marginBottom: 16 }}>人证信息比对情况{index + 1}</h4>
          <div><span>比对发起公证人：</span><span>{unNull(item.userName)}</span></div>
          <div><span>发起时间：</span><span>{unNull(item.createdTime)}</span></div>
          <div><span>对比类型：</span><span>{unNull(item.title)}</span></div>
          <div><span>比对得分：</span><span>{unNull(item.coincidenceDegree)}</span></div>
          <div><span>金额：</span><span>{unNull(item.price)}</span></div>
          {imgList([item.image2])}
        </div>)
    })
  }
  const orgInfo = (appPartyUser, partyFile) => {
    return (
      <div className='content'>
        <div className="text">
          <div>
            <span>名称：</span><span>{unNull(appPartyUser.name)}</span>
          </div>
          <div>
            <span>统一社会信用代码：</span><span>{unNull(appPartyUser.creditCode)}</span>
          </div>
          <div>
            <span>类型：</span><span>{unNull(appPartyUser.type)}</span>
          </div>
          <div>
            <span>住所：</span><span>{unNull(appPartyUser.address)}</span>
          </div>
          <div>
            <span>法人：</span><span>{unNull(appPartyUser.legalPerson)}</span>
          </div>
          <div>
            <span>注册资本：</span><span>{unNull(appPartyUser.registeredCapital)}</span>
          </div>
          <div>
            <span>成立时间：</span><span>{unNull(appPartyUser.establishmentTime)}</span>
          </div>
          <div>
            <span>营业期限：</span><span>{unNull(appPartyUser.businessTerm)}</span>
          </div>
          <div>
            <span>经营范围：</span><span>{unNull(appPartyUser.businessScope)}</span>
          </div>
          <div>
            <span>联系电话：</span><span>{unNull(appPartyUser.email)}</span>
          </div>
        </div>

        <div className="image">
          <div style={{ marginTop: 16 }}>
            <p>统一社会信用代码</p>
            {imgList(partyFile[0])}
          </div>
          <div>
            <p>法人/委托人资料</p>
            {imgList(partyFile[1])}
          </div>
          <div>
            <p>其他信息</p>
            {imgList(partyFile[2])}
          </div>
        </div>
      </div>)
  }
  // imgClick = (rotate) => {
  //   this.setState({
  //     rotate: rotate + 90
  //   })
  //   var pic = document.getElementsByTagName('img')
  //   var r = rotate + 90
  //   $("img").eq(pic.length - 1).css('transform', 'rotate(' + r + 'deg)');
  // }
 
  const personInfo = (appPartyUser, partyFile) => {
    return (
      <div >
        <div className={`${appPartyUser.riskPeople && styles.riskbackground} text`}>
          <div>
            <span>姓名：</span><span>{unNull(appPartyUser.name)}</span>
          </div>
          <div>
            <span>身份证号：</span><span>{unNull(appPartyUser.idCard)}</span>
          </div>
          <div>
            <span>民族：</span><span>{unNull(appPartyUser.famousRace)}</span>
          </div>
          <div>
            <span>住所：</span><span>{unNull(appPartyUser.address)}</span>
          </div>
          <div>
            <span>性别：</span><span>{unNull(appPartyUser.sex)}</span>
          </div>
          <div>
            <span>签名机关：</span><span>{unNull(appPartyUser.lssueOffice)}</span>
          </div>
          <div>
            <span>出生日期：</span><span>{unNull(appPartyUser.birth)}</span>
          </div>
          <div>
            <span>有效期：</span><span>{unNull(appPartyUser.validityPeriod)}</span>
          </div>
          <div>
            <span>比对得分：</span><span>{unNull(appPartyUser.recognitionResult)}</span>
          </div>
          <div>
            <span>风险控制：</span><span>{appPartyUser.riskPeople === 0 ? "未标记" : "已标记"}</span>
          </div>
          <div>
            <span>风险备注：</span><span>{unNull(appPartyUser.riskPeopleReason)}</span>
          </div>
        </div>

        <div className="image">
          <div style={{ marginTop: 16 }}>
            <p>身份资料</p>
            {imgList(partyFile[0])}
          </div>
          <div>
            <p>签名信息</p>
            {appPartyUser.autographUrl != "" &&appPartyUser.autographUrl!=undefined&&appPartyUser.autographUrl!=null&&appPartyUser.autographUrl!='null'?
              <Card
                title="材料"
                style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
              >
                <img style={{ width: 160, height: 180 }} src={`data:image/png;base64,${appPartyUser.autographUrl}`} className='fileImage' />
              </Card> : <p>无</p>}
          </div>
          <div>
            <p>婚姻状况说明</p>
            {imgList(partyFile[1])}
          </div>
          <div>
            <p>户口信息</p>
            {imgList(partyFile[2])}
          </div>
          <div>
            <p>资产信息</p>
            {imgList(partyFile[3])}
          </div>
          {RecordList(comparisonRecordList)}
        </div>
      </div>)
  }
  return (
    <div>
      <Modal visible={visibleDetailsModal} onCancel={() => onOk()} {...modalProps} style={IEVersion()?{right:"30%"}:{}}>

        {
          type === 'org' ? orgInfo(appPartyUser, partyFile) : personInfo(appPartyUser, partyFile)
        }
        <Button type='primary' style={{ width: "100%", height: 40 }} onClick={() => onOk()}>收起</Button>
      </Modal>
    </div>


  )
}
function unNull(value, replace = '--') {
  return value ? value : replace;
}
detailsModal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(detailsModal)
