import modelExtend from 'dva-model-extend'
import { list as queryBasicInfoList, tagListService, addDefaultItemsService,fileDeleteService } from 'services/basicInfo'

import { pageModel } from 'utils/model'

export default modelExtend(pageModel, {

  namespace: 'matter',
  state: {
    basicInfoList: [],
    vedioList: [],
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {

      })
    },
  },

  effects: {
    * queryInfoList({ payload, }, { call, put }) {    
      const data = yield call(queryBasicInfoList, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            basicInfoList: data.list,
          },
        })
      } else {
        throw data
      }
    },
    // getVedioEffect
    * getVedioEffect({ payload,}, { call, put }) {
      yield put({
        type: 'updateState',
        payload: {
          vedioList: vedioList.push(payload)
        },
      })
  },
    * queryTagLsit({ payload, }, { call, put }) {
      const data = yield call(tagListService, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            tagList: data.list,
            showModalDefault:true},
        })
      } else {
        throw data
      }
    },
    *addDefaultItems({ payload, }, { call, put }) {
      const addDefaultItemsData = yield call(addDefaultItemsService, payload)
      const data = yield call(queryBasicInfoList, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            basicInfoList: data.list,
          },
        })
      } else {
        throw data
      }
    },
    * deleteFile({ payload, }, { call, put }) {
      const data = yield call(fileDeleteService,payload)
    },
  },
})
