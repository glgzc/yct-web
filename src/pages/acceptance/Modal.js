import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Tree, Modal, Table ,Popconfirm, Icon,Select} from 'antd'
import city from '../../utils/city'
import {IEVersion} from 'utils/util';
const FormItem = Form.Item
const TreeNode = Tree.TreeNode

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const modal = ({
  item = {},
  onOk,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },

  modelVisibleGroup,
  interfaceList,
  dispatch,
  visibleCreateApi,
  createOk,
  createCancel,
  onCancel,
  onDeleteApi,
  onEdit,
  onEditOk,
  currentApiItem,
  modalType,
  onChangeMethod,
  currentMethod,
  ...modalProps
}) => {
  
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: modalType==='updateApi' ?currentApiItem.key : item.key,
      }
      for(const key in data){
        if(data[key]===undefined){
          data[key]=''
        }
      }
      if(modalType==='updateApi'){
        data.interfaceId=currentApiItem.interfaceId
        onEditOk(data)
      }else{
        data.id = item.id
        if(data.method==='按调用'){
          data.method=0
        }else if(data.method==='包断'){
          data.method=1
        }
        data.method = parseInt(data.method)
        data.price = parseFloat(data.price)*100
        onOk(data)
      }
    })
  }

  const createApiOK = () =>{
    if(modalType==='createPriceGroup'){
      handleOk()
    }else{
      createOk()
    }
  }
  const createApiCancel = () =>{
    if(modalType==='update'){
      onCancel()
    }else{
      createCancel()
    }
  }

  const onDeleteApiOk = (record) =>{
    onDeleteApi(record.interfaceId)
  }

  const onEditApi = (record) =>{
    onEdit(record)
  }

  const onChangeMethodOk =(value) =>{
    onChangeMethod(parseInt(value))
  }

  const loop = data => data.map((item) => {
    if (item.children) {
      return (
        <TreeNode key={item.menuId} title={item.name} >
          {loop(item.children)}
        </TreeNode>
      )
    }
    return <TreeNode key={item.menuId} title={item.name} />
  })

  const modalOpts = {
    ...modalProps,
    onOk: visibleCreateApi ? handleOk : createApiOK,
    onCancel: visibleCreateApi ? createApiCancel : onCancel
  }

  const columns = [
    {
      title: '名称',
      dataIndex: 'interfaceName',
      key: 'interfaceName'
    }, {
      title: 'URL',
      dataIndex: 'interfaceUrl',
      key: 'interfaceUrl'
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (
          <div>
            <a onClick={()=>onEditApi(record)}>编辑</a>&nbsp;
            <Popconfirm title="确定删除吗？" onConfirm={()=>onDeleteApiOk(record)} icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}>
              <a href="#">删除</a>
            </Popconfirm>
          </div>
        )
      },
    },
  ]

  return (
    <Modal {...modalOpts}
      okText={modelVisibleGroup? '确定': (visibleCreateApi ? '确定' : '添加接口')}style={IEVersion()?{right:"30%"}:{}}>
      <Form layout="horizontal">
        {
          modelVisibleGroup ? (
            <div>
              <FormItem label="价格组名称" hasFeedback {...formItemLayout}>
                {getFieldDecorator('title', {
                  initialValue: item.title,
                  rules: [
                    {
                      required: true,
                    },
                  ],
                })(<Input />)}
              </FormItem>
            </div>
          )
          : (
            <div>
                {
                  visibleCreateApi ? 
                  (
                    <div>
                      {
                        modalType==='update' ? 
                        (
                          <div>
                            <FormItem label="收费方式" hasFeedback {...formItemLayout}>
                            {getFieldDecorator('method', {
                              initialValue: item.method === 0 ? '按调用' : '包断',
                              rules: [
                                {
                                  required: true,
                                },
                              ],
                            })(
                              <Select onChange={onChangeMethodOk}>
                                <Select.Option value="0">按调用</Select.Option>
                                <Select.Option value="1">包断</Select.Option>
                              </Select>
                            )}
                            </FormItem>
                            <FormItem label="价格（元）" hasFeedback {...formItemLayout}>
                              {getFieldDecorator('price', {
                                initialValue: currentMethod? 0 : item.price/100,
                                rules: [
                                  {
                                    required: true,
                                  },
                                ],
                              })(
                                <Input disabled={currentMethod? true : false} />
                              )}
                            </FormItem>
                          </div>
                        ) : 
                        (
                          <div>
                            <FormItem label="接口名称" hasFeedback {...formItemLayout}>
                            {getFieldDecorator('interfaceName', {
                              initialValue: currentApiItem.interfaceName,
                              rules: [
                                {
                                  required: true,
                                },
                              ],
                            })(<Input />)}
                            </FormItem>
                            <FormItem label="接口URL" hasFeedback {...formItemLayout}>
                              {getFieldDecorator('interfaceUrl', {
                                initialValue: currentApiItem.interfaceUrl,
                                rules: [
                                  {
                                    required: true,
                                  },
                                ],
                              })(<Input />)}
                            </FormItem>
                          </div>
                        )
                      }
                    </div>
                  ) :
                  (
                    <Table 
                    dataSource={interfaceList}
                    columns={columns} />
                  )
                }
            </div>
          )
        }
      </Form>
    </Modal>
  )
}

modal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(modal)
