import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const CostSettlementList = ({ ...tableProps }) => {
  
  const columns = [
    {
      title: '序号',
      dataIndex: 'id',
      key: 'id'
    }, {
      title: '收费项目',
      dataIndex: 'project',
      key: 'project'
    }, {
      title: '结算方式',
      dataIndex: 'settlementMethod',
      key: 'settlementMethod'
    }, {
      title: '金额',
      dataIndex: 'money',
      key: 'money'
    }, {
      title: '收费方式',
      dataIndex: 'chargeMethod',
      key: 'chargeMethod'
    }, {
      title: '结算人',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: '开票时间',
      dataIndex: 'time',
      key: 'time'
    }, {
      title: '是否作废',
      dataIndex: 'isDelete',
      key: 'isDelete'
    }
  ]

  const dataSource = [
    {
      id: 1,
      project: '公证费',
      settlementMethod: '方式名1',
      money: 2205,
      chargeMethod: '微信支付',
      name: '王小明',
      time: '2018-11-12',
      isDelete: '否'
    }
  ]

  return (
    <div style={{marginTop:'15px'}}>
      <Table
        {...tableProps}
        // dataSource={dataSource}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={record => record.id}
        pagination={false}
      />
    </div>
  )
}

CostSettlementList.propTypes = {
}

export default CostSettlementList
