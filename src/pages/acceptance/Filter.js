import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Form, Button, Row, Col, Input } from 'antd'

const Search = Input.Search


const Filter = ({
  isMotion,
  switchIsMotion,
  onFilterChange,
  changeInput,
  addToCollect,
  filter,
  form: {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
  },
}) => {
  const handleFields = (fields) => {
    // const { createTime } = fields
    // if (createTime.length) {
    //   fields.createTime = [createTime[0].format('YYYY-MM-DD'), createTime[1].format('YYYY-MM-DD')]
    // }
    return fields
  }

  const handleSubmit = () => {
    let fields = getFieldsValue()
    fields = handleFields(fields)
    onFilterChange(fields)
  }
  const changeValue = () => {
    let fields = getFieldsValue()
    fields = handleFields(fields)
    changeInput(fields)
  }
  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const handleChange = (key, values) => {
    let fields = getFieldsValue()
    fields[key] = values
    fields = handleFields(fields)
    onFilterChange(fields)
  }
  const { name, address } = filter

  let initialCreateTime = []
  if (filter.createTime && filter.createTime[0]) {
    initialCreateTime[0] = moment(filter.createTime[0])
  }
  if (filter.createTime && filter.createTime[1]) {
    initialCreateTime[1] = moment(filter.createTime[1])
  }
  const addCollect = () => {
    addToCollect()
  }
  return (
    // <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    //   <div style={{ display: 'flex', flex: 1, marginBottom: '15px' }}>
    //     <div {...ColProps} md={6} sm={6}>
    //       {getFieldDecorator('keywords', { initialValue: name })(<Search placeholder="姓名、手机号" onSearch={handleSubmit} />)}
    //     </div>
    //     <div md={4} style={{ height: 32, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
    //       {getFieldDecorator('risk')(
    //         <Select placeholder="风险控制" style={{ width: 140, marginLeft: 24 }}>
    //           <Option key="all">所有人</Option>
    //           <Option key="1">已标记</Option>
    //           <Option key="0">未标记</Option>
    //         </Select>)}
    //     </div>
    //     <div {...TwoColProps} md={4} style={{ marginLeft: 24 }}>
    //       <Button type="primary" className="margin-right" onClick={handleSubmit}>查询</Button>
    //     </div>
    //   </div>
    //   <Button type="primary" className="margin-right" onClick={addPerson}>添加自然人</Button>
    // </div>

    <Row gutter={24} style={{ marginBottom: '15px' }}>
      <Col xs={{ span: 18 }} >
        {getFieldDecorator('keywords', { initialValue: name })(<Search style={{ width: 200, marginRight: 20 }} placeholder="事项、公证员、助理" onSearch={handleSubmit} onChange={changeValue}/>)}
        <Button type="primary" onClick={handleSubmit}>查询</Button>
      </Col>
      <Col xs={{ span: 6 }} style={{textAlign:'right'}}>
       <Button type="primary" onClick={addCollect}>新建采集</Button>
      </Col>
    </Row>
  )
}
export default Form.create()(Filter)
