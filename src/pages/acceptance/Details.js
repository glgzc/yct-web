import React from 'react'
import { Select, Card, Icon, Tag } from 'antd'
import './List.less'
import $ from "jquery"
import PersonList from './PersonList'
import Lightbox from 'react-images'
import moment from 'moment'
import { config } from 'utils';
import Document from './Document'
import ForeignNationals from './ForeignNationals'
import Company from './Company'
const { fileSource, showVideotURL,host,api} = config;
const { downloadWordURL} =api
const Option = Select.Option;
const token=sessionStorage.getItem("token")
class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      value: '',
      bottons: '查看',
      previewVisible: false,
      previewImage: [],
      previewIndex: 0,
      rotate: 0,
      current: 0,
    };
  }
  imgClick = (rotate) => {
    this.setState({
      rotate: rotate + 90
    })
    var pic = document.getElementsByTagName('img')
    var r = rotate + 90
    $("img").eq(pic.length - 1).css('transform', 'rotate(' + r + 'deg)');
  }
  handlePreview = (url) => {
    let currentTime = '&' + new Date().getTime()
    const arr = [{ src: `${fileSource}/${url}?width=400&height=400&mode=fit` + currentTime, caption: '单击旋转' }]
    this.setState({
      previewImage: arr,
      previewVisible: true,
    })
  };
  handleHideDetails() {
    this.props.onHideDetails()
  }
  showVideo() {
    const { currentInfo } = this.props
    const { basicInfo } = currentInfo;
    if (basicInfo.video != undefined && basicInfo.video != '' && basicInfo.videoState != '0') {
      let video = basicInfo.video.split(",")
      let currentVideoArry = []
      video.map((item) => {
        if (item != '') {
          currentVideoArry.push(
            <a href={showVideotURL + item} target="_blank" style={{ display: 'block' }}>{item.substring(item.lastIndexOf("\/") + 1, item.length)}</a>
          )
        }
      })
      return currentVideoArry
    } else {
      return null
    }
  }
  edit() {
    this.props.editWord()
  }
  downPeople(){
    this.props.downPeoples()
  }
  editForeign(){
    this.props.editForeigns()
  }
  editCompany(){
    this.props.editCompanys()
  }
  downWords(type) {
    const {
      currentItem,
    } = this.props
    let token = sessionStorage.getItem("token")
    window.open(host+downloadWordURL+'?matterId='+currentItem.id+"&type="+type+'&token='+token)
  }
  render() {
    const {
      currentItem,
      currentInfo,
      showDetailModal, showEditWords,clickOnOk,clickOnCancle,personIndex,addPersons,getWordInfo,showEditForeign,getForeignInfo,agentIndex,addAgentPersons,
      getCompanyInfo,showEditCompany,
    } = this.props
    const { basicInfo, fileList, partyList } = currentInfo;
    const { previewVisible, previewImage, previewIndex } = this.state
    const detailsModalProps = {
      title: <div style={{ textAlign: "center" }}>当事人信息详情</div>,
      okText: "收起",
      footer: null,
      onOk() {
        // dispatch({
        //   type: "acceptance/hideDetailsModal"
        // })
      },
    }
    let currentTime = '&' + new Date().getTime()
    let otherInfo = ''
    if (basicInfo.data != null && basicInfo.data != '') {
      const arr = JSON.parse(basicInfo.data)
      arr.map((item, index) => {
        if (index === arr.length - 1) {
          return otherInfo = otherInfo + item.tagName + "-" + item.c
        }
        else return otherInfo = otherInfo + item.tagName + "-" + item.c + "/"
      }
      )
    }
    const PersonListProps = {

    }
    const modalPropsPeople = {
      visible: showEditWords, 
      
      personIndex,
      getWordInfo,
      title: <div style={{ textAlign: 'center' }}>公证申请表个人</div>,
      width:1080,
      wrapClassName: 'vertical-center-modal',
      onOk(data,matterId) {
        let type = 0
        clickOnOk(data,matterId,type)
      },
      onCancel() {
        clickOnCancle()
      },
      addPerson() {
        addPersons()
      }
    }
    const modalPropsCompanys = {
      visible: showEditCompany, 
      personIndex,
      agentIndex,
      getCompanyInfo,
      title: <div style={{ textAlign: 'center' }}>公 证 申 请 表 (单位)</div>,
      width:1080,
      wrapClassName: 'vertical-center-modal',
      onOk(data,matterId) {
        let type = 1
        clickOnOk(data,matterId,type)
      },
      onCancel() {
        clickOnCancle()
      },
      addPerson() {
        addPersons()
      },
      addAgentPerson() {
        addAgentPersons()
      },
    }
    const modalPropsForeignNationals = {
      visible: showEditForeign, 
      personIndex,
      getForeignInfo,
      title: <div style={{ textAlign: 'center' }}>德阳市旌湖公证处涉外民事公证申请表（个人适用）</div>,
      width:1080,
      wrapClassName: 'vertical-center-modal',
      onOk(data,matterId) {
        let type = 2
        clickOnOk(data,matterId,type)
      },
      onCancel() {
        clickOnCancle()
      },
      addPerson() {
        addPersons()
      }
    }
    const imgShow = (fileList) => {
      return <div className='fileWrap'>
        {
          fileList.length !== 0 ?
            fileList.map((item, index) => {
              return (
                <Card
                  key={index}
                  title={`材料${index + 1}`}
                  style={{ width: 206, marginLeft: '5px', marginRight: '5px' }}
                >
                  <img onClick={this.handlePreview.bind(this, item)} src={`${fileSource}${item}?width=400&height=400&mode=fit` + currentTime} className='fileImage' />
                </Card>
              )
            })
            : '无'
        }
      </div>
    }
    return (
      <div className='acceptanceDetails'>
       { showEditWords?<Document {...modalPropsPeople}/> :null}
     {showEditForeign?<ForeignNationals {...modalPropsForeignNationals}/>:null}
        {showEditCompany?<Company {...modalPropsCompanys}/>:null}
        <a onClick={this.handleHideDetails.bind(this)}><Icon type="close" className='closeIcon' /></a>
        <div className='detailsHeader'>
          <h2>受理详情</h2>
          <div>
            <span>部门：{unNull(basicInfo.organization)}</span>
            <span>公证员：{unNull(basicInfo.notaryName)}</span>
            <span>协办人：{unNull(basicInfo.coOrganizer)}</span>
            {/* <span>状态：{basicInfo.state ? '已受理' : '待受理'}</span> */}
          </div>
        </div>
        <div className='marginTop'>
          <h3>基本信息</h3>
          <div className='baseInfoContent'>
            <div>
              <span>批号：</span>
              <span>{unNull(basicInfo.batchNumber)}</span>
            </div>
            {/* <div>
            <span>公证书编号：</span>
            <span>{unNull(basicInfo.certificateNumber)}</span>
          </div> */}
            <div>
              <span>类别：</span>
              <span>{unNull(basicInfo.categoryl)}</span>
            </div>
            <div>
              <span>事项：</span>
              <span>{unNull(basicInfo.notarizationMattersName)}</span>
            </div>
            <div>
              <span>使用地：</span>
              <span>{unNull(basicInfo.siteOfUse)}</span>
            </div>
            <div>
              <span>用途：</span>
              <span>{unNull(basicInfo.purpose)}</span>
            </div>
            <div>
              <span>份数：</span>
              <span>{unNull(basicInfo.numberOfCopies)}</span>
            </div>
            <div>
              {/* 不确定是否有该字段 */}
              <span>领证人：</span>
              <span>{unNull(basicInfo.certificationHolder)}</span>
            </div>
            <div>
              {/* 不确定是否有该字段 */}
              <span>助理：</span>
              <span>{unNull(basicInfo.assistant)}</span>
            </div>
            <div>
              <span>办理时间：</span>
              <span>{unNull(moment(parseInt(basicInfo.processingTime)).format('YYYY-MM-DD HH:mm:ss') == "Invalid date" ||moment(parseInt(basicInfo.processingTime)).format('YYYY-MM-DD HH:mm:ss') == "1970-01-01 08:00:02"? basicInfo.processingTime : moment(parseInt(basicInfo.processingTime)).format('YYYY-MM-DD HH:mm:ss'), 'xxx xxx xxx')}</span>
            </div>
            <div>
              <span>是否需要寄送副本：</span>
              <span>{basicInfo.send ? '是' : '否'}</span>
            </div>
            <div>
              <span>急件：</span>
              <span>{basicInfo.urgentItems ? '是' : '否'}</span>
            </div>
            <div>
              <span>需要认证：</span>
              <span>{basicInfo.authentication ? '是' : '否'}</span>
            </div>
            <div>
              <span>币种：</span>
              <span>{unNull(basicInfo.currency)}</span>
            </div>
            <div>
              <span>译文：</span>
              <span>{unNull(basicInfo.translation)}</span>
            </div>
          </div>
          <div className='otherInfo'>
            <span>其他信息：</span>
            <span>{unNull(otherInfo)}</span>
          </div>
        </div>
        <div className='marginTop'>
          <h3>人员信息</h3>
          <PersonList {...PersonListProps} list={partyList} showDetailModal={(record) => showDetailModal(record)} />
        </div>
        {/* <div className='marginTop'>
        <h3>费用信息</h3>
        <CostInfoList />
      </div> */}
        {/* <div className='marginTop'>
        <h3>费用结算</h3>
        <CostSettlementList />
      </div> */}
        <div className='marginTop'>
          <h3>补充资料</h3>
          <h4 style={{ marginTop: 16 }}>公证证明材料</h4>
          {imgShow(fileList[0])}
          <h4 style={{ marginTop: 16 }}>当事人主体资料补充</h4>
          {imgShow(fileList[1])}
          <h4 style={{ marginTop: 16 }}>办理过程材料</h4>
          {imgShow(fileList[2])}
          <h4 style={{ marginTop: 16 }}>有关证明材料</h4>
          {imgShow(fileList[3])}
          <h4 style={{ marginTop: 16 }}>公证收费资料</h4>
          {imgShow(fileList[4])}
          <h4 style={{ marginTop: 16 }}>录像资料</h4>
          {this.showVideo()}
          <div style={{ marginTop: 16 }}><Icon type="pushpin-o" style={{ float: "left", fontSize: 16, color: '#08c' }} /><h4>备注</h4></div>
          <div style={{ marginTop: 5, marginBottom: 5 }}>
            {
              basicInfo.tagsName.map((item) => {
                return <Tag color="#108ee9" style={{ marginRight: 5 }}>{item}</Tag>
              })
            }
          </div>
          <p>{basicInfo.remark}</p>
          <div style={{ marginTop: 16 }}><h4>文档生成</h4></div>
          <div className='fileWrap'>
            <Card
              key={1}
              title={'公证申请表个人'}
              style={{ width: 206, marginLeft: '5px', marginRight: '5px', textAlign: 'center' ,marginTop:20}}
            >
              <div style={{ overflow: 'hidden' }}>
              {/* currentItem.id */}
                <a onClick={this.edit.bind(this)}>编辑</a>  <a style={{ marginLeft: 20 }}  onClick={this.downWords.bind(this,0)}>下载</a>
              </div>
            </Card>
            <Card
              key={2}
              title={'公证申请表单位'}
              style={{ width: 206, marginLeft: '5px', marginRight: '5px', textAlign: 'center',marginTop:20,marginLeft:20,marginRight:20 }}
            >
              <div style={{ overflow: 'hidden' }}>
                <a onClick={this.editCompany.bind(this)}>编辑</a>  <a style={{ marginLeft: 20 }} onClick={this.downWords.bind(this,1)} >下载</a>
              </div>
            </Card>
            <Card
              key={3}
              title={'德阳市旌湖公证处涉港、澳民事公证申请表（个人适用）'}
              style={{ width: 206, marginLeft: '5px', marginRight: '5px', textAlign: 'center',marginTop:20}}
            >
              <div style={{ overflow: 'hidden' }}>
                <a onClick={this.editForeign .bind(this)}>编辑</a>  <a onClick={this.downWords.bind(this,2)}>下载</a>
              </div>
            </Card>
          </div>
        </div>
        {/* <Button type='primary' style={{height:40}}>保存</Button> */}
     
        <Lightbox
          onClickImage={this.imgClick.bind(this, this.state.rotate)}
          showThumbnails={true}
          backdropClosesModal={true}
          currentImage={previewIndex}
          images={previewImage}
          isOpen={previewVisible}
          onClickPrev={() => {
            this.setState({
              previewIndex: previewIndex - 1,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')

          }}
          onClickThumbnail={(index) => {
            this.setState({
              previewIndex: index,
              rotate: 0
            })
            var pic = document.getElementsByTagName('img')
            $("img").eq(pic.length - 1).css('transform', 'rotate(' + 0 + 'deg)')
          }}
          onClose={() => { this.setState({ previewVisible: false }) }}
        />
      </div>
    )
  }
}

function unNull(value, replace = '--') {
  return value ? value : replace;
}
export default Details;
