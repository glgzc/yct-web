import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const CostInfoList = ({ ...tableProps }) => {
  
  const columns = [
    {
      title: '序号',
      dataIndex: 'id',
      key: 'id'
    }, {
      title: '收费项目',
      dataIndex: 'project',
      key: 'project'
    }, {
      title: '应收金额',
      dataIndex: 'receivable',
      key: 'receivable'
    }, {
      title: '已收金额',
      dataIndex: 'accepted',
      key: 'accepted'
    }, {
      title: '登记费',
      dataIndex: 'register',
      key: 'register'
    }, {
      title: '追加费',
      dataIndex: 'append',
      key: 'append'
    }, {
      title: '减免费',
      dataIndex: 'reduction',
      key: 'reduction'
    }, {
      title: '退费',
      dataIndex: 'refund',
      key: 'refund'
    }
  ]

  const dataSource = [
    {
      id: 1,
      project: '公证费',
      receivable: 2205,
      accepted: 0,
      register: 2205,
      append: 0,
      reduction: 0,
      refund: 0
    }
  ]

  return (
    <div style={{marginTop:'15px'}}>
      <Table
        {...tableProps}
        // dataSource={dataSource}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={record => record.id}
        // rowSelection={false}
        pagination={false}
      />
    </div>
  )
}

CostInfoList.propTypes = {
}

export default CostInfoList
