import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Radio, Modal, Row, Col, Checkbox } from 'antd'
import {IEVersion} from 'utils/util';
import moment from 'moment'
const examineModal = ({
  examineData,
  ...ExamineModalProps

}) => {
  const getTagList = () => {
    if (examineData.data) {
      const tagList = JSON.parse(examineData.data)
      let result = tagList.map((item) => {
        return (
          <div style={{ overflow: 'hidden', marginTop: 13 }}>
            <div style={{ float: 'left' }}>{item.tagName ? item.tagName : ''}</div>
            <div style={{ float: 'left', marginLeft: 20 }}>{item.c ? item.c : ''}</div>
          </div>
        )
      })
      return result
    }
  }
  console.log(examineData,'examineData')
  return (
    <Modal {...ExamineModalProps} style={IEVersion()?{right:"30%"}:{}}>
      <Row gutter={24}>
        <h4 style={{ marginLeft: 12 }}>基本信息</h4>
        <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
        <Col md={{ span: 12 }}>
          <div style={{ marginTop: 10 }}>名称</div>
          <div style={examineData.title !=undefined? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.title !=undefined? examineData.title : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>公证员</div>
          <div style={examineData.notary ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.notary ? examineData.notary : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>助理</div>
          <div style={examineData.assistant ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.assistant ? examineData.assistant : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>使用地</div>
          <div style={examineData.siteOfUse ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.siteOfUse ? examineData.siteOfUse : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>办理时间</div>
          <div style={examineData.processingTime ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>
            {examineData.processingTime ? (/^[0-9]+$/.test(examineData.processingTime))?moment().format('YYYY-MM-DD HH:mm:ss'):examineData.processingTime : '暂无信息'}</div>
          <div style={{ overflow: 'hidden', marginTop: 13 }}>
            <div style={{ float: 'left' }}>需要寄送副本：</div>
            <div style={{ float: 'right', marginRight: 120 }}>{examineData.send == 1 ? '是' : '否'}</div>
          </div>
          <div style={{ overflow: 'hidden', marginTop: 13 }}>
            <div style={{ float: 'left' }}>急件:</div>
            <div style={{ float: 'right', marginRight: 120 }}>{examineData.urgentItems == 1 ? '是' : '否'}</div>
          </div>
          <div style={{ overflow: 'hidden', marginTop: 13 }}>
            <div style={{ float: 'left' }}>需要认证:</div>
            <div style={{ float: 'right', marginRight: 120 }}>{examineData.authentication == 1 ? '是' : '否'}</div>
          </div>
        </Col>
        <Col md={{ span: 12 }}>
          <div style={{ marginTop: 10 }}>部门</div>
          <div style={examineData.organization ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.organization ? examineData.organization : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>协办人</div>
          <div style={examineData.coOrganizer ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.coOrganizer ? examineData.coOrganizer : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>领证人</div>
          <div style={examineData.certificationHolder ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.certificationHolder ? examineData.certificationHolder : '暂无信息'}</div>
          <div style={{ marginTop: 13 }}>用途</div>
          <div style={examineData.purpose ? { marginTop: 13, marginLeft: 12, height: 22 } : { marginTop: 13, marginLeft: 12, height: 22, color: "#cccccc" }}>{examineData.purpose ? examineData.purpose : '暂无信息'}</div>
        </Col>
      </Row>
      <h4 style={{ marginTop: 30 }}>其他信息</h4>
      <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
     {examineData.data?<h4 style={{}}>后台配置的标签名称及备注</h4>:<h3 style={{color:"#cccccc"}}>暂无信息</h3>}
      {getTagList()}
    </Modal>
  )
}

examineModal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(examineModal)
