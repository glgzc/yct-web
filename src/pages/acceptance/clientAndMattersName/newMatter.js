import React, { Component } from 'react'
import { connect } from 'dva'
import { message, Tabs, Button, Icon, Table, Form, Radio, Switch, Upload, Cascader, Input, InputNumber, Modal, Checkbox, Select } from 'antd'
import cascadeData from '../../../assets/matter.json'
import currency from '../../../assets/currency.json'
// import Eloam from '../../../components/Eloam/index'
import CrossBrowserDriver from '../../../components/CrossBrowserDriver/index'
import BasicInfoForm from '../../basicInfo/form'
import ExamineModal from './examineModal'
import { config } from 'utils'
import noData from '../../../assets/noData.png';
const { host, api, fileSource, showVideotURL } = config
const { fileUpload } = api
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;
const { Options } = Select
const sections = [
  {
    key: 1,
    title: "公证证明材料",
  },
  {
    key: 2,
    title: "当事人主体资料补充"
  },
  {
    key: 3,
    title: "办理过程材料"
  },
  {
    key: 4,
    title: "有关证明材料"
  },
  {
    key: 5,
    title: "公证收费资料"
  },
]
class NewMatter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fileList: {},
      modalVisible: false,
      tagNameArry: [],
      ExamineModalVisible: false,
      examineData: '',
      previewVisible: false,
      previewImage: '',
      complete: [],
      showVedio: false,
      vedioList: [],
      memoText: '',
      supplementStateInfo: { "1": false, "2": false, "3": false, "4": false, "5": false },
      name: '',
    }
    this.pictureType = 0
  }
  loadMatterNamesExpandableList(cascadeData) {
    return cascadeData.map((matterCategory) => ({
      value: matterCategory.key,
      label: matterCategory.key,
      children: matterCategory.list.map(matterIndex => ({
        value: matterIndex.title,
        label: matterIndex.title,
        children: matterIndex.data.map(matter => ({
          value: matter,
          label: matter,
          // 是否收藏了
          collected: false
        }))
      }))
    }));
  }

  loadBasicInfoList() {
    this.props.dispatch({
      type: 'matter/queryInfoList',
      payload: {
      }
    })
  }
  getVedio(filePath) {
    this.props.dispatch({
      type: 'matter/getVedioEffect',
      payload: {
        filePath
      }
    })
  }
  loadTagList() {
    this.props.dispatch({
      type: 'matter/queryTagLsit',
      payload: {
      }
    })
    this.setState({ modalVisible: true, modalType: 'create' })
  }
  getInfoChecked() {
    const { infoChecked } = this.props.matter
    return infoChecked
  }
  onInfoCheckChange(e) {
    const val = e.target.value
    const matterId = sessionStorage.getItem('matterId')
    sessionStorage.setItem("infoChecked", val)
    this.props.dispatch({
      type: 'matter/updateState',
      payload: {
        infoChecked: val
      }
    })
  }
  onSwitchChange(val, key) {
    const { complete, supplementStateInfo } = this.state
    const { dispatch, acceptance, changeSupplementState } = this.props
    const { supplementState } = acceptance
    if (val) {
      complete.push(key)
      supplementStateInfo[key] = true
      this.setState({
        complete: complete,
        supplementStateInfo: supplementStateInfo
      })
      changeSupplementState(supplementStateInfo)
    } else {
      supplementStateInfo[key] = false
      var newArr = complete.filter(item => {
        if (item !== key) {
          return item
        }
      })
      this.setState({
        complete: newArr,
        supplementStateInfo: supplementStateInfo
      })
      changeSupplementState(supplementStateInfo)
    }
  }
  onTextInputChange(e) {
    const { value } = e.target
    const { acceptance } = this.props
    const { matterOtherData } = acceptance
    const matterId = sessionStorage.getItem("matterId")
    if (!matterOtherData[matterId]) {
      matterOtherData[matterId] = {}
    }
    matterOtherData[matterId]["memoText"] = value
    this.setState({
      memoText: value
    })
  }
  onTakePicture(type) {
    this.pictureType = type
    let timestamp = new Date().getTime()
    const localId = sessionStorage.getItem("batchId")
    const userId = sessionStorage.getItem('userId')
    const matterId = sessionStorage.getItem("matterId")
    this.refs.crossBrowserDriver.openVideoMain(0, {
      userId,
      localId,
      partyNumber: matterId,
      type: 2,
      dataType: type - 1,
      clientUpdateAt: timestamp
    })
  }

  onUploadChange({ file, fileList, key }) {
    const sessionFileList = this.props.fileList
    if (file.status == 'done') {
      if (!sessionFileList[key - 1]) {
        sessionFileList[key - 1] = []
        sessionFileList[key - 1].push(file.response.fileId)
      } else {
        sessionFileList[key - 1].push(file.response.fileId)
      }
    }
    this.props.getFileList(sessionFileList, 'sessionFileList')
    const files = this.state.fileList
    if (file.status === 'done') {
      const { response } = file
      if (response.code) {
        message.error("文件上传失败:" + response.msg, 5);
        fileList[fileList.length - 1]["status"] = "error"
      }
    }
    files[key] = fileList
    this.setState({ fileList: files })

  }
  onUploadRemove({ file, fileList, key }) {
    const currentFile = this.props.fileList
    if (file.response) {
      this.props.dispatch({
        type: 'matter/deleteFile',
        payload: {
          fileId: file.response.fileId
        }
      })
      if (JSON.stringify(currentFile[key - 1]) == "{}") {
        let currentNumber = currentFile[key - 1].indexOf(file.response.fileId)
        currentFile[key - 1].splice(currentNumber, 1)
        this.props.getFileList(currentFile, 'sessionFileList')
      }
    } else {
      let fileID = file.url.substring(file.url.lastIndexOf("\/") + 1, file.url.length)
      let arr = fileID.split("?");
      this.props.dispatch({
        type: 'matter/deleteFile',
        payload: {
          fileId: arr[0]
        }
      })
      if (JSON.stringify(currentFile[key - 1]) == "{}") {
        let currentNumber = currentFile[key - 1].indexOf(arr[0])
        currentFile[key - 1].splice(currentNumber, 1)
        this.props.getFileList(currentFile, 'sessionFileList')
      }
    }
  }
  handlePreview(file) {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }
  handleCancel = () => this.setState({ previewVisible: false })
  showTheVedioList() {
    const { vedioList } = this.state
    let list = []
    console.log(vedioList, 'vedioList')
    vedioList.map((item, index) => {
      list.push(
        // <a href={item.filePath?filePath:showVideotURL+(item.replace(/,/g, ""))} style={{ display: 'block' }} target="_blank">{'视频录像' + (index + 1)}</a>
        <a href={item.filePath != undefined && item.filePath != null ? item.filePath : showVideotURL + (item.replace(/,/g, ""))} style={{ display: 'block' }} target="_blank">{'视频录像' + (index + 1)}</a>
      )
    })
    return list
  }
  showNoDta = () => {
    return (
      <div style={{ textAlign: "center" }}>
        <img
          style={{ marginTop: 50, display: 'inline-block', marginBottom: 50 }}
          src={noData}
        />
      </div>
    )
  }
  renderUpload() {
    const { fileList, complete, supplementStateInfo } = this.state
    const matterId = sessionStorage.getItem("matterId")
    const localId = sessionStorage.getItem("batchId")
    const userId = sessionStorage.getItem('userId')
    const token = sessionStorage.getItem('token')
    let timestamp = new Date().getTime()
    const uploadData = {
      localId,
      userId,
      partyNumber: matterId,
      type: 2,
      fileMd5: "1",
      clientUpdateAt: timestamp,
    }
    const action = host + fileUpload + "?token=" + token
    const uploadProps = {
      multiple:true,
      action,
      listType: "picture-card",
    }
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传文件</div>
      </div>
    );
    return sections.map((item) => {
      return [
        <div key={"t" + item.key} style={{ padding: "15px 0", borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
          <h3 style={{ float: 'left', margin: 0 }}>{item.title}</h3>
          <div style={{ float: 'right' }} >
            <Switch onChange={(val) => this.onSwitchChange(val, item.key)} checked={supplementStateInfo[item.key]} />
            {complete.indexOf(item.key) == -1 ? <p style={{ paddingRight: 0, border: 0, marginRight: 15, float: "left", paddingTop: 10 }}>资料不完整</p> : <p style={{ paddingRight: 0, border: 0, marginRight: 15, float: "left", paddingTop: 10 }}>资料已采齐</p>}
            <Button style={{ paddingRight: 0, border: 0, marginLeft: 15, outline: 0 }} onClick={() => this.onTakePicture(item.key)}>
              <span>拍照上传</span>
              <Icon type="camera" />
            </Button>
          </div>
        </div>,
        <div key={"u" + item.key} className="clearfix" style={{ padding: "15px 0" }}>
          <Upload {...uploadProps}
            data={{
              ...uploadData,
              dataType: item.key - 1,
            }}
            onChange={({ file, fileList }) => this.onUploadChange({ file, fileList, key: item.key })}
            onRemove={(file) => this.onUploadRemove({ file, fileList, key: item.key })}
            fileList={fileList[item.key]}
            onPreview={(file) => this.handlePreview(file)}
          >
            {uploadButton}
          </Upload>
        </div>
      ]
    })
  }
  makeFile(files) {
    let newArry = []
    let currentTime = '?' + new Date().getTime()
    if (files != undefined) {
      files.map((item, index) => {
        newArry.push(
          {
            uid: index,
            name: index + 'xxx.png',
            url: fileSource + item + currentTime
          }
        )
      })
    }
    return newArry
  }
  componentDidMount() {
    this.loadBasicInfoList()
    const {
      number, fileList, isEditMatter, memoText, supplementState, infoId, vedioList, matterOtherData, name
    } = this.props
    if (isEditMatter) {
      sessionStorage.setItem("infoChecked", infoId)
      let files = {}
      let fileArry0 = [fileList["0"]]
      let fileArry1 = [fileList["1"]]
      let fileArry2 = [fileList["2"]]
      let fileArry3 = [fileList["3"]]
      let fileArry4 = [fileList["4"]]
      files["1"] = this.makeFile(fileArry0["0"])
      files["2"] = this.makeFile(fileArry1["0"])
      files["3"] = this.makeFile(fileArry2["0"])
      files["4"] = this.makeFile(fileArry3["0"])
      files["5"] = this.makeFile(fileArry4["0"])
      this.setState({
        fileList: files,
        memoText: memoText,
        supplementStateInfo: supplementState,
        vedioList: vedioList,
        name: name
      })
      this.props.dispatch({
        type: 'matter/updateState',
        payload: {
          infoChecked: infoId
        }
      })

    }
    window.addEventListener('message', (e) => {
      const {
        getVedioList,
      } = this.props
      let filePath = e.data
      let currentArry = this.state.vedioList
      currentArry.push(filePath)
      this.setState({
        vedioList: currentArry
      })
      let newArry = []
      for (let index = 0; index < currentArry.length; index++) {
        newArry.push(currentArry[index].filePath)
      }
      getVedioList(newArry)
    }, false)

  }
  render() {
    const {
      form, selectedRowKeysClient, selectedRowKeysORG, toShowAddView, showAddView, getPartyClientArry, PartyClientKeys, PartyClientList,
      getPartyORGArry, PartyORGKeys, PartyORGList, confirmToClientAndmatterName, partyList, choiceMatterToclientAndMatterView, getCopies, heelPartyList, getTagList,
      getVedioList, showCascader, number, acceptance, memoList, infoId
    } = this.props
    const {translation ,currency} = acceptance
    const { basicInfoList, tagList } = this.props.matter
    const { modalType, modalVisible, tagNameArry, ExamineModalVisible, examineData, previewVisible, previewImage, showVedio, vedioList, memoText, name } = this.state
    const { validateFields, getFieldDecorator, getFieldsValue, resetFields } = form;
    const options = this.loadMatterNamesExpandableList(cascadeData)
    const checkBoxTag = () => {
      if (memoList != undefined && memoList != [] && memoList != null) {
        let result = memoList.map((item) => {
          return { label: item.organizationName, value: item.organizationId, id: item.id }
        })
        return (
          <CheckboxGroup options={result} onChange={onChangeCheckbox} />
        )
      } else {
        return null
      }
    }

    const onChangeCheckbox = (checkedValues) => {
      let tags = {}
      let result = checkedValues.map((item) => {
        if (!tags[item]) {
          tags[item] = true
        }
      })
      const { acceptance } = this.props
      const { matterOtherData } = acceptance
      const matterId = sessionStorage.getItem("matterId")
      if (!matterOtherData[matterId]) {
        matterOtherData[matterId] = {}
      }
      matterOtherData[matterId]["tags"] = tags
    }
    const choiceMatter = (value) => {
      this.setState({
        name: value[value.length - 1]
      })
      choiceMatterToclientAndMatterView(value[value.length - 1])
    }
    const copies = (value) => {
      getCopies(value)
    }
    const clickTop = (index) => {
      if (index == 0) {
        return
      }
      let currentData = partyList[index]
      let nextData = partyList[index - 1]
      partyList[index] = nextData
      partyList[index - 1] = currentData
      heelPartyList(partyList)
    }
    const clickDown = (index) => {
      if (index == partyList.length - 1) {
        return
      }
      let currentData = partyList[index]
      let nextData = partyList[index + 1]
      partyList[index] = nextData
      partyList[index + 1] = currentData
      heelPartyList(partyList)
    }
    const columns = [{
      title: '',
      dataIndex: 'name',
      key: 'name',
    }, {
      title: '',
      dataIndex: 'id',
      key: 'id',
    }, {
      title: '',
      dataIndex: 'address',
      key: 'address',
      render: (text, row, index) => {
        return (<div>
          <a style={{ fontSize: 18 }} onClick={() => { clickTop(index) }}>↑</a>
          <a style={{ fontSize: 18 }} onClick={() => { clickDown(index) }}>↓</a>
        </div>)
      },
    }

    ];
    const clientColumns = [{
      title: '姓名',
      dataIndex: 'name',
      render: text => text,
    }, {
      title: '身份证号',
      dataIndex: 'idCard',
    }]
    const ORGColumns = [{
      title: '名称',
      dataIndex: 'name',
      render: text => text,
    }, {
      title: '统一社会信用代码',
      dataIndex: 'creditCode',
    }]
    const isFooter = () => {
      return (
        <div>
          <a href="javascript:;" style={{
            width: '100%', textAlign: 'center', display: 'block', padding: 10, borderWidth: 1, borderStyle: 'dashed', borderRadius: 4, borderColor: '#D9D9D9',
          }} onClick={() => showAddViewFunction(true)}>
            + 新增成员 </a>
          {showAddView ? <div style={{ position: 'absolute', zIndex: 999, top: 14, backgroundColor: 'white', width: '100%', borderRadius: 4, borderWidth: 1, borderStyle: 'solid', borderColor: '#cccccc', }}>
            <Tabs defaultActiveKey="1" onChange={callback} tabBarExtraContent={
              <div>
                <Button onClick={() => cancle()}>取消</Button>
                <Button type="primary" style={{ marginLeft: 20, marginRight: 10 }} onClick={() => confirm()}>确认</Button>
              </div>
            } style={{ marginTop: 10 }}>
              <TabPane tab="自然人" key="1">
                <Table
                  rowKey={i => i.idCard}
                  rowSelection={{ onChange: partyClientArry, selectedRowKeys: PartyClientKeys }}
                  columns={clientColumns} dataSource={selectedRowKeysClient} bordered
                  pagination={false} />
              </TabPane>
              <TabPane tab="机构" key="2">
                <Table
                  rowKey={i => i.creditCode}
                  rowSelection={{ onChange: partyORGArry, selectedRowKeys: PartyORGKeys }}
                  columns={ORGColumns} dataSource={selectedRowKeysORG} bordered
                  pagination={false} />
              </TabPane>
            </Tabs>
          </div> : null}
        </div>
      )
    }

    const showAddViewFunction = (visible) => {
      toShowAddView(visible)
    }
    const cancle = () => {
      toShowAddView(false)
    }
    const callback = () => {

    }
    const partyClientArry = (selectedRowKeys, selectedRows) => {
      getPartyClientArry(selectedRowKeys, selectedRows)
    }
    const partyORGArry = (selectedRowKeys, selectedRows) => {
      getPartyORGArry(selectedRowKeys, selectedRows)
    }
    const confirm = () => {
      let currentPartyList = []
      PartyORGList.map((Item) => {
        currentPartyList.push({ name: Item.name, id: Item.creditCode, partyType: '1' })
      })
      PartyClientList.map((Item) => {
        currentPartyList.push({ name: Item.name, id: Item.idCard, partyType: '0' })
      })
      confirmToClientAndmatterName(currentPartyList, PartyClientKeys, PartyORGKeys)
      toShowAddView(false)
    }

    const addDefaultItemsDispatch = (data) => {
      this.props.dispatch({
        type: 'matter/addDefaultItems',
        payload: data
      })
      this.setState({ modalVisible: false, tagNameArry: [] })
    }
    const modalProps = {
      modalType,
      item: modalType === 'create' ? {} : {},
      okText: '确定',
      cancelText: '取消',
      tagList: tagList,
      visible: modalVisible,
      maskClosable: false,
      tagNameArry,
      width: 640,
      //confirmLoading: loading.effects['role/update'],
      title: `${modalType === 'create' ? '新增事项预设' : 'Update Manager'}`,
      wrapClassName: 'vertical-center-modal',
      onOk(data) {
        addDefaultItemsDispatch(data)
      },
      onCancel: () => {
        this.setState({ modalVisible: false })
      },
      // checkedValues
      getTagCheckBoxArry: (checkedValues) => {
        this.setState({ tagNameArry: checkedValues })
      }
    }
    const ExamineModalProps = {
      okText: '确定',
      cancelText: '取消',
      width: 640,
      visible: ExamineModalVisible,
      examineData,
      title: '查看详情',
      wrapClassName: 'vertical-center-modal',
      onOk: () => {
        this.setState({
          ExamineModalVisible: false
        })
      },
      onCancel: () => {
        this.setState({
          ExamineModalVisible: false
        })
      },
    }
    const examineClick = (item) => {
      this.setState({
        examineData: item,
        ExamineModalVisible: true,
      })
    }
    const setVedioStart = () => {
      this.setState({
        showVedio: true
      })
    }
    const setVedioEnd = () => {
      this.setState({
        showVedio: false
      })
    }
    const choiceTranslation = (e) => {
      this.props.dispatch({
        type: 'acceptance/updateState',
        payload: {
          translation: e.target.value
        }
      })
    }
    const choiceCurrency = (e) => {
      this.props.dispatch({
        type: 'acceptance/updateState',
        payload: {
          currency: e.target.value
        }
      })
    }
    return (
      <div style={{ width: "100%", flexDirection: "row", display: 'flex', backgroundColor: '#F8F8F8', marginTop: 20, }}>
        {/* 左边 */}
        <div style={{ width: "50%", backgroundColor: 'white', marginRight: 10, paddingLeft: 26, paddingTop: 20, paddingBottom: 20, borderRadius: 6, paddingRight: 30, }}>
          <span>
            <h2>基本信息</h2>
          </span>
          <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
          <Form layout={"horizontal"} style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
            <Form.Item
              // label=" 选择事项:"
              style={{ marginRight: 30, width: 294 }}
            >
              <p>已选事项: {name}</p>
              {getFieldDecorator('name', {
                initialValue: ''
              })(
                <Cascader options={options} onChange={choiceMatter} placeholder="请选择" showSearch={true} expandTrigger='hover'
                // popupVisible={true}
                />
              )}
            </Form.Item>
            <Form.Item
              // label=" 选择事项:"
              style={{ marginRight: 30, width: 294 }}
            >
              <p>译文:</p>
              {getFieldDecorator('translation', {
                initialValue: translation ||''
              })(
                <Input  onChange={choiceTranslation} placeholder="请输入译文" showSearch={true} expandTrigger='hover'/>
              )}
            </Form.Item>
          </Form>
          <Form layout={"horizontal"} style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
            <Form.Item
              label=" 选择份数:"
              style={{ marginRight: 30, width: 294 }}
            >
              {getFieldDecorator('dds', {
                initialValue: number
              })(
                <InputNumber
                  min={1}
                  max={99}
                  onChange={copies}
                />
              )}
            </Form.Item>
            <Form.Item
              label="币种:"
              style={{ marginRight: 30, width: 294 }}
            >
              {getFieldDecorator('currency', {
                initialValue: currency ||''
              })(
                <Input  onChange={choiceCurrency} placeholder="请输入币种" showSearch={true} expandTrigger='hover'/>
              )}
            </Form.Item>
          </Form>
          <span>
            <h2>当事人列表</h2>
          </span>
          <div style={{ position: 'relative' }}>
            <Table dataSource={partyList} columns={columns} style={{ marginTop: 8 }} pagination={false} footer={isFooter} />
          </div>
        </div>
        {/* 右边 */}
        <div style={{ width: "50%", }}>
          <div style={{ width: "100%", marginLeft: 10, paddingLeft: 34, paddingBottom: 38, backgroundColor: 'white', paddingTop: 20, borderRadius: 6, paddingRight: 30, }}>
            <div style={{ overflow: 'hidden' }}>
              <h2 style={{ color: '	#000000', float: 'left' }}>多人视频</h2>
              {showVedio ? <Button type='primary' style={{ float: 'right', width: 70 }} onClick={setVedioEnd}>关闭</Button> : <Button type='primary' style={{ float: 'right', width: 70 }} onClick={setVedioStart}>创建</Button>}
            </div>
            <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
            {showVedio ?
              <div>
                <iframe style={{ border: 0, height: 800, width: "110%" }} src={"http://chat.sip.feelbang.com?username=" + localStorage.getItem("username")}></iframe>
                <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span></div> : null}
            <h3 style={{ color: '	#000000', }}>多人视频录像列表</h3>
            <div>
              {vedioList.length === 0 ? this.showNoDta() : this.showTheVedioList()}
            </div>
          </div>
          <div style={{ width: "100%", marginLeft: 10, paddingLeft: 34, paddingBottom: 38, backgroundColor: 'white', paddingTop: 20, borderRadius: 6, paddingRight: 30, marginTop: 15 }}>
            <h2>附件资料</h2>
            <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
            {this.renderUpload()}
            <div style={{ padding: "15px 0", borderBottom: '1px solid #f0f0f0', overflow: 'hidden' }}>
              <h3 style={{ float: 'left', margin: 0 }}>备注信息</h3>
            </div>
            <div style={{ marginTop: 24, marginBottom: 4 }}>
              {checkBoxTag()}
            </div>
            <div className="clearfix" style={{ padding: "15px 0" }}>
              <TextArea row={4} onChange={(e) => this.onTextInputChange(e)}
                value={memoText}
              />
            </div>
          </div>
          {/* {modalVisible && <BasicInfoForm {...modalProps} />}
          {ExamineModalVisible ? <ExamineModal {...ExamineModalProps} /> : null} */}
          <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel} width={window.innerWidth * 0.6 || document.documentElement.clientWidth * 0.6 || document.body.clientWidth * 0.6} >
            <div style={{ textAlign: 'center', width: "100%" }}>
              <img alt="example" style={{ width: '90%', height: window.innerHeight * 0.7 || document.documentElement.clientHeight * 0.7 || document.body.clientHeight * 0.7 }} src={previewImage} />
            </div>
          </Modal>
          <CrossBrowserDriver ref="crossBrowserDriver"
            onBiokey={(fingerData) => {
            }}
            onBarcodeDecode={(data) => {

            }}
            onAssistPicture={(base64) => {

            }}
            onTakePicture={(info) => {
              // const { fileList } = this.state
              // info = info.replace(new RegExp(" ", "gm"), '');
              // info = info.replace(new RegExp("{", "gm"), '{"');
              // info = info.replace(new RegExp("}", "gm"), '"}');
              // info = info.replace(new RegExp("code=", "gm"), 'code":"');
              // info = info.replace(new RegExp(",fileId=", "gm"), '","fileId":"');
              // let infoJSON = {}
              // try {
              //   infoJSON = JSON.parse(info)
              // } catch (e) {
              //   message.error("服务器返回数据格式异常", 5);
              // }
              // if (!infoJSON.fileId) {
              //   return
              // }
              // const url = fileSource + infoJSON.fileId
              // const list = fileList[String(this.pictureType)] || []
              // list.push({
              //   uid: new Date().getTime(),
              //   name: info.fileId,
              //   status: 'done',
              //   url: url
              // })
              // fileList[String(this.pictureType)] = list
              // this.setState({ fileList })
              let a = info.json()
              a.then(obj => {
                const { fileList } = this.state
                let infoJSON = {}
                try {
                  infoJSON = obj
                } catch (e) {
                  message.error("服务器返回数据格式异常", 3);
                  return
                }
                if (!infoJSON.fileId) {
                  message.error(infoJSON.msg || "文件上传失败", 3);
                  return
                }
                const url = fileSource + infoJSON.fileId
                const list = fileList[String(this.pictureType)] || []
                list.push({
                  uid: new Date().getTime(),
                  name: infoJSON.fileId,
                  status: 'done',
                  url: url
                })
                fileList[String(this.pictureType)] = list
                this.setState({
                  fileList,
                })
              })
            }} />
        </div>
      </div>
    )
  }
}

const MatterForm = Form.create()(NewMatter)

export default connect(({ acceptance, matter, loading }) => ({ acceptance, matter, loading }))(MatterForm)
