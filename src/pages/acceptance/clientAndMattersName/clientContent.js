import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Tabs, Button, Icon, Table } from 'antd'
const TabPane = Tabs.TabPane;

const ClientContent = ({ onRowClick, goBackToIndex, showClient, choiceClientFuction, choiceMatterFuction, clientList, onChangeClientLsits, paginationClientList,
  selectedRowKeysClient, getClientCurrentList,orgListData, onChangeMatterPage, paginationMatter, getORGCurrentList,selectedRowKeysORG,
  clientRowKeys,ORGRows,
}) => {
  const goToBack = () => {
    goBackToIndex()
  }
  const saveData = () => {

  }
  const reset = () => {

  }
 
  const choiceClient = () => {
    choiceClientFuction()
  }
  const choiceMatter = () => {
    choiceMatterFuction()
  }
  const deleteCurrent = (record) => {
    var result = [];
    let currentClientRowKeys = [];
    for (var i = 0; i < clientRowKeys.length; i++) {
      if (clientRowKeys[i] != record.idCard) {
        currentClientRowKeys.push(clientRowKeys[i]);
      }
    }
    for (var i = 0; i < selectedRowKeysClient.length; i++) {
      if (selectedRowKeysClient[i] != record) {
        result.push(selectedRowKeysClient[i]);
      }
    }
    getClientCurrentList(result,currentClientRowKeys)
  }
  const deleteCurrentORG = (record) => {
    var result = [];
    let currentORGRowKeys = [];
    for (var i = 0; i < ORGRows.length; i++) {
      if (ORGRows[i] != record.creditCode) {
        currentORGRowKeys.push(ORGRows[i]);
      }
    }
    for (var i = 0; i < selectedRowKeysORG.length; i++) {
      if (selectedRowKeysORG[i] != record) {
        result.push(selectedRowKeysORG[i]);
      }
    }
    getORGCurrentList(result,currentORGRowKeys)
  }
  const clientColumns = [{
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
  }, {
    title: '身份证号',
    dataIndex: 'idCard',
    key: 'idCard',
  },
    {
    title: '操作',
    dataIndex: 'address',
    key: 'address',
    render: (text, record) => (
      <span>
        <a onClick={() => deleteCurrent(record)}>删除</a>
      </span>
    ),
    }
  ];
  const matterColumns = [{
    title: '名称',
    dataIndex: 'name',
    key: 'name',
    render: text => text,
  }, {
    title: '统一社会信用代码',
    dataIndex: 'creditCode',
    key: 'creditCode',
    },
    {
      title: '操作',
      dataIndex: 'address',
      key: 'address',
      render: (text, record) => (
        <span>
          <a onClick={() => deleteCurrentORG(record)}>删除</a>
        </span>
      ),
      }];

  const allClientDolumns = [{
    title: '姓名',
    dataIndex: 'name',
    render: text => text,
  }, {
    title: '身份证号',
    dataIndex: 'idCard',
  }]
  const allMatterColumns = [{
    title: '名称',
    dataIndex: 'name',
    key: 'name',
    render: text => text,
  }, {
    title: '统一社会信用代码',
    dataIndex: 'creditCode',
    key: 'creditCode',
  }];
  const onSelectORGChange = (selectedRowKeys, selectedRows) => {
    // let currentArrys = filterORG(AllORGList, selectedRowKeys)
    getORGCurrentList(selectedRows,selectedRowKeys)
  }
  const onSelectChange = (selectedRowKeys, selectedRows) => {
    // let currentArry = filterClient(AllClientList, selectedRowKeys)
    getClientCurrentList(selectedRows, selectedRowKeys)
    // console.log(AllClientList,'AllClientList')
  }
 
  const filterClient = (arr1, arr2) => {
    return arr1.filter((ele) =>
      arr2.filter((x) => x === ele.idCard).length > 0
    );
  }
  const filterORG = (arr1, arr2) => {
    return arr1.filter((ele) =>
      arr2.filter((x) => x === ele.creditCode).length > 0
    );
  }
  return (
    <div style={{ width: "100%", flexDirection: "row", display: 'flex', backgroundColor: '#F8F8F8', marginTop: 20, }}>
      {/* 左边 */}
      <div style={{ width: "50%", backgroundColor: 'white', marginRight: 10, paddingLeft: 26, paddingTop: 20, paddingBottom: 20, borderRadius: 6, paddingRight: 30, }}>
        <span>
          <h2 style={{ color: '	#000000' }}>自然人</h2>
        </span>
        <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
        <Table columns={clientColumns} dataSource={selectedRowKeysClient} pagination={false} bordered />
        <div style={{ marginTop: 40 }}>
          <h2 style={{ color: '	#000000' }}>机构</h2>
        </div>
        <span style={{ display: 'block', width: "100%", height: 1, backgroundColor: '#e2e2e2', marginBottom: 25, marginTop: 14 }}></span>
        <Table columns={matterColumns} dataSource={selectedRowKeysORG} pagination={false} style={{ paddingBottom: 110 }} bordered />
      </div>
      {/* 右边 */}
      <div style={{ width: "50%", marginLeft: 10, paddingLeft: 34, paddingBottom: 38, backgroundColor: 'white', paddingTop: 20, borderRadius: 6, paddingRight: 30, }}>
        <Tabs defaultActiveKey="1" >
          <TabPane tab="自然人" key="1">
            <Table
              rowKey={i => i.idCard}
              rowSelection={{
                onChange: onSelectChange,
                selectedRowKeys:clientRowKeys
              }} columns={allClientDolumns} dataSource={clientList} bordered onChange={onChangeClientLsits} pagination={paginationClientList} />
          </TabPane>
          <TabPane tab="机构" key="2">
            <Table rowSelection={{
              onChange: onSelectORGChange,
              selectedRowKeys:ORGRows
            }} columns={allMatterColumns} dataSource={orgListData} bordered onChange={onChangeMatterPage}
              rowKey={i => i.creditCode} pagination={paginationMatter} />
          </TabPane>
        </Tabs>
      </div>
    </div>
  )
}

export default ClientContent
