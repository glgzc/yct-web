import React from 'react'

import { connect } from 'dva'
import { Row, Col, Button, Select, Table, Checkbox, Form, InputNumber, Radio } from 'antd'
import noData from '../../../assets/noData.png';
import BasicInfoForm from '../../basicInfo/form'
import ExamineModal from './examineModal'
const Option = Select.Option;
const RadioGroup = Radio.Group;
const MatterContent = ({ goToNewMatterView, matter, toEditMatter, deleteMatterItemToClientAndMatter, acceptance, dispatch, isEditMatter, isEditBatch
}) => {
  const { matterOtherData, tagList, showModalDefault, tagModalType, tagNameArry, basicInfoList, ExamineModalVisible, infoChecked,
    examineData,infoId} = acceptance
  console.log(infoId, 'infoId')
  let totalFee = 0
  Object.keys(matterOtherData).map((key) => {
    const item = matterOtherData[key]

    totalFee = totalFee + parseFloat(item["cost"] || 0)

  })
  const deleteMatterItem = (index, id) => {
    matter.splice(index, 1)
    delete matterOtherData[id]
    deleteMatterItemToClientAndMatter(matter)
  }
  const editMatter = (index, row) => {
    sessionStorage.setItem("matterId", row.id)
    sessionStorage.setItem("editIndex", index)
    toEditMatter(index, row)
  }
  const columns = [{
    title: '序号',
    dataIndex: 'index',
  }, {
    title: '事项名称',
    dataIndex: 'name',
    render: text => text,
  },
  {
    title: '当事人',
    dataIndex: 'partyName',
    render: text => text,
  }, {
    title: '份数',
    dataIndex: 'number',
  },
  {
    title: '操作',
    dataIndex: 'sd',
    key: 'sd',
    render: (text, row, index) => {
      return (<div>
        <a onClick={() => { deleteMatterItem(index, row["id"]) }} style={{ marginRight: 12 }}>删除</a>
        <a onClick={() => { editMatter(index, row) }}>编辑</a>
      </div>)
    },
  }
  ];
  const onFeeChange = (val, id) => {
    const { matterOtherData } = acceptance
    if (!matterOtherData[id]) {
      matterOtherData[id] = {}
    }
    matterOtherData[id]["cost"] = val
    dispatch({
      type: "acceptance/currentMethod",
      payload: {
        matterOtherData
      }
    })
  }
  const onFeeCheckChange = (e) => {
    const checked = e.target.checked
    const { matterOtherData } = acceptance
    matterOtherData["feeChecked"] = checked ? 1 : 0
    dispatch({
      type: "acceptance/currentMethod",
      payload: {
        matterOtherData
      }
    })
  }
  const changeLevel = (value) => {
    const { matterOtherData } = acceptance
    matterOtherData["approvalLevel"] = value
    dispatch({
      type: "acceptance/currentMethod",
      payload: {
        matterOtherData
      }
    })
  }
  const showNoDta = () => {
    return (
      <div style={{ textAlign: "center" }}>
        <img
          style={{ marginTop: 50, display: 'inline-block', marginBottom: 50 }}
          src={noData}
        />
      </div>
    )
  }
  const payList = () => {
    return matter.map((item, index) => {
      let val = 0
      if (matterOtherData[item.id]) {
        val = matterOtherData[item.id]["cost"] || ""
      }
      return (
        <div style={{ overflow: 'hidden', margin: '5px 0', width: '100%' }}>
          <div style={{ overflow: 'hidden', float: "left", lineHeight: "32px" }}>
            <span>{index + 1}、</span>
            <span style={{ marginLeft: 10 }}>{item.name}</span>
          </div>

          <div style={{ overflow: 'hidden', float: 'right', width: 90 }}>
            <div style={{ float: "left", marginRight: 10, lineHeight: "32px" }}>¥:</div>
            <InputNumber style={{ float: 'left', width: 60 }}
              value={val}
              min={0}
              onChange={(val) => onFeeChange(val, item.id)} />
          </div>
        </div>
      )
    })
  }
  const loadTagList = () => {
    dispatch({
      type: 'acceptance/queryTagLsit',
      payload: {}
    })
  }
  const modalProps = {
    modalType: tagModalType,
    item: tagModalType === 'create' ? {} : {},
    okText: '确定',
    cancelText: '取消',
    tagList: tagList,
    visible: showModalDefault,
    maskClosable: false,
    tagNameArry: tagNameArry,
    width: 640,
    //confirmLoading: loading.effects['role/update'],
    title: `${tagModalType === 'create' ? '新增事项预设' : '修改事项预设'}`,
    wrapClassName: 'vertical-center-modal',
    onOk(data) {
      dispatch({
        type: 'acceptance/addDefaultItems',
        payload: data
      })
    },
    onCancel: () => {
      dispatch({
        type: 'acceptance/updateState',
        payload: {
          showModalDefault: false,
        }
      })
    },
    // checkedValues
    getTagCheckBoxArry: (checkedValues) => {
      dispatch({
        type: 'acceptance/updateState',
        payload: {
          tagNameArry: checkedValues
        }
      })
    }
  }
  const ExamineModalProps = {
    okText: '确定',
    cancelText: '取消',
    width: 640,
    visible: ExamineModalVisible,
    examineData,
    title: '查看详情',
    wrapClassName: 'vertical-center-modal',
    onOk: () => {
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          ExamineModalVisible: false
        }
      })
    },
    onCancel: () => {
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          ExamineModalVisible: false
        }
      })
    },
  }
  const examineClick = (item) => {
    dispatch({
      type: 'acceptance/examine',
      payload: {
        examineData: item,
      }
    })
  }
  const onInfoCheckChange = (e) => {
    const val = e.target.value
    sessionStorage.setItem("infoChecked", val)
    dispatch({
      type: 'acceptance/currentMethod',
      payload: {
        infoChecked: val,
        infoId:val,
      }
    })
  }
  return (
    <Row gutter={24} style={{ width: "100%", backgroundColor: '#F8F8F8', marginTop: 20, }}>
      {/* 左边 */}
      <Col md={{ span: 12 }} >
        <div style={{ background: 'white', padding: 20 }}>
          <div style={{ overflow: 'hidden', paddingBottom: 20, marginBottom: 20, borderBottom: '1px solid #f0f0f0' }}>
            <h2 style={{ float: 'left', margin: 0 }}>事项管理</h2>
            <Button style={{ float: 'right' }} type='primary' onClick={goToNewMatterView}>添加</Button>
          </div>
          <Table columns={columns} dataSource={matter} pagination={false} />
        </div>
      </Col>
      {/* 右边 */}
      <Col md={{ span: 12 }}>
        <div style={{ background: 'white', padding: 20 }}>
          <div style={{ overflow: 'hidden', paddingBottom: 20, marginBottom: 20, borderBottom: '1px solid #f0f0f0' }}>
            <h2 style={{ float: 'left', margin: 0 }}>台账信息</h2>
            <Checkbox style={{ float: 'right', marginTop: 5 }} onChange={(e) => onFeeCheckChange(e)} checked={matterOtherData["feeChecked"] ? true : false}>已付费</Checkbox>
          </div>

          {matter.length === 0 ? showNoDta() : payList()}

          <div style={{ overflow: 'hidden', paddingTop: 20, marginTop: 20, borderTop: '1px solid #f0f0f0' }}>
            <h2 style={{ color: '	#E02020', fontSize: 16, float: 'left' }}>总计:¥{totalFee}</h2>
            <span style={{ float: 'right', display: 'block', marginTop: 2 }}>共{matter.length}项</span>
          </div>
          <div style={{ overflow: 'hidden', paddingBottom: 20, marginBottom: 20, borderBottom: '1px solid #f0f0f0', marginTop: 20, borderTop: '1px solid #f0f0f0', paddingTop: 20 }}>
            <h2 style={{ float: 'left', margin: 0 }}>审批信息</h2>
          </div>
          <div style={{ overflow: 'hidden', paddingBottom: 20, marginBottom: 20, marginTop: 20, }}>
            <h3 style={{ float: 'left', margin: 0, marginTop: 4 }}>审批等级</h3>
            <Select defaultValue="请选择" style={{ width: 220, float: 'left', marginLeft: 80 }} onChange={changeLevel} value={matterOtherData["approvalLevel"]}>
              <Option value="1">部门审批出证</Option>
              <Option value="2">审签室审批出证</Option>
              <Option value="3">主任审批出证</Option>
            </Select>
          </div>
          {showModalDefault && <BasicInfoForm {...modalProps} />}
          {ExamineModalVisible ? <ExamineModal {...ExamineModalProps} /> : null}
          <div style={{ overflow: 'hidden', paddingBottom: 20, marginBottom: 20, borderBottom: '1px solid #f0f0f0', marginTop: 20, borderTop: '1px solid #f0f0f0', paddingTop: 20 }}>
            <h2 style={{ float: 'left', marginBottom: 0 }}>公证事项预设信息</h2>
            <Button type="primary" size="small" style={{ float: "right" }} onClick={loadTagList}>新增</Button>
          </div>
          <RadioGroup
            style={{ width: '100%' }}
            onChange={onInfoCheckChange}
          value={infoId+ '' ||''}
          >
            {
              basicInfoList.map((item) => {
                return (
                  <div style={{ overflow: 'hidden', padding: '10px 0', borderBottom: '1px solid #f6f6f6' }}>
                    <Radio
                      value={String(item.id)} style={{
                        display: 'block',
                        width: 120,
                        float: "left",
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        whiteSpace: "nowrap"
                      }}>
                      {item.title || ''}
                    </Radio>
                    <div style={{ float: 'right' }}>
                      <a onClick={() => examineClick(item)}>查看</a>
                    </div>
                  </div>
                )
              })
            }
          </RadioGroup>
        </div>
      </Col>
    </Row>
  )
}

export default connect(({ acceptance, loading }) => ({ acceptance, loading }))(MatterContent)
