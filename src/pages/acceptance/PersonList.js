import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const PersonList = ({list, showDetailModal}) => {
  let number = 1
  const showDetail = (record)=>{
    showDetailModal(record)
  }
  const columns = [
    {
      title: '序号',
      key: 'id',
      render: ()=>{
        return number++
      }
    }, {
      title: '类型',
      dataIndex: 'partyType',
      key: 'partyType',
      render: (text)=>{
        return text === 0 ? '自然人' : (text === 1 ? '机构' : (text === 2 ?'法人' : '委托人'))
      }
    }, {
      title: '姓名/名称',
      dataIndex: 'partyName',
      key: 'partyName'
    }, {
      title: '性别/类型',
      dataIndex: 'sex',
      key: 'sex'
    }, {
      title: '证件号码/统一社会信用代码',
      dataIndex: 'card',
      key: 'card'
    }, {
      title: '出生日期/成立时间',
      dataIndex: 'birth',
      key: 'birth'
    }, {
      title: '地址',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: '联系电话',
      dataIndex: 'mobile',
      key: 'mobile'
    }, {
      title: '资料',
      dataIndex: 'operation',
      key: 'operation',
      render: (text, record) => <div style={{width:40,paddingLeft:16}}><a onClick={()=>showDetail(record)}>查看</a></div>
    }
    
  ]

  return (
    <div style={{marginTop:'15px',overflow:'auto'}}>
      <Table
        dataSource={list}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        pagination={false}
        width={'100%'}
        rowKey={record => record.sort}
      />
    </div>
  )
}
export default PersonList
