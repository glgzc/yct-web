import React from 'react'
import PropTypes from 'prop-types'
import { Form, Tree, Modal, Card, Button, Input, Checkbox, Row, Col, InputNumber } from 'antd'
// import styles from './document.less'
import InputArry from './InputArry'
import { config } from 'utils';
import { IEVersion } from 'utils/util';
const { fileSource } = config;


const document = ({
  onOk,
  addPerson,
  getWordInfo,
  personIndex,
  form,
  ...modalPropsPeople
}) => {
  const { validateFields, getFieldDecorator, getFieldsValue, resetFields } = form;
  const modalPropsPeoples = {
    ...modalPropsPeople,
    onOk:click,
    // onCancel: visibleCreateApi ? createApiCancel : onCancel
  }
  console.log(getWordInfo, 'getWordInfo')
  let matterArry = []
  let matter = {}
  let matterOthers = {}
  if (getWordInfo.matter != undefined) {
    matter = JSON.parse(getWordInfo.matter)
    if (matter.other != undefined) {
      matterOthers = matter.other
    } else {
      matterOthers = {}
    }
    for (const key in matter) {
      matterArry.push(key)
    }
  } else {
    matter ={}
  }
 
  let proveDataArry = []
  let proveData = {}
  if (getWordInfo.matter != undefined) {
    proveData = JSON.parse(getWordInfo.proveData)
    for (const key in proveData) {
      proveDataArry.push(key)
    }
  }
  let proveDataOther = []
  if(JSON.stringify(proveData) != '{}' && proveData.other != undefined && proveData.other != null){
    proveDataOther = JSON.parse(proveData.other)
  }
  let showContent = {}
  if(getWordInfo.content){
    showContent = JSON.parse(getWordInfo.content)
  }
  // const documentModalOpts = {
  //   ...modalProps,
  //   onOk: visibleCreateApi ? handleOk : createApiOK,
  //   onCancel: visibleCreateApi ? createApiCancel : onCancel
  // }
  const click = () => {
    const data = getFieldsValue()
    // 获取当事人
    let userInfo = []
    for (let index = 0; index < personIndex; index++) {
      userInfo.push(
        {agentName:data['agentName'+index],name:data['name'+index],address:data['address'+index],sex:data['sex'+index],mobile:data['mobile'+index],card:data['card'+index]}
      )
    }
    // 公证事项 matter
    let currentMatter = {}
    for (let index = 0; index < data.matter.length; index++) {
      currentMatter[data.matter[index]] = data.matter[index]
    }
    currentMatter.other = data.otherMatter
    // 证明材料
    let currentProveData ={}
    let other = []
    for (let index = 0; index < 4; index++) {
      other.push(
        JSON.stringify({id:index+1,content:data['otherProve'+index]})
        )
    }
    for (let index = 0; index < data.proveData.length; index++) {
      currentProveData[data.proveData[index]] = data.proveData[index]
    }
    currentProveData.other = JSON.stringify(other)
    let currentContent = {}
    currentContent.applyTime=data.applyTime
    currentContent.autograph = data.autograph
    currentContent.content= '申请人（代理人）承诺：'+data.content
    let currentData = {}
    currentData.title = data.title
    currentData.userInfo = JSON.stringify(userInfo)
    currentData.matter = JSON.stringify(currentMatter)
    currentData.use = data.use 
    currentData.number = data.number
    currentData.proveData = JSON.stringify(currentProveData)
    currentData.content = JSON.stringify( currentContent)
    currentData.matterId = getWordInfo.matterId
    let newData = JSON.stringify(currentData)
    // console.log(newData,'newData')
    onOk(newData,getWordInfo.matterId)
  }
  const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }
  const formItemStyle = {
    marginRight: 20, width: 300
  }
  const showPeopleFormItem = () => {
    let userInfo = {}

    if (getWordInfo.userInfo != undefined) {
      userInfo = JSON.parse(getWordInfo.userInfo)
    }
    let formItemArry = []
    for (let index = 0; index < personIndex; index++) {
      formItemArry.push(
        <div>
          <Form layout={"inline"} >
            <Form.Item label="申请人姓名" style={formItemStyle}>
              {getFieldDecorator('name' + index, { initialValue: getWordInfo.userInfo != undefined ? userInfo[index]!= undefined?userInfo[index].name ||'':'' : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="代理人姓名" style={formItemStyle}>
              {getFieldDecorator('agentName' + index, { initialValue: getWordInfo.userInfo != undefined ? userInfo[index]!= undefined ?userInfo[index].agentName:'' : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="性别" style={formItemStyle}>
              {getFieldDecorator('sex' + index, { initialValue: getWordInfo.userInfo != undefined ?userInfo[index]!= undefined? userInfo[index].sex:'' : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="住址或单位" style={formItemStyle}>
              {getFieldDecorator('address' + index, { initialValue: getWordInfo.userInfo != undefined ? userInfo[index]!= undefined? userInfo[index].address:'': '' })(<Input />)}
            </Form.Item>
            <Form.Item label="身份证件号" style={formItemStyle}>
              {getFieldDecorator('card' + index, { initialValue: getWordInfo.userInfo != undefined ? userInfo[index]!= undefined?userInfo[index].card:'' : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="电话" style={formItemStyle}>
              {getFieldDecorator('mobile' + index, { initialValue: getWordInfo.userInfo != undefined ? userInfo[index]!= undefined? userInfo[index].mobile:'': '' })(<Input />)}
            </Form.Item>
          </Form>
        </div>
      )
    }
    return formItemArry
  }
  return (
    // <div className="text">
    <Modal  {...modalPropsPeoples}  onOk={() => click()}>
      <div>
        <Form layout={"inline"} style={{textAlign:'center'}}>
        <Form.Item label="公证申请表(个人)">
          {getFieldDecorator('title', { initialValue: getWordInfo.title != undefined ? getWordInfo.title : '' })(<Input />)}
        </Form.Item>
        </Form>
        <div style={{ overflow: 'hidden' }}>
          <Button onClick={() => addPerson()} style={{ float: 'right' }}>添加申请人+</Button>
        </div>
        {showPeopleFormItem()}
        <h3 style={{ marginTop: 8, marginBottom: 8 }}>根据情况在下列“□ ”处打“√”标记，未列明的请在“其他”中填写</h3>
        <Form.Item label="申办公证事项">
          {getFieldDecorator('matter', {
            initialValue: matterArry || [],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={4}>
                  <Checkbox value="委托书">委托书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="转委托书">转委托书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="声明书公证">声明书公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="赠与书">赠与书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="赠与合同">赠与合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="受赠书">受赠书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="遗嘱公证">遗嘱公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="继承公证">继承公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="民间借贷合同公证">民间借贷合同公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="财产约定协议公证">财产约定协议公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="离婚协议公证">离婚协议公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="赔偿协议公证">赔偿协议公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="遗赠公证">遗赠公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="收养关系公证">收养关系公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="房屋买卖合同公证">房屋买卖合同公证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value=" 签名（捺印）"> 签名（捺印）</Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        <Form layout={"inline"} >
        <Form.Item label="其他公证事项">
            {getFieldDecorator('otherMatter', {
              initialValue:
                JSON.stringify(matterOthers) != '[{}]' && JSON.stringify(matterOthers) != '{}' ? matter.other :
                ''
            })(<Input />)}
        </Form.Item>
        </Form>
        <Form layout={"inline"} >
          <Form.Item label="公证书用途" style={formItemStyle}>
            {getFieldDecorator('use', {
              initialValue:getWordInfo['use']==null && getWordInfo['use']==''?'':getWordInfo['use']
            })(<Input />)}
          </Form.Item>
          <Form.Item label="公证书份数">
            {getFieldDecorator('number', { initialValue: getWordInfo.number != undefined ? getWordInfo.number : '' })(<InputNumber min={1} max={10} />)}
          </Form.Item>
        </Form>
        <Form.Item label="证明材料">
          {getFieldDecorator('proveData', {
            initialValue: proveDataArry || [],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={4}>
                  <Checkbox value="身份证">身份证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="户口簿">户口簿</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="结婚证">结婚证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="离婚证">离婚证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="房屋所有权证">房屋所有权证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="国有土地使用证">国有土地使用证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="不动产权证书">不动产权证书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="民事判决书">民事判决书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="财产分割协议">财产分割协议</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="亲属关系证明">亲属关系证明</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="死亡证明">死亡证明</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="商品房买卖合同">商品房买卖合同</Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        <Form layout={"inline"} >
          <Form.Item label="提供的其他相关证明材料1">
            {getFieldDecorator('otherProve0', { initialValue:JSON.stringify(proveDataOther) != '[{}]' && JSON.stringify(proveDataOther) != '[]'?JSON.parse(proveDataOther[0]).content:''})(<Input />)}
          </Form.Item>
          <Form.Item label="提供的其他相关证明材料2" >
            {getFieldDecorator('otherProve1', { initialValue:JSON.stringify(proveDataOther) != '[{}]' && JSON.stringify(proveDataOther) != '[]'?JSON.parse(proveDataOther[1]).content:''})(<Input />)}
          </Form.Item>
          <Form.Item label="提供的其他相关证明材料3">
            {getFieldDecorator('otherProve2', { initialValue:JSON.stringify(proveDataOther) != '[{}]' && JSON.stringify(proveDataOther) != '[]'?JSON.parse(proveDataOther[2]).content:''})(<Input />)}
          </Form.Item>
          <Form.Item label="提供的其他相关证明材料4">
            {getFieldDecorator('otherProve3', { initialValue:JSON.stringify(proveDataOther) != '[{}]' && JSON.stringify(proveDataOther) != '[]'?JSON.parse(proveDataOther[3]).content:''})(<Input />)}
          </Form.Item>
        </Form>
        {/* <h3>申请人（代理人）承诺:上述所填内容、提供的资料以及询问记录回答的相关内容均真实、有效，如有隐瞒或不实，我愿意承担由此引起的一切法律责任。</h3> */}
        <Form layout={"inline"}>
          {/* <Form.Item label="申请日期" style={formItemStyle}>
            {getFieldDecorator('applyTime', {
              initialValue: ''
            })(
              <div>
                <Input />
              </div>
            )}
          </Form.Item> */}
          <div style={{overflow:'hidden'}}>
          <Form.Item label="申请日期"  style={{float:'right',marginLeft:20}}>
            {getFieldDecorator('applyTime', { initialValue:showContent.applyTime||''})(<Input />)}
          </Form.Item>
          <Form.Item label="申请人（承诺人）签名" style={{float:'right'}}>
            {getFieldDecorator('autograph', { initialValue:showContent.autograph ||''})(<Input />)}
          </Form.Item>
          <Form.Item label="申请人（代理人）承诺" style={{float:'right'}}>
            {getFieldDecorator('content', { initialValue:showContent.content ||'上述所填内容、提供的资料以及询问记录回答的相关内容均真实、有效，如有隐瞒或不实，我愿意承担由此引起的一切法律责任。'})(<Input />)}
          </Form.Item>
          </div>
         
         
        </Form>
      </div>
    </Modal>
    // </div>


  )
}
function unNull(value, replace = '--') {
  return value ? value : replace;
}
document.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(document)
