import React from 'react'
import PropTypes from 'prop-types'
import { Form, Tree, Modal, Card, Button, Input, Checkbox, Row, Col, InputNumber } from 'antd'
// import styles from './company.less'
import InputArry from './InputArry'
import { config } from 'utils';
import { IEVersion } from 'utils/util';
const { fileSource } = config;


const company = ({
  onOk,
  addPerson,
  agentIndex,
  getCompanyInfo,
  addAgentPerson,
  personIndex,
  form,
  ...modalPropsPeople
}) => {
  const { validateFields, getFieldDecorator, getFieldsValue, resetFields } = form;
  const modalPropsPeoples = {
    ...modalPropsPeople,
    onOk: click,
  }
  let matterArry = []
  let matter = {}
  let matterOther = {}
  let matterOtherValue1 = {}
  let matterOtherValue2 = {}
  if (getCompanyInfo.matter != undefined) {
    matter = JSON.parse(getCompanyInfo.matter)
    if (matter.other != undefined) {
      matterOther = JSON.parse(matter.other)
      matterOtherValue1 = JSON.parse(matterOther[0]).value
      matterOtherValue2 = JSON.parse(matterOther[1]).value
    } else {
      matterOtherValue1 = ''
      matterOtherValue2 = ''
    }
    for (const key in matter) {
      matterArry.push(key)
    }
  }
  let proveDataArry = []
  let proveData = {}
  let proveDataOthers = {}
  let proveDataOtherValue1 = {}
  let proveDataOtherValue2 = {}
  if (getCompanyInfo.proveData != undefined) {
    proveData = JSON.parse(getCompanyInfo.proveData)
    if (proveData.other != undefined) {
      proveDataOthers = JSON.parse(proveData.other)
      proveDataOtherValue1 = JSON.parse(proveDataOthers[0]).value
      proveDataOtherValue2 = JSON.parse(proveDataOthers[1]).value
    } else {
      proveDataOtherValue1 = ''
      proveDataOtherValue2 = ''
    }
    for (const key in proveData) {
      proveDataArry.push(key)
    }
  }
  let showContent = {}
  if (getCompanyInfo.content!= undefined) {
    showContent = JSON.parse(getCompanyInfo.content)
  }
  const click = () => {
    const data = getFieldsValue()
    // 获取当事人
    let userInfo = []
    for (let index = 0; index < personIndex; index++) {
      userInfo.push(
        {
          agentInfo: JSON.stringify({ partyName: data['partyName' + index], sex: data['sex' + index], mobile: data['mobile' + index], card: data['card' + index] }),
          orgInfo: JSON.stringify({ person: data['companyPeopleName' + index], code: data['companyCard' + index], mobile: data['companyMobile' + index], name: data['companyName' + index] })
        })
    }
    // 公证事项 matter
    let currentMatter = {}
    for (let index = 0; index < data.matter.length; index++) {
      currentMatter[data.matter[index]] = data.matter[index]
    }
    let currentMatterOtherArry = []
    currentMatterOtherArry.push(JSON.stringify({ name: '其他', value: data.otherMatter1 }))
    currentMatterOtherArry.push(JSON.stringify({ name: '其他', value: data.otherMatter2 }))
    currentMatter.other = JSON.stringify(currentMatterOtherArry)
    // 证明材料
    let currentProveData = {}
    for (let index = 0; index < data.proveData.length; index++) {
      currentProveData[data.proveData[index]] = data.proveData[index]
    }
    currentProveData['申请公证的文书'] = data.theBook
    let currentProveDataOtherData = []
    currentProveDataOtherData.push(JSON.stringify({ name: '其他', value: data.otherProve0 }))
    currentProveDataOtherData.push(JSON.stringify({ name: '其他', value: data.otherProve1 }))
    currentProveData.other = JSON.stringify(currentProveDataOtherData)
    let currentContent = {}
    currentContent.applyTime = data.applyTime
    currentContent.autograph = data.autograph
    currentContent.mobile = data.content
    let currentData = {}
    currentData.title = data.title
    currentData.userInfo = JSON.stringify(userInfo)
    currentData.matter = JSON.stringify(currentMatter)
    currentData.use = data.use
    currentData.number = data.number
    currentData.proveData = JSON.stringify(currentProveData)
    currentData.content = JSON.stringify(currentContent)
    currentData.matterId = getCompanyInfo.matterId
    let newData = JSON.stringify(currentData)
    onOk(newData, getCompanyInfo.matterId)
  }
  const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }
  const formItemStyle = {
    marginRight: 20, width: 300
  }
  const showPeopleFormItem = () => {
    let userInfo = {}
    if (getCompanyInfo.userInfo != undefined) {
      userInfo = JSON.parse(getCompanyInfo.userInfo)
    }
    let formItemArry = []
    for (let index = 0; index < personIndex; index++) {
      formItemArry.push(
        <div>
          <h3>{'申请人' + (index + 1)}</h3>
          <Form layout={"inline"} >
            <Form.Item label="单位名称" style={formItemStyle}>
              {getFieldDecorator('companyName' + index,{ initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].orgInfo).name  : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="法定代表人(负责人)" style={formItemStyle}>
              {getFieldDecorator('companyPeopleName' + index,{ initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].orgInfo).person  : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="营业执照(社会统一信用代码)" >
              {getFieldDecorator('companyCard' + index,{ initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].orgInfo).code  : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="联系方式" style={formItemStyle}>
              {getFieldDecorator('companyMobile' + index,{ initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].orgInfo).mobile  : '' })(<Input />)}
            </Form.Item>
          </Form>
          <h3>{'代理人' + (index + 1)}</h3>
          <Form layout={"inline"} >
            <Form.Item label="姓名" style={formItemStyle}>
              {getFieldDecorator('partyName' + index, { initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].agentInfo).partyName:'' })(<Input />)}
            </Form.Item>
            <Form.Item label="性别" style={formItemStyle}>
              {getFieldDecorator('sex' + index, { initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].agentInfo).sex:'' })(<Input />)}
            </Form.Item>
            <Form.Item label="身份证件号" style={formItemStyle}>
              {getFieldDecorator('card' + index,{ initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].agentInfo).card:'' })(<Input />)}
            </Form.Item>
            <Form.Item label="电话" style={formItemStyle}>
              {getFieldDecorator('mobile' + index, { initialValue:JSON.stringify(userInfo) != '[{}]' &&JSON.stringify(userInfo) != '{}'? JSON.parse(userInfo[index].agentInfo).mobile :'' })(<Input />)}
            </Form.Item>
          </Form>
        </div>
      )
    }
    return formItemArry
  }
  return (
    <Modal  {...modalPropsPeoples} onOk={() => click()}>
      <div>
        <Form layout={"inline"} style={{ textAlign: 'center' }}>
          <Form.Item label="公证申请表(单位)">
            {getFieldDecorator('title', { initialValue: getCompanyInfo.title != undefined ? getCompanyInfo.title : '' })(<Input />)}
          </Form.Item>
        </Form>
        <div style={{ overflow: 'hidden' }}>
          <Button onClick={() => addPerson()} style={{ float: 'right' }}>添加申请代理人+</Button>
        </div>
        {showPeopleFormItem()}
        <h3 style={{ marginTop: 8, marginBottom: 8 }}>根据情况在下列“□ ”处打“√”标记，未列明的请在“其他”中填写</h3>
        <Form.Item label="申办公证事项">
          {getFieldDecorator('matter', {
            initialValue: matterArry || [],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={4}>
                  <Checkbox value="委托">委托</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="声明">声明</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="保证">保证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="证明签名、指印">证明签名、指印</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="证明印鉴">证明印鉴</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="保全证据">保全证据</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="民事协议">民事协议</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="赠与">赠与（赠与书赠与合同）</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="遗嘱">遗嘱</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="遗赠抚养协议">遗赠抚养协议</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="继承权">继承权</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="提存">提存</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="抵押/质押登记">抵押/质押登记</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="保管遗嘱/遗产">保管遗嘱/遗产</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="代写法律事务文书">代写法律事务文书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="公证法律咨询">公证法律咨询</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="借款合同">借款合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="抵押合同">抵押合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="质押合同">质押合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="保证合同">保证合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="授信合同">授信合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="最高额抵（质）押合同">最高额抵（质）押合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="反担保合同">反担保合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="借款担保合同">借款担保合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="债权文书赋予强制执行效力">债权文书赋予强制执行效力</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value=" 现场监督"> 现场监督</Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        <Form layout={"inline"} >
          <Form.Item label="其他公证事项1">
            {getFieldDecorator('otherMatter1', { initialValue: matterOtherValue1 || '' })(<Input />)}
          </Form.Item>
          <Form.Item label="其他公证事项2">
            {getFieldDecorator('otherMatter2', {
              initialValue:
                matterOtherValue2 ||
                ''
            })(<Input />)}
          </Form.Item>
        </Form>
        <Form layout={"inline"} >
          <Form.Item label="公证书用途" style={formItemStyle}>
            {getFieldDecorator('use', { initialValue: getCompanyInfo.use != undefined ? getCompanyInfo.use : '' })(<Input />)}
          </Form.Item>
          <Form.Item label="公证书份数">
            {getFieldDecorator('number', { initialValue: getCompanyInfo.number != undefined ? getCompanyInfo.number : '' })(<InputNumber min={1} max={10} />)}
          </Form.Item>
        </Form>
        <Form.Item label="证明材料">
          {getFieldDecorator('proveData', {
            initialValue: proveDataArry || [],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={4}>
                  <Checkbox value="身份证">身份证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="户口簿">户口簿</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="房产证">房产证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="房屋查档信息">房屋查档信息</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="国土证">国土证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="购房合同">购房合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="商品房信息摘要">商品房信息摘要</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="民事协议文本">民事协议文本</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="结婚证">结婚证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="离婚证">离婚证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="法院判决或调解">法院判决或调解</Checkbox>
                </Col>

                <Col span={4}>
                  <Checkbox value="婚姻登记记录证明">婚姻登记记录证明</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="营业执照">营业执照</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="组织机构代码证">组织机构代码证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="股东会决议或董事会决议">股东会决议或董事会决议</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="法定代表人身份证">法定代表人身份证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="委托书">委托书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="驾驶证">驾驶证</Checkbox>
                </Col>

                <Col span={4}>
                  <Checkbox value="车辆登记证">车辆登记证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="车辆行驶证">车辆行驶证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="商标注册证明">商标注册证明</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="收据">收据</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="台湾或香港居民来往大陆通行证">台湾或香港居民来往大陆通行证</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="商品房买卖合同">商品房买卖合同</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="商品房买卖合同摘要">商品房买卖合同摘要</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="公证书">公证书</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="护照">护照</Checkbox>
                </Col>
                <Col span={4}>
                  <Checkbox value="公司章程">公司章程</Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        <Form layout={"inline"} >
          <Form.Item label="申请公证的文书">
            {getFieldDecorator('theBook', { initialValue: proveData['申请公证的文书'] ? proveData['申请公证的文书'] : '' })(<Input />)}
          </Form.Item>
          <Form.Item label="提供的其他相关证明材料1">
            {getFieldDecorator('otherProve0', { initialValue: proveDataOtherValue1||'' })(<Input />)}
          </Form.Item>
          <Form.Item label="提供的其他相关证明材料2" >
            {getFieldDecorator('otherProve1', {
              initialValue:proveDataOtherValue2||''})(<Input />)}
          </Form.Item>
        </Form>
        <Form layout={"inline"}>
          <div style={{ overflow: 'hidden' }}>
            <Form.Item label="申请日期" style={{ float: 'right', marginLeft: 20 }}>
              {getFieldDecorator('applyTime', { initialValue: showContent.applyTime || '' })(<Input />)}
            </Form.Item>
            <Form.Item label="申请人/代理人签名（盖章）" style={{ float: 'right' }}>
              {getFieldDecorator('autograph', { initialValue: showContent.autograph || '' })(<Input />)}
            </Form.Item>
            <Form.Item label="联系电话" style={{ float: 'right' }}>
              {getFieldDecorator('content', { initialValue: showContent.mobile || '' })(<Input />)}
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
  )
}
function unNull(value, replace = '--') {
  return value ? value : replace;
}
company.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(company)
