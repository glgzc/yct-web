import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import List from './List'
import Lightbox from 'react-images'
import Filter from './Filter'
import Modal from './detailsModal'
import Details from './Details'
import ClientAndMattersName from './clientAndMattersName'
const Acceptance = ({ location, dispatch, acceptance, loading }) => {
  const {
    getTagList, memoList, memoText, supplementState, showEditWords, personIndex, agentIndex, keywords,
    currentDetailInfo,translation,currency,
    visibleDetailsModal,
    detailsInfo,
    list,
    pagination,
    currentItem,
    visibleDetails,
    currentInfo,
    modalVisible,
    modalType,
    isMotion,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    currentApiItem,
    currentMethod,
    showClientAndMattersNameVisible,
    showClient,
    clientList,
    paginationClientList,
    selectedRowKeysClient,
    orgListData,
    paginationMatter,
    showFinalMatter,
    selectedRowKeysORG,
    clientRowKeys,
    ORGRows,
    showAddView,
    PartyClientKeys, PartyClientList,
    PartyORGKeys, PartyORGList,
    matterOtherData,
    partyList, PartyClientKeysCancle, PartyORGKeysCancle, name, number, matter, matterIndex, vedioList, urrentDetailedIMG, showTheCurrentDetailedIMG, fileList, isEditMatter, isEditBatch,
    showCascader, matterId, infoId, currentFile, getWordInfo, showEditForeign, getForeignInfo, getCompanyInfo, showEditCompany,
  } = acceptance
  const { pageSize } = pagination
  const modalProps = {
    currentApiItem,
    modalType,
    currentMethod: currentMethod === '' ? currentItem.method : currentMethod,
    item: modalType === 'update' ? currentItem : {},
    visible: modalVisible,
    modelVisibleGroup,
    interfaceList,
    visibleCreateApi,
    maskClosable: false,
    confirmLoading: loading.effects['acceptance/update'],
    title: `${modalType === 'create' ? (visibleCreateApi ? '添加服务接口' : '服务接口') : (modalType === 'update' ? '编辑服务接口----' + currentItem.interfaceName : (modalType === 'updateApi' ? '编辑服务接口' : '添加价格组'))}`,
    wrapClassName: 'vertical-center-modal',
    onOk(data) {
      dispatch({
        type: `acceptance/${modalType}`,
        payload: data,
      })
    },
    onCancel() {
      dispatch({
        type: 'acceptance/hideModal',
      })
    },
    createOk() {
      dispatch({
        type: 'acceptance/showCreateApi',
        payload: {
          currentApiItem: {},
          modalType: 'create'
        }
      })
    },
    createCancel() {
      dispatch({
        type: 'acceptance/hideCreateApi',
        payload: {
          modalType: 'create'
        }
      })
    },
    onDeleteApi(data) {
      dispatch({
        type: 'acceptance/deleteInterface',
        payload: data
      })
    },
    onEdit(data) {
      dispatch({
        type: 'acceptance/showUpdateApi',
        payload: {
          currentApiItem: data,
          modalType: 'updateApi'
        }
      })
    },
    onEditOk(data) {
      dispatch({
        type: 'acceptance/updateInterface',
        payload: {
          modalType: 'create',
          data
        }
      })
    },
    onChangeMethod(value) {
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          currentMethod: value
        }
      })
    }
  }
  const listProps = {
    list,
    loading: loading.effects['acceptance/query'],
    pagination,
    location,
    isMotion,
    dispatch,
    vedioList,
    onChange(page) {
      dispatch({
        type: 'acceptance/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
          keywords: keywords,
        }
      })
    },
    onRowClick(record) {
      dispatch({
        type: 'acceptance/info',
        payload: {
          currentItem: record,
          visibleDetails: true
        }
      })
    },
    getEditData(record) {
      dispatch({
        type: 'acceptance/EditData',
        payload: {
          currentItem: record,
        }
      })
    }
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'acceptance/queryKeyWords',
        payload: {
          keywords: value.keywords,
        },
      })
    },
    changeInput(value) {
      // dispatch({
      //   type: 'acceptance/currentMethod',
      //   payload: {
      //     keywords: value.keywords,
      //   },
      // })
    },
    onSearch(fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/acceptance',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/acceptance',
      }))
    },
    switchIsMotion() {
      dispatch({ type: 'acceptance/switchIsMotion' })
    },
    addToCollect() {
      //生成一个批次id
      const id = new Date().getTime()
      sessionStorage.setItem("batchId", id)
      dispatch({ type: 'acceptance/addNewBatch' })
      // dispatch({ type: 'acceptance/showClientAndMattersName' })
    }
  }

  const detailsProps = {
    showEditForeign, getForeignInfo, getCompanyInfo, showEditCompany, agentIndex,
    getWordInfo,
    personIndex,
    showEditWords,
    currentDetailInfo,
    currentItem,
    currentInfo,
    vedioList,
    onHideDetails() {
      dispatch({
        type: 'acceptance/hideDetails',
        payload: {
          visibleDetails: false
        }
      })
    },
    showDetailModal(record) {
      dispatch({
        type: 'acceptance/clientInfo',
        payload: {
          ...record,
          visibleDetailsModal: true
        }
      })
    },
    hideDetailModal() {
      dispatch({
        type: 'acceptance/hideDetailsModal',
        payload: {
          visibleDetailsModal: false
        }
      })
    },
    editWord() {
      dispatch({
        type: 'acceptance/editWord',
        payload: {
          matterId: currentItem.id,
        }
      })
    },
    downPeoples() {
      dispatch({
        type: 'acceptance/downPeopleModels',
        payload: {
          matterId: currentItem.id,
        }
      })
    },
    editForeigns() {
      dispatch({
        type: 'acceptance/editForeign',
        payload: {
          matterId: currentItem.id,
        }
      })
    },
    editCompanys() {
      dispatch({
        type: 'acceptance/editCompany',
        payload: {
          matterId: currentItem.id,
        }
      })
    },
    clickOnOk(data, matterId, type) {
      dispatch({
        type: 'acceptance/saveWordTable',
        payload: {
          showEditWords: false,
          data: data,
          matterId: matterId,
          type: type
        }
      })
    },
    clickOnCancle() {
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          showEditWords: false,
          showEditForeign: false,
          showEditCompany: false,
        }
      })
    },
    addPersons() {
      let currentIndex = personIndex + 1
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          personIndex: currentIndex,
        }
      })
    },
    addAgentPersons() {
      let currentIndex = agentIndex + 1
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          agentIndex: currentIndex,
        }
      })
    }
  }
  const detailsModalProps = {
    currentDetailInfo,
    visibleDetailsModal,
    urrentDetailedIMG, showTheCurrentDetailedIMG,
    detailsInfo,
    vedioList,
    title: <div style={{ textAlign: "center" }}>当事人信息详情</div>,
    okText: "收起",
    footer: null,
    onOk() {
      dispatch({
        type: "acceptance/hideDetailsModal"
      })
    },
    changeImg(arr) {
      dispatch({
        type: 'acceptance/currentMethod',
        payload: {
          urrentDetailedIMG: arr,
          showTheCurrentDetailedIMG: true,
        }
      })
    }
  }
  const ClientAndMattersNameProps = {dispatch,currency,
    partyList, PartyClientKeysCancle, PartyORGKeysCancle, number, matter, matterIndex, name, getTagList, vedioList, showCascader, matterId, fileList, isEditBatch,
    isEditMatter, memoList, memoText, infoId,translation,currency,
    PartyORGKeys, PartyORGList,
    PartyClientKeys, PartyClientList,
    showAddView,
    showClientAndMattersNameVisible,
    showClient,
    clientList,
    paginationClientList,
    selectedRowKeysClient,
    clientRowKeys,
    ORGRows,
    paginationMatter,
    orgListData,
    showFinalMatter,
    selectedRowKeysORG,
    supplementState,
    matterOtherData,
    changeSupplementStates(SupplementState) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          supplementState: SupplementState,
        }
      })
    },
    goToclientAndMatterView() {
      dispatch({
        type: "acceptance/goToclientAndMatterViewVisible",
        payload: {
          // partyList: [],
          // PartyORGList: [],
          // PartyClientList: [],
          // PartyORGKeys: [],
          // PartyClientKeys: [],
          // PartyClientKeysCancle: [],
          // PartyORGKeysCancle: [],
          // selectedRowKeysClient: [],
          // clientRowKeys: [],
          // selectedRowKeysORG: [],
          // ORGRows: [],
        }
      })
    },
    goToNewMatterView() {
      const id = new Date().getTime()
      sessionStorage.setItem("matterId", id)
      sessionStorage.setItem("editIndex", -1)
      dispatch({
        type: "acceptance/showNewMatterViewVisible",
        payload: {
          matterOtherData
        },
      })
    },
    goBackToIndex() {
      dispatch({
        type: "acceptance/hideshowClientAndMattersNameVisible",
        payload: {
          partyList: [],
          PartyORGList: [],
          PartyClientList: [],
          PartyORGKeys: [],
          PartyClientKeys: [],
          PartyClientKeysCancle: [],
          PartyORGKeysCancle: [],
          selectedRowKeysClient: [],
          clientRowKeys: [],
          selectedRowKeysORG: [],
          ORGRows: [],
        }
      })
    },
    choiceClientFuction() {
      dispatch({
        type: "acceptance/choiceClientReducers",
      })
    },
    choiceMatterFuction() {
      dispatch({
        type: "acceptance/choiceMatter",
      })
    },
    onChangeClientLsit(page) {
      dispatch({
        type: "acceptance/onChangeClients",
        payload: {
          page: page.current,
          pageSize: page.pageSize,
        }
      })
    },
    onChangeMatterPages(page) {
      dispatch({
        type: "acceptance/onChangeMatters",
        payload: {
          page: page.current,
          pageSize: page.pageSize,
        }
      })
    },
    getClientCurrentLists(selectedRowKeysClients, clientRowKeys) {
      // let newClientRowKeys = selectedRowKeysClients.concat(selectedRowKeysClient)
      // let result = []
      // for (let i = 0, len = newClientRowKeys.length; i < len; i++) {
      //   if (result.indexOf(newClientRowKeys[i]) === -1) result.push(newClientRowKeys[i]);
      // }
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          selectedRowKeysClient: selectedRowKeysClients,
          clientRowKeys: clientRowKeys,
        }
      })
    },
    getORGCurrentLists(ORGCurrentLists, ORGRows) {
      // let newORGRowKeys = ORGCurrentLists.concat(selectedRowKeysORG)
      // let result = []
      // for (let i = 0, len = newORGRowKeys.length; i < len; i++) {
      //   if (result.indexOf(newORGRowKeys[i]) === -1) result.push(newORGRowKeys[i]);
      // }
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          selectedRowKeysORG: ORGCurrentLists,
          ORGRows: ORGRows
        }
      })
    },
    toShowAddViews(visible) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          showAddView: visible,
          PartyClientKeys: PartyClientKeysCancle,
          PartyORGKeys: PartyORGKeysCancle
        }
      })
    },
    getPartyClientArrys(PartyClientKeys, PartyClientList) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          PartyClientKeys: PartyClientKeys,
          PartyClientList: PartyClientList,
        }
      })
    },
    getPartyORGArrys(PartyORGKeys, PartyORGList) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          PartyORGKeys: PartyORGKeys,
          PartyORGList: PartyORGList,
        }
      })
    },
    reSetAddParTyData() {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          selectedRowKeysClient: [],
          clientRowKeys: [],
          selectedRowKeysORG: [],
          ORGRows: [],
        }
      })
    },
    confirmToClientAndmatterNames(partyList, PartyClientKeys, PartyORGKeys) {
      // 储存当前rowKeys
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          partyList: partyList,
          PartyClientKeysCancle: PartyClientKeys,
          PartyORGKeysCancle: PartyORGKeys,
        }
      })
    },
    choiceMatterToclientAndMatterViews(name) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          name: name,
        }
      })
    },
    getCopiesToindex(number) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          number: number
        }
      })
    },
    saveMatterDataToIndex(matter, newIndex) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          matter: matter,
          matterIndex: newIndex,
          showFinalMatter: false,
          partyList: [],
          PartyORGList: [],
          PartyClientList: [],
          PartyORGKeys: [],
          PartyClientKeys: [],
          PartyClientKeysCancle: [],
          PartyORGKeysCancle: [],
          clientRowKeys: [],
          ORGRows: [],
          currency:'',
          translation: '',
        }
      })
    },
    reserMatter() {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          partyList: [],
          PartyORGList: [],
          PartyClientList: [],
          PartyORGKeys: [],
          PartyClientKeys: [],
          PartyClientKeysCancle: [],
          PartyORGKeysCancle: [],
        }
      })
    },
    saveDataToIndex(payload) {
      dispatch({
        type: "acceptance/addAndEditBatch",
        payload: payload
      })
    },
    heelPartyLists(partyList) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          partyList: partyList
        }
      })
    },
    getVedioLists(currentArry) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          vedioList: currentArry
        }
      })
    },
    deleteMatterItemToClientAndMatters(matter) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          matter: matter,
          partyList: []
        }
      })
    },
    editMatterToClientAndMatters(index, row) {
      dispatch({
        type: "acceptance/editMatterModels",
        payload: {
          index,
          row,
          selectedRowKeysClient,
          selectedRowKeysORG,
          matterOtherData
        }
      })
    },
    getFileLists(files) {
      dispatch({
        type: "acceptance/currentMethod",
        payload: {
          fileList: files
        }
      })
    }
  }
  return (
    <div style={{ overflow: 'auto', width: '100%' }}>
      {/* <Lightbox
        onClickImage={() => { console.log('点击') }}
        showThumbnails={true}
        backdropClosesModal={true}
        currentImage={urrentDetailedIMG}
        images={urrentDetailedIMG}
        isOpen={showTheCurrentDetailedIMG}
        onClose={() => { console.log('关闭') }}
      /> */}
      {showClientAndMattersNameVisible ?
        <ClientAndMattersName {...ClientAndMattersNameProps} /> :
        <div className='content'>
          <div className='content-left' style={{ overflow: 'auto', width: '100%' }}>
            <Filter {...filterProps} />
            <List {...listProps} />
          </div>
          {
            visibleDetails ?
              <div className='content-right' style={{ overflow: 'auto', width: '100%' }}>
                <Details {...detailsProps} />
              </div>
              : null
          }
          {visibleDetailsModal ?
            <Modal {...detailsModalProps} /> : null
          }
        </div>}
    </div>
  )
}

Acceptance.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ acceptance, loading }) => ({ acceptance, loading }))(Acceptance)
