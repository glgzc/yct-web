import React from 'react'
import { Table, Modal, Popconfirm, Form, Badge, Menu, Dropdown, Icon, } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'

import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'
import '../../utils/index'

const confirm = Modal.confirm

const List = ({ list, onRowClick,getEditData, ...tableProps }) => {
  const handleMenuClick = (record, e) => {
    getEditData(record)
  };
  const expandedRowRender = (record) => {
    const columns = [
      { title: '事项名称', dataIndex: 'mattersName', key: 'mattersName' },
      {
        title: '顺序', dataIndex: 'currency', key: 'currency', render: (value, row, index) => {
          return (
            <div>
              {index ? index + 1 : 1}
            </div>
          )
        }
      },
      { title: '当事人', dataIndex: 'partyStr', key: 'partyStr' },
      {
        title: '份数', dataIndex: 'number', key: 'number', render: (value, row, index) => {
          return (
            <div>
              {value ? parseInt(value): 0}
            </div>
          )
        }
      },
    ];
    return (
      <Table
        columns={columns}
        className={classnames({ [styles.table]: true })}
        dataSource={record.mattersList}
        pagination={false}
        rowKey={row => row.id}
        onRowClick={onRowClick.bind(this)}
      />
    );
  };

  const columns = [
    {
      title: '批号',
      dataIndex: 'batchNumber',
      key: 'batchNumber',
      render: (text) => {
        return text.slice(0, 10) + "...";
      }
    },
    {
      title: '公证员',
      dataIndex: 'notary',
      key: 'notary'
    }, {
      title: '协办人',
      dataIndex: 'coOrganizer',
      key: 'coOrganizer',
      render: (value) => {
        return value!='null'? value :'';
      }
    },
    {
      title: '采集人',
      dataIndex: 'userName',
      key: 'userName',
    },
    {
      title: '提交时间',
      dataIndex: 'collectionTime',
      key: 'collectionTime'
    },
    {
      title: '公证书编号',
      dataIndex: 'certificateNumber',
      key: 'certificateNumber',
      render: (value) => {
        return value ? value : '--';
      }
    },
    {
      title: '审批',
      dataIndex: 'approvalLevel',
      key: 'approvalLevel',
      render: (value) => {
        if (value == null || value == undefined || value == '') {
          return <p>暂无数据</p>
        } else if (value == 1) {
          return <p>部门审批出证</p>
        } else if (value == 2) {
          return <p>审签室审批出证</p>
        } else if (value == 3) {
          return <p>主任审批出证</p>
        }
      }
    },
    {
      title: '批次状态',
      dataIndex: 'state',
      key: 'state',
      render: (value) => {
        if (value == null || value == undefined || value == '') {
          return <p>暂无数据</p>
        } else if (value == 1) {
          return <p>待拉取</p>
        } else if (value == 2) {
          return <p>办证系统已受理</p>
        } else if (value == 3) {
          return <p>已获取审批数据</p>
        }
        else if (value == 4) {
          return <p>已推送钉钉</p>
        }
        else if (value == 5) {
          return <p>审批已通过</p>
        }
        else if (value == 6) {
          return <p>已出号</p>
        }
        else if (value == 7) {
          return <p>已制证建档</p>
        }
        else if (value == 8) {
          return <p>已归档</p>
        }
      }
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (
          <div style={{overflow:'hidden'}}>
            <a onClick={e => handleMenuClick(record, e)} style={{display:'inline-block'}}>编辑</a>
          </div>
        )
      },
    },
  ]
  for (let i = 0; i < list.length; ++i) {
    list[i].key = i
  }
  return (
    <div style={{ marginTop: '25px' }}>
      <Table
        dataSource={list}
        // className={classnames({ [styles.table]: true })}
        className="components-table-demo-nested"
        bordered
        columns={columns}
        simple
        pagination={tableProps.pagination}
        onChange={tableProps.onChange}
        expandedRowRender={expandedRowRender}
        // expandRowByClick={true}
      />
    </div>
  )
}

export default List
