import React from 'react'
import PropTypes from 'prop-types'
import { Form, Tree, Modal, Card, Button, Input, Checkbox, Row, Col, InputNumber, Radio } from 'antd'
// import styles from './ForeignNationals.less'
import InputArry from './InputArry'
import { config } from 'utils';
import { IEVersion } from 'utils/util';
const { fileSource } = config;


const ForeignNationals = ({
  onOk,
  addPerson,
  getForeignInfo,
  personIndex,
  form,
  ...modalPropsForeignNationals
}) => {
  const { validateFields, getFieldDecorator, getFieldsValue, resetFields } = form;
  const modalPropsPeoples = {
    ...modalPropsForeignNationals,
    onOk: click,
    // onCancel: visibleCreateApi ? createApiCancel : onCancel
  }
  console.log(getForeignInfo, 'getForeignInfo')
  let matterArry = []
  let matter = {}
  if (getForeignInfo.matter != undefined) {
    matter = JSON.parse(getForeignInfo.matter)
    for (const key in matter) {
      matterArry.push(key)
    }
  }
  let showPeeres = []
  let showAgent = []
  let showContent ={}
  if(getForeignInfo.peeres){
     showPeeres =JSON.parse(getForeignInfo.peeres) 
  }
  if(getForeignInfo.agent){
    showAgent = JSON.parse(getForeignInfo.agent)
  }
  if(getForeignInfo.content){
    showContent = JSON.parse(getForeignInfo.content)
  } 
  // const documentModalOpts = {
  //   ...modalProps,
  //   onOk: visibleCreateApi ? handleOk : createApiOK,
  //   onCancel: visibleCreateApi ? createApiCancel : onCancel
  // }
  const click = () => {
    const data = getFieldsValue()
    console.log(data, 'data')
    // 获取当事人
    let userInfo = {}
    userInfo.address = data.address
    userInfo.sex = data.sex
    userInfo.name = data.name
    userInfo.mobile = data.mobile
    userInfo.birth = data.birth
    userInfo.card = data.card
    userInfo.beforeName = data.beforeName
    userInfo.englishName = data.englishName
    userInfo.workUnit = data.workUnit

    // 公证事项 matter
    let currentMatter = {}
    for (let index = 0; index < data.matter.length; index++) {
      if (data.matter[index] == '出生公证') {
        currentMatter[data.matter[index]] = data.birthAddress
      } else {
        currentMatter[data.matter[index]] = data.matter[index]
      }
    }
    // 同行人
    let peeresArry = []
    let currentPeeres1 = {}
    let currentPeeres2 = {}
    currentPeeres1.appellation = data.appellation1
    currentPeeres1.name = data.colleagueName1
    currentPeeres1.birth = data.colleagueBirth1
    currentPeeres1.card = data.colleagueCard1
    currentPeeres1.address = data.colleagueAddress1
    peeresArry.push((currentPeeres1))
    currentPeeres2.appellation = data.appellation2
    currentPeeres2.name = data.colleagueName2
    currentPeeres2.birth = data.colleagueBirth2
    currentPeeres2.card = data.colleagueCard2
    currentPeeres2.address = data.colleagueAddress2
    peeresArry.push((currentPeeres2))
    // 代理人
    let agentArry = []
    let currentAgent = {}
    currentAgent.name = data.agentName
    currentAgent.sex = data.agentSex
    currentAgent.address = data.agentAddress
    currentAgent.mobile = data.agentMobile
    currentAgent.autograph = data.agentAutograph
    agentArry.push((currentAgent))
    // content 
    let content = {}
    content.applyTime = data.applyTime
    content.autograph = data.autograph
    let currentContent = {}
    currentContent.applyTime = data.applyTime
    currentContent.autograph = data.autograph
    currentContent.content = data.content
    // 填装数据
    let currentData = {}
    currentData.title = data.title
    currentData.userInfo = JSON.stringify(userInfo)
    currentData.matter = JSON.stringify(currentMatter)
    currentData.useAdress = data.useAdress
    currentData.number = data.number
    currentData.isTranslation = data.isTranslation
    currentData.languages = data.languages
    currentData.isWitness = data.isWitness
    currentData.peeres = JSON.stringify(peeresArry)
    currentData.agent = JSON.stringify(agentArry)
    currentData.content = JSON.stringify(currentContent)
    currentData.matterId = getForeignInfo.matterId
    let newData = JSON.stringify(currentData)
    console.log(newData,'newData')
    onOk(newData, getForeignInfo.matterId)
  }
  const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }
  const formItemStyle = {
    marginRight: 20, width: 300
  }
  const showPeopleFormItem = () => {
    let userInfo = {}
    if (getForeignInfo.userInfo != undefined) {
      userInfo = JSON.parse(getForeignInfo.userInfo)
    }
    let formItemArry = []
    for (let index = 0; index < 1; index++) {
      formItemArry.push(
        <div>
          <Form layout={"inline"} >
            <Form.Item label="姓名" style={formItemStyle}>
              {getFieldDecorator('name', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.name : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="曾用名" style={formItemStyle}>
              {getFieldDecorator('beforeName', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.beforeName : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="外文译名" style={formItemStyle}>
              {getFieldDecorator('englishName', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.englishName : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="出生日期" style={formItemStyle}>
              {getFieldDecorator('birth', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.birth : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="性别" style={formItemStyle}>
              {getFieldDecorator('sex', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.sex : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="现住址" style={formItemStyle}>
              {getFieldDecorator('address', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.address : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="身份证件号" style={formItemStyle}>
              {getFieldDecorator('card', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.card : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="电话" style={formItemStyle}>
              {getFieldDecorator('mobile', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.mobile : '' })(<Input />)}
            </Form.Item>
            <Form.Item label="工作单位" style={formItemStyle}>
              {getFieldDecorator('workUnit', { initialValue: getForeignInfo.userInfo != undefined ? userInfo.workUnit : '' })(<Input />)}
            </Form.Item>
          </Form>
        </div>
      )
    }
    return formItemArry
  }
  // console.log(proveDataArry, 'proveDataArry')
  return (
    // <div className="text">
    <Modal  {...modalPropsPeoples} onOk={() => click()}>
      <div>
        <Form layout={"inline"} style={{ textAlign: 'center' }}>
          <Form.Item label="公证申请表个人">
            {getFieldDecorator('title', { initialValue: getForeignInfo.title != undefined ? getForeignInfo.title : '' })(<Input />)}
          </Form.Item>
        </Form>
        {showPeopleFormItem()}
        {/* <h3 style={{ marginTop: 8, marginBottom: 8 }}>根据情况在下列“□ ”处打“√”标记，未列明的请在“其他”中填写</h3> */}
        <Form layout={"inline"} >
          <Form.Item label="申请公证内容">
            {getFieldDecorator('matter', {
              initialValue: matterArry || [],
            })(
              <Checkbox.Group style={{ width: '100%' }}>
                <Row>
                  <Col span={4}>
                    <Checkbox value="未受刑事处分公证">未受刑事处分公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="学历公证">学历公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="成绩单公证">成绩单公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="亲属关系公证">亲属关系公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="工作经历公证">工作经历公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="资信证明公证">资信证明公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="证明材料公证">证明材料公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="死亡公证">死亡公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="户籍与住所公证">户籍与住所公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="国籍公证">国籍公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="在职/在读公证">在职/在读公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="其他">其他</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="学位公证">学位公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="出生公证">出生公证</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="证件公证">证件公证（护照、驾驶证、健康证等）</Checkbox>
                  </Col>
                  <Col span={4}>
                    <Checkbox value="婚姻状况公证">婚姻状况公证（未婚、结婚、离婚、未再婚）</Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>,
            )}
          </Form.Item>
          <Form layout={"inline"} >
            <Form.Item label="出生公证(出生地: &nbsp;&nbsp; 父:  &nbsp;&nbsp;母: &nbsp;&nbsp; )" >
              {getFieldDecorator('birthAddress', { initialValue:matter["出生公证"] ||'(出生地: 父:  母:  )'})(<Input />)}
            </Form.Item>
          </Form>
        </Form>
        <Form layout={"inline"} >
          <Form.Item label="公证目的及使用地" style={formItemStyle}>
            {getFieldDecorator('useAdress', { initialValue: getForeignInfo.useAdress ||'' })(<Input />)}
          </Form.Item>
          <Form.Item label="所需公证份数">
            {getFieldDecorator('number', { initialValue: getForeignInfo.number||'' })(<InputNumber min={1} max={10} />)}
          </Form.Item>
          <Form.Item label="语种">
            {getFieldDecorator('languages', { initialValue:getForeignInfo.languages|| '' })(<Input  />)}
          </Form.Item>
        </Form>
        <Form layout={"inline"} >
          <Form.Item label="是否需要译文" style={formItemStyle}>
            {getFieldDecorator('isTranslation',{ initialValue:getForeignInfo.isTranslation|| 0 })(
              <Radio.Group>
                <Radio value={1}>是</Radio>
                <Radio value={0}>否</Radio>
              </Radio.Group>,
            )}
          </Form.Item>
          <Form.Item label="是否需要认证" style={formItemStyle}>
            {getFieldDecorator('isWitness',{ initialValue:getForeignInfo.isWitness|| 0 })(
              <Radio.Group>
                <Radio value={1}>是</Radio>
                <Radio value={0}>否</Radio>
              </Radio.Group>,
            )}
          </Form.Item>
        </Form>
        <h3>同行人1:</h3>
        <Form layout={"inline"} >
          <Form.Item label="称谓" style={formItemStyle}>
            {getFieldDecorator('appellation1',{initialValue:getForeignInfo.peeres? showPeeres[0].appellation:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="姓名" style={formItemStyle}>
            {getFieldDecorator('colleagueName1',{initialValue:getForeignInfo.peeres? showPeeres[0].name:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="出生日期" style={formItemStyle}>
            {getFieldDecorator('colleagueBirth1',{initialValue:getForeignInfo.peeres? showPeeres[0].birth:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="证件号" style={formItemStyle}>
            {getFieldDecorator('colleagueCard1',{initialValue:getForeignInfo.peeres?showPeeres[0].card:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="现地址" style={formItemStyle}>
            {getFieldDecorator('colleagueAddress1',{initialValue:getForeignInfo.peeres?showPeeres[0].address:'' })(<Input />)}
          </Form.Item>
        </Form>
        <h3>同行人2:</h3>
        <Form layout={"inline"} >
          <Form.Item label="称谓" style={formItemStyle}>
            {getFieldDecorator('appellation2', { initialValue:getForeignInfo.peeres?showPeeres[1].appellation:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="姓名" style={formItemStyle}>
            {getFieldDecorator('colleagueName2', { initialValue:getForeignInfo.peeres?showPeeres[1].name:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="出生日期" style={formItemStyle}>
            {getFieldDecorator('colleagueBirth2', { initialValue:getForeignInfo.peeres?showPeeres[1].birth:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="证件号" style={formItemStyle}>
            {getFieldDecorator('colleagueCard2', { initialValue: getForeignInfo.peeres?showPeeres[1].card:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="现地址" style={formItemStyle}>
            {getFieldDecorator('colleagueAddress2', { initialValue:getForeignInfo.peeress?howPeeres[1].address:'' })(<Input />)}
          </Form.Item>
        </Form>
        <h3>代申请人:</h3>
        <Form layout={"inline"} >
          <Form.Item label="姓名" style={formItemStyle}>
            {getFieldDecorator('agentName', { initialValue:JSON.stringify(showAgent)!='[]'?showAgent[0].name:'' })(<Input />)}
          </Form.Item>
          <Form.Item label="性别" style={formItemStyle}>
            {getFieldDecorator('agentSex', { initialValue:JSON.stringify(showAgent)!='[]'?showAgent[0].sex: '' })(<Input />)}
          </Form.Item>
          <Form.Item label="住址" style={formItemStyle}>
            {getFieldDecorator('agentAddress', { initialValue:JSON.stringify(showAgent)!='[]'?showAgent[0].address: '' })(<Input />)}
          </Form.Item>
          <Form.Item label="电话" style={formItemStyle}>
            {getFieldDecorator('agentMobile', { initialValue:JSON.stringify(showAgent)!='[]'?showAgent[0].mobile:  '' })(<Input />)}
          </Form.Item>
          <Form.Item label="代申请人签名" style={formItemStyle}>
            {getFieldDecorator('agentAutograph', { initialValue: JSON.stringify(showAgent)!='[]'?showAgent[0].autograph: '' })(<Input />)}
          </Form.Item>
        </Form>
        <h3>申请人（代申请人）在此承诺，上述所填内容、提供材料以及谈话笔录内容均真实、有效，如有隐瞒或不实，我愿承担由此引起的一切法律责任。</h3>
        <Form layout={"inline"} >
          <div style={{ overflow: 'hidden' }}>
            <Form.Item label="申请日期" style={{ float: 'right', marginLeft: 20 }}>
              {getFieldDecorator('applyTime', { initialValue: getForeignInfo.content?showContent.applyTime:'' })(<Input />)}
            </Form.Item>
            <Form.Item label="申请人（代申请人）" style={{ float: 'right' }}>
              {getFieldDecorator('autograph', { initialValue:getForeignInfo.content?showContent.autograph :'' })(<Input />)}
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
    // </div>


  )
}
function unNull(value, replace = '--') {
  return value ? value : replace;
}
ForeignNationals.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(ForeignNationals)
