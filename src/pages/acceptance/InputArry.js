import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Button, Icon, Input } from 'antd'
 
// import './index.scss'
 
class InputArray extends PureComponent {
    constructor(props) {
        super(props)
    }
 
    handleChange = index => {
        const { value, onChange } = this.props
        const newValue = [...value]
 
        newValue[index] = target.value
 
        onChange(newValue)
    }
 
    handleDelete = e => {
        const target = e.currentTarget
        const index = target.parentNode.parentNode.firstChild.dataset.index
        const { value, onChange } = this.props
        const newValue = [...value]
 
        newValue.splice(Number(index), 1)
 
        onChange(newValue)
    }
 
    handleAdd = () => {
        const { value, onChange } = this.props
        const newValue = [...value, '']
 
        onChange(newValue)
    }
 
    render() {
        const { value, ...others } = this.props
 
        const closeBtn = <Icon type="close-circle" onClick={this.handleDelete} />
 
        return (
            <div className="input-array-component">
                {value.map((v, i) => {
                    return (
                        <div key={i}>
                            <Input
                                {...others}
                                value={v}
                                suffix={closeBtn}
                                data-index={i}
                                onChange={() => this.handleChange(i)}
                            />
                        </div>
                    );
                })}
                <div>
                    <Button type="dashed" icon="plus" onClick={this.handleAdd}>添加</Button>
                </div>
            </div>
        );
    }
}
 
InputArray.defaultProps = {
    value: []
}
 
export default InputArray