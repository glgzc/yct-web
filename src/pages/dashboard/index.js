import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Row, Col, Card ,DatePicker,Table,Icon} from 'antd'
import { color } from 'utils'
import { NumberCard } from './components'
import styles from './index.less'
import ApiChart from './ApiChart'

const { MonthPicker, RangePicker } = DatePicker

const Dashboard = ({ location, dispatch, dashboard, loading }) => {
	const {  numbers,sumList,countList } = dashboard
	const numberCards = numbers.map((item, key) => (
		<Col key={key} lg={8} md={12}>
	    	<NumberCard {...item} />
	    </Col>
	))
	const onChange=(date, dateString)=>{
		const startAt=dateString[0]
		const endAt=dateString[1]
		dispatch({
	 	   type: 'dashboard/query',
        	payload: {
        		startAt,
        		endAt
        	},
	    })
	}

	const columns = [{
		title: '接口名',
		dataIndex: 'name',
	}, {
		title: '使用次数',
		dataIndex: 'count',
		render: text=>{
			return <span>{text}次</span>
		}
	}, {
		title: '使用费用',
		dataIndex: 'price',
		render: text=>{
			return <span>{text}元</span>
		}
	}];

	const data = [{
		key: '1',
		name: '智能人证比对',
		count: 99,
		price: 32
	}, {
		key: '2',
		name: '智能身份核验',
		count: 1,
		price: 32
	}, {
		key: '3',
		name: '身份证信息采集',
		count: 229,
		price: 32
	}, {
		key: '4',
		name: '营业执照信息采集',
		count: 0,
		price: 32
	}, {
		key: '5',
		name: '库存人照核验',
		count: 0,
		price: 0
	}, {
		key: '6',
		name: '驾驶证信息采集',
		count: 0,
		price: 0
	}, {
		key: '7',
		name: '银行卡信息采集',
		count: 0,
		price: 0
	}, {
		key: '8',
		name: '行驶证信息采集',
		count: 0,
		price: 0
	}, {
		key: '9',
		name: 'NFC',
		count: 0,
		price: 0
	}];

    return (
        <div className="content-inner" style={{backgroundColor:'transparent',padding:0}}>
    		<Row gutter={24}>{numberCards}</Row>
			<Card
			title={
				<p style={{fontSize:'16px',color: '#666'}}><Icon type="database" style={{marginRight:'5px'}} /><span>使用报表</span>&nbsp;&nbsp;<span style={{fontSize: '13px',color:'#9EACB5'}}>按时间区查看使用情况</span></p>
			}
			extra={<RangePicker size='large' />}>
				<Row gutter={24}>
					<Col span={12} style={{width:'50%'}}>
						<ApiChart />
					</Col>
					<Col span={12}>
						<Table columns={columns} dataSource={data} />
					</Col>
				</Row>
			</Card>
        </div>
    )
}
export default connect(({ dashboard, loading }) => ({ dashboard, loading }))(Dashboard)
