import React, {Component} from 'react';
import echarts from 'echarts';
import { Card, Icon } from 'antd';

class ApiChart extends Component{
    componentDidMount(props) {
        const apiChart = echarts.init(document.getElementById("apiChart"));
        apiChart.setOption({
            color: ['#3398DB'],
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    data : ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'直接访问',
                    type:'bar',
                    barWidth: '60%',
                    data:[10, 52, 200, 334, 390, 330, 220]
                }
            ]
        })

        apiChart.on('finished', function () {
            apiChart.resize();
        });

        window.onpageshow = function(){
            apiChart.resize();
        }
    }

    render(){
        return(
            <Card 
            bordered={false}
            title={
                <p style={{fontSize:'16px',color: '#3398DB'}}><Icon type="bar-chart" style={{marginRight:'5px'}} /><span>接口总体调用情况</span>&nbsp;&nbsp;<span style={{fontSize:'13px',color:'#9eacb4'}}>区间内调用情况分析</span></p>
            }
            style={{width:'100%'}}>
                <div id='apiChart' style={{width: '100%',height:'450px'}}></div>
            </Card>
        )
    }
}

export default ApiChart;
