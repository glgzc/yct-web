import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form,message} from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import { config } from 'utils'
import {isAdmin,IEVersion} from 'utils/util';
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'
import '../../utils/index'
const confirm = Modal.confirm
const { playFileSourceDev, api,fileSourceDev } = config
const { downVideoList} = api
const List = ({ onDeleteItem, onEditItem,editingKey, ...tableProps }) => {

  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
        window.open(playFileSourceDev+record.videoUrl)
    } else if (e.key === '2') {
      message.success("正在生成下载,请耐心等待......")
      const token = sessionStorage.getItem('token')
      window.open(playFileSourceDev+downVideoList+'?token='+token+'&urls='+record.videoUrl,'_self')
    }else if (e.key === '3') {
      confirm({
        title: '确定要删除该条视频吗?',
        onOk() {
          let data = [{ matterId: record.matterId, videoUrl: record.videoUrl }]
          onDeleteItem(
            JSON.stringify(data) 
          )
        },
      })
    }
  }

  const expandedRowRender = (record) => {
    const columns = [
      { title: '视频名称', dataIndex: 'videoName', key: 'videoName' },
      { title: '事项名称', dataIndex: 'matterName', key: 'matterName' },
      { title: '当事人', dataIndex: 'partyNames', key: 'partyNames' },
      {
        title: '操作',
        key: 'operation',
        width: 100,
        render: (text, record) => {
          return <DropOption onMenuClick={e => handleMenuClick(record, e)} menuOptions={isAdmin() ? [{ key: '1', name: '播放' }, { key: '2', name: '下载' }, { key: '3', name: '删除' }] :
          [{ key: '1', name: '播放' }, { key: '2', name: '下载' }]} />
        },
      },
    ];
    return (
      <Table
        columns={columns}
        className={classnames({ [styles.table]: true })}
        dataSource={record.videoList}
        pagination={false}
        rowKey={row => row.matterId}
      />
    );
  };
  const columns = [
    {
      title: '批次号',
      dataIndex: 'batchNumber',
      key: 'batchNumber'
    }, {
      title: '采集者',
      dataIndex: 'userName',
      key: 'userName'
    }, {
    }, {
      title: '创建时间',
      dataIndex: 'updateTime',
      key: 'updateTime',
      render: (text) => {
        return( text!=null?text:'')
      }
    }, 
  ]


  return (
    <div>
      <Table
        {...tableProps}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={record => record.localId}
        rowSelection={null}
        expandedRowRender={expandedRowRender}
      />
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
}

export default List
