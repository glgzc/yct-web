/*
 * @Description: role auth editor
 * @Author: empty
 * @LastEditors: Please set LastEditors
 * @Date: 2019-02-28 16:19:45
 * @LastEditTime: 2019-03-23 15:37:35
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Tree, Modal, Select, message } from 'antd';
const FormItem = Form.Item
const TreeNode = Tree.TreeNode

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}
const styles = {
  layout: {
    display: 'flex'
  },
  baseForm: {
    flex: '1'
  },
  authForm: {
    flex: '1'
  }
}
class modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentOrgId: null,
      orgMenuList: [],
      currentOrgMenuList: [],
    }
    this.handleOk = this.handleOk.bind(this);
    this.onCheck = this.onCheck.bind(this);
    this.loop = this.loop.bind(this);
    this.handleSelectOrg = this.handleSelectOrg.bind(this);
  }
  handleOk = () => {
    const { validateFields } = this.props.form;
    validateFields((errors, values) => {
      if (errors) {
        return
      }
      const data = {
        roleName: values.roleName,
        remark: values.remark,
        orgMenuList: this.props.orgMenuList
      }
      if (this.props.item.roleId) {
        data.roleId = this.props.item.roleId
      }
      this.props.onOk(data)
    })
  }

  onCheck = (checkedKeys, e) => {
    if (!this.state.currentOrgId) {
      message.warn('请先选择机构');
      return;
    }
    this.props.updateOrgMenuListByOrg({
      organizationId: this.state.currentOrgId,
      menuList: checkedKeys.map(key => Number(key))
    })
    this.setState({
      currentOrgMenuList: checkedKeys
    })
  }

  loop = data => data.map((item) => {
    if (item.children) {
      return (
        <TreeNode key={item.menuId} title={item.name} >
          {this.loop(item.children)}
        </TreeNode>
      )
    }
    return <TreeNode key={item.menuId} title={item.name} />
  })


  handleSelectOrg = (orgId) => {
    const selectedId = Number(orgId[0]);
    for (let item of this.props.orgMenuList) {
      if (item.organizationId === selectedId) {
        this.setState({
          currentOrgId: selectedId,
          currentOrgMenuList: item.menuList.map(menu => String(menu))
        });
        return;
      }
    }
    // 新数据制空
    this.setState({
      currentOrgId: selectedId,
      currentOrgMenuList: []
    })
  }

  render() {
    const { item, form, treeData, orgMenuList ,treeSelectData, ...modalProps } = this.props;
    const { getFieldDecorator } = form;
    const modalOpts = {
      ...modalProps,
      onOk: this.handleOk,
      width: 800
    }
    const orgList = orgMenuList
    let arr = []
    orgList.map((item)=>arr.push(item.organizationId))
    return (
      <Modal {...modalOpts}>
        <Form layout="horizontal" style={styles.layout}>
          <div style={styles.baseForm}>
            <FormItem label="角色名称" hasFeedback {...formItemLayout}>
              {getFieldDecorator('roleName', {
                initialValue: item.roleName,
                rules: [
                  {
                    required: true,
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem label="角色备注" hasFeedback {...formItemLayout}>
              {getFieldDecorator('remark', {
                initialValue: item.remark ? item.remark : '',
                rules: [
                  {
                    required: false,
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem label="选择机构" hasFeedback {...formItemLayout}>
              {getFieldDecorator('organizationId')(
                <Tree
                  onSelect={this.handleSelectOrg}
                  defaultExpandAll>
                  {treeSelectData.map(org =>
                    <TreeNode key={org.id} title={arr.indexOf(org.id) < 0 ? org.name : <span style={{ color: '#08c' }}>{org.name}</span>} />
                  )}
                </Tree>
              )}
            </FormItem>
          </div>
          <div style={styles.authForm}>
            <FormItem label="权限" hasFeedback {...formItemLayout}>
              {getFieldDecorator('menuIdList', {
                rules: [
                  {
                    required: false,
                  },
                ],
              })(
                <Tree
                  checkedKeys={this.state.currentOrgMenuList}
                  onCheck={this.onCheck}
                  defaultExpandAll
                  checkable
                >
                  {this.loop(treeData)}
                </Tree>
              )}
            </FormItem>
          </div>


        </Form>
        <div>

        </div>
      </Modal>
    )
  }
}

modal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(modal)
