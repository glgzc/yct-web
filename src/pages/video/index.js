import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import queryString from 'query-string'
import { Row, Col, Button, Popconfirm } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'

const Video = ({ location, dispatch, video, loading }) => {
  const { 
    list, 
    pagination, 
    currentItem,
    modalVisible, 
    modalType, 
    isMotion, 
    selectedRowKeys,
    treeData,
    treeCheckedKeys,
    treeSelectData,
    orgMenuList
  } = video;
  const { pageSize } = pagination

  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['video/update'],
    title: `${modalType === 'create' ? '添加角色' : '编辑角色信息'}`,
    wrapClassName: 'vertical-center-modal',
    treeData,
    treeCheckedKeys,
    treeSelectData,
    orgMenuList,
    onOk (data) {
      console.log(data);
      console.log(modalType)
      dispatch({
        type: `video/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'video/hideModal',
      })
    },
    onTreeCheck (checkedKeys) {
      dispatch({
        type: 'video/updateState',
        payload: {
          treeCheckedKeys: checkedKeys,
        },
      })
    },
    updateOrgMenuListByOrg(payload) {
      dispatch({
        type: 'video/updateOrgMenuListByOrg',
        payload
      })
    }
  }

  const listProps = {
    dataSource: list,
    loading: loading.effects['video/query'],
    pagination,
    location,
    isMotion,
    onChange(page) {
      const { query,search, pathname } = location

      dispatch(routerRedux.push({
        pathname,
        search: queryString.stringify({
          ...query,
          ...queryString.parse(search),
          page: page.current,
          pageSize: page.pageSize,
        }),
      }))
     
    },
    onDeleteItem (data) {
      dispatch({
        type: 'video/delete',
        payload: {
          data
        },
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'video/downLoad',
        payload: {
          urls: item,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'video/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange (value) {
      dispatch(routerRedux.push({
        pathname: location.pathname,
        query: {
          ...value,
          page: 1,
          pageSize,
        },
      }))
    },
    onSearch (fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/video',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/video',
      }))
    },
    onAdd () {
      dispatch({
        type: 'video/menuAuth',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion () {
      dispatch({ type: 'video/switchIsMotion' })
    },
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'video/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }

  return (
    <div className="content-inner">
      <Filter {...filterProps} />
        {
          selectedRowKeys.length > 0 &&
          <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
            <Col>
              {`Selected ${selectedRowKeys.length} items `}
              <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
                <Button type="primary" style={{ marginLeft: 8 }}>Remove</Button>
              </Popconfirm>
            </Col>
          </Row>
        }
        <List {...listProps} />
      {modalVisible && <Modal {...modalProps} />}
    </div>
  )
}

Video.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ video, loading }) => ({ video, loading }))(Video)
