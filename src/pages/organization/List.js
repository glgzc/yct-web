import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm

const List = ({ onDeleteItem, onEditItem,editingKey, ...tableProps }) => {
  const {dataSource} = tableProps
  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该机构吗?',
        onOk () {
          onDeleteItem(record.id)
        },
      })
    }
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id'
    }, {
      title: '机构名',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: '描述',
      dataIndex: 'describe',
      key: 'describe'
    }, {
      title: '创建时间',
      dataIndex: 'createdTime',
      key: 'createdTime',
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return <DropOption onMenuClick={e => handleMenuClick(record, e)} menuOptions={[{ key: '1', name: '编辑' }, { key: '2', name: '删除' }]} />
      },
    },
  ]


  return (
    <div style={{marginTop:'10px'}}>
      {
        dataSource && dataSource.length?
        <Table
        {...tableProps}
        className={classnames(styles.table)}
        bordered
        columns={columns}
        simple
        rowKey={record => record.id}
        rowSelection={false}
      /> : ''
      }
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
}

export default List
