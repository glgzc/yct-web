import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux, HashRouter } from 'dva/router'
import { connect } from 'dva'
import { Card, Popconfirm, Table, Tabs, Icon } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'
import Group from './Group'
import Balance from './Balance'
import Recharge from './Recharge'
import Api from './Api'
import { isAdmin } from 'utils/util';
const Organization = ({ location, dispatch, organization, loading, history }) => {
  const {
    list,
    pagination,
    currentItem,
    modalVisible,
    modalType,
    isMotion,
    selectedRowKeys,
    organizationList,
    parentOrganization,
    allList,
    visibleOperator,
    visibleApi,
    visibleBusiness,
    visibleGroup,
    visibleBalance,
    visibleRecharge,
    visible,
    currentOrganization,
    priceGroupList,
    rechargeList,
    apiList
  } = organization;
  const { pageSize } = pagination
  const modalProps = {
    organizationList,
    parentOrganization,
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['user/update'],
    title: `${modalType === 'create' ? '添加机构' : '编辑机构信息'}`,
    wrapClassName: 'vertical-center-modal',
    onOk(data) {
      dispatch({
        type: `organization/${modalType}`,
        payload: data,
      })
    }
  }

  const listProps = {
    dataSource: list,
    loading: loading.effects['organization/query'],
    pagination,
    location,
    isMotion,
    onChange(page) {
      dispatch({
        type: 'organization/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
        }
      })
    },
    onDeleteItem(id) {
      dispatch({
        type: 'organization/delete',
        payload: id,
      })
    },
    onEditItem(item) {
      dispatch({
        type: 'organization/selection',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'organization/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'organization/queryAllList',
        payload: {
          ...value,
          page: 1,
          pageSize,
        }
      })
    },
    onSearch(fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/organization',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/organization',
      }))
    },
    onAdd() {
      dispatch({
        type: 'organization/selection',
        payload: {
          modalType: 'create'
        }
      })
    },
    switchIsMotion() {
      dispatch({ type: 'organization/switchIsMotion' })
    },
  }

  const groupProps = {
    visible: visibleGroup,
    currentOrganization,
    priceGroupList,
    title: '编辑价格组----' + currentOrganization.name,
    wrapClassName: 'vertical-center-modal',
    onGroupOk(data) {
      dispatch({
        type: 'organization/settingPriceGroup',
        payload: data
      })
    }
  }

  const blanceProps = {
    visible: visibleBalance,
    currentOrganization,
    priceGroupList,
    title: '充值----' + currentOrganization.name,
    wrapClassName: 'vertical-center-modal',
    onBlanceOk(data) {
      dispatch({
        type: 'organization/setBlance',
        payload: data
      })
    }
  }

  const rechargeProps = {
    visible: visibleRecharge,
    currentOrganization,
    rechargeList,
    title: '充值记录----' + currentOrganization.name,
    wrapClassName: 'vertical-center-modal'
  }

  const apiProps = {
    visible: visibleApi,
    currentOrganization,
    apiList,
    title: '接口调用----' + currentOrganization.name,
    wrapClassName: 'vertical-center-modal'
  }

  const modalCancel = () => {
    dispatch({
      type: 'organization/hideModal',
    })
  }

  const onChangeTabs = (activeKey) => {
    if (activeKey === '2') {
      dispatch({
        type: 'organization/query'
      })
    } else if (activeKey === '1') {
      dispatch({
        type: 'organization/queryAllList'
      })
    }
  }

  const handleClick = (record, operationName) => {
    if (operationName === 'Operator') {
     
        dispatch(routerRedux.push({
          pathname: '/app/user',
          query: {
            organizationId:record.id,
          },
        }))
      return;
    }
    if (operationName === 'Business') {

      dispatch(routerRedux.push({
        pathname: '/app/acceptance',
        query: {
          organizationId:record.id,
        },
      }))
      return;
    }
    dispatch({
      type: `organization/show${operationName}`,
      payload: {
        currentOrganization: record
      }
    })
  }

  const columns = [
    {
      key: 'id',
      dataIndex: 'id',
      render: (text, record) => {
        return (
          <Card
            key={record.id}
            className="organizationCard"
            title={<p style={{ fontWeight: 'bold', textAlign: 'left' }}>{record.name}</p>}
          >
            <div className='organizationSetting'>
              <div onClick={() => handleClick(record, 'Operator')}>
                <Card>
                  <Icon type='team' />
                  <p>用户</p>
                  <span style={{ backgroundColor: '#ecf6fd' }}>{record.organizationOtherInfo.userCount}</span>
                </Card>
              </div>
              <div onClick={() => handleClick(record, 'Api')}>
                <Card> <Icon type='api' /> <p>接口调用</p> <span style={{ backgroundColor: 'rgb(255,240,245' }}>{record.organizationOtherInfo.interfaceCallCount}</span> </Card>
              </div>
              <div onClick={() => handleClick(record, 'Business')}>
                <Card> <Icon type='bars' /> <p>业务记录</p> </Card>
              </div>
              {
                isAdmin() && <div onClick={() => handleClick(record, 'Group')}>
                  <Card> <Icon type='layout' /> <p>组</p> <span style={{ backgroundColor: 'rgb(255, 228, 196' }}>{record.organizationOtherInfo.priceGroupName}</span> </Card>
                </div>
              }
              {
                isAdmin() && <div onClick={() => handleClick(record, 'Balance')}>
                  <Card> <Icon>¥</Icon> <p>余额</p> <span style={{ backgroundColor: 'rgb(240, 255, 240)' }}>{parseInt(record.organizationOtherInfo.balance/100)}元</span> </Card>
                </div>
              }


              <div onClick={() => handleClick(record, 'Recharge')}>
                <Card> <Icon type='bars' /> <p>充值记录</p> </Card>
              </div>
            </div>
          </Card>
        )
      },
    },
  ]

  return (
    <div className="content-inner">
      <Tabs
        defaultActiveKey='1'
        onChange={onChangeTabs}>
        <Tabs.TabPane tab={<span><Icon type="setting" />机构设置</span>} key="1">
          <div className='organizationTable'>
          <Filter {...filterProps} />
            <Table
              dataSource={allList}
              rowKey={record => record.id}
              columns={columns}
              pagination={false} />
          </div>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span><Icon type="bars" />机构列表</span>} key="2">
          <List {...listProps} />
        </Tabs.TabPane>
      </Tabs>
      {modalVisible && <Modal {...modalProps} modalCancel={modalCancel} />}
      {visibleGroup && <Group {...groupProps} modalCancel={modalCancel} />}
      {visibleBalance && <Balance {...blanceProps} modalCancel={modalCancel} />}
      {visibleRecharge && <Recharge {...rechargeProps} modalCancel={modalCancel} />}
      {visibleApi && <Api {...apiProps} modalCancel={modalCancel} />}
    </div>
  )
}

Organization.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ organization, loading }) => ({ organization, loading }))(Organization)
