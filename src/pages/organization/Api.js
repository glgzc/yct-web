import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Modal, Table } from 'antd'


const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 14,
    },
}

const Api = ({
    visible,
    currentOrganization,
    modalCancel,
    apiList,
    ...apiProps
    }) => {

    const modalOpts = {
        ...apiProps,
        visible,
        onCancel:modalCancel,
        onOk: modalCancel
    }

    const columns = [
        {
            title: '接口名称',
            dataIndex: 'interface_name',
            key: 'interface_name'
        },{
            title: '接口使用次数',
            dataIndex: 'useFrequency',
            key: 'useFrequency'
        },{
            title: '价格',
            dataIndex: 'useCost',
            key: 'useCost'
        }
    ]

    return (
        <Modal 
        {...modalOpts} >
            <Table
            dataSource={apiList} 
            columns={columns}
            rowKey={record => record.id}
            pagination= {false} />
        </Modal>
    )
}

Api.propTypes = {
    currentOrganization: PropTypes.object
}

export default Form.create()(Api)