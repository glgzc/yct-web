import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Select, Modal } from 'antd'


const FormItem = Form.Item
const Option = Select.Option

const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 14,
    },
}

const Group = ({
    visible,
    currentOrganization,
    onGroupOk,
    modalCancel,
    form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue,
    },
    priceGroupList,
    ...groupProps
    }) => {
    const handleOk = () => {
        validateFields((errors) => {
            if (errors) {
                return
            }
            const data = {
                ...getFieldsValue(),
                key: currentOrganization.key,
            }
            for(const key in data){
                if(data[key]===undefined){
                data[key]=''
                }
            }
            data.organizationId=currentOrganization.id
            data.priceGroupId=parseInt(data.priceGroupId)
            onGroupOk(data)
        })
    }

    const modalOpts = {
        ...groupProps,
        onOk: handleOk,
        visible,
        onCancel:modalCancel
    }

    return (
        <Modal 
        {...modalOpts} >
            <Form layout="horizontal">
                <FormItem label="价格组" hasFeedback {...formItemLayout}>
                {getFieldDecorator('priceGroupId', {
                    initialValue: currentOrganization.organizationOtherInfo.priceGroupId ? currentOrganization.organizationOtherInfo.priceGroupId + '' : '',
                    rules: [
                        {
                            required: true,
                        },
                    ],
                })(
                    <Select
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                    {
                        priceGroupList.length && priceGroupList.map(element => {
                            return <Select.Option value={`${element.priceGroupId}`} key={element.priceGroupId} type='option'>{element.title}</Select.Option>                
                        })
                    }
                    </Select>
                )}
                </FormItem>
            </Form>
        </Modal>
    )
}

Group.propTypes = {
    form: PropTypes.object.isRequired,
    // type: PropTypes.string,
    // item: PropTypes.object,
    // onOk: PropTypes.func,
}

export default Form.create()(Group)