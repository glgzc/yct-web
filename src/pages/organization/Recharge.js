import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Modal, Table } from 'antd'


const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 14,
    },
}

const Recharge = ({
    visible,
    currentOrganization,
    modalCancel,
    rechargeList,
    ...rechargeProps
    }) => {

    const modalOpts = {
        ...rechargeProps,
        visible,
        onCancel:modalCancel,
        onOk: modalCancel
    }

    const columns = [
        {
            title: '日期',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render : (text)=>{
                return new Date(text).format('yyyy-MM-dd hh:mm:ss')
            }
        },{
            title: '操作人',
            dataIndex: 'userName',
            key: 'userName'
        }, {
            title: '金额',
            dataIndex: 'amount',
            key: 'amount',
            render : (text)=>{
                return parseInt(text/100)
            }
        }
    ]

    return (
        <Modal 
        {...modalOpts} >
            <Table 
            dataSource={rechargeList} 
            columns={columns}
            rowKey={record => record.id} />
        </Modal>
    )
}

Recharge.propTypes = {
    currentOrganization: PropTypes.object
}

export default Form.create()(Recharge)