import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Modal } from 'antd'


const FormItem = Form.Item

const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 14,
    },
}

const Balance = ({
    visible,
    currentOrganization,
    onBlanceOk,
    modalCancel,
    form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue,
    },
    priceGroupList,
    ...blanceProps
    }) => {
    const handleOk = () => {
        validateFields((errors) => {
            if (errors) {
                return
            }
            const data = {
                ...getFieldsValue(),
                key: currentOrganization.key,
            }
            for(const key in data){
                if(data[key]===undefined){
                data[key]=''
                }
            }
            data.organizationId=currentOrganization.id
            data.amount=parseFloat(data.amount)*100
            onBlanceOk(data)
        })
    }

    const modalOpts = {
        ...blanceProps,
        onOk: handleOk,
        visible,
        onCancel:modalCancel
    }

    return (
        <Modal 
        {...modalOpts} >
            <Form layout="horizontal">
                <FormItem label="金额（元）" hasFeedback {...formItemLayout}>
                {getFieldDecorator('amount', {
                    initialValue: 0,
                    rules: [
                        {
                            required: true
                        },
                    ],
                })(<Input />)}
                </FormItem>
            </Form>
        </Modal>
    )
}

Balance.propTypes = {
    form: PropTypes.object.isRequired,
    currentOrganization: PropTypes.object,
    onOk: PropTypes.func,
}

export default Form.create()(Balance)