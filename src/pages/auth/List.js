import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal,Popconfirm,Form } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'

import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'
import '../../utils/index'

const confirm = Modal.confirm

const List = ({ onDeleteItem, onEditItem,editingKey, ...tableProps }) => {

  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该角色吗?',
        onOk () {
          onDeleteItem(record.roleId)
        },
      })
    }
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'roleId',
      key: 'roleId'
    }, {
      title: '角色名称',
      dataIndex: 'roleName',
      key: 'roleName'
    }, {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark'
    }, {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: (text) => {
        return( text!=null? text:'')
      }
    }, {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return <DropOption onMenuClick={e => handleMenuClick(record, e)} menuOptions={[{ key: '1', name: '编辑' }, { key: '2', name: '删除' }]} />
      },
    },
  ]


  return (
    <div>
      <Table
        {...tableProps}
        className={classnames({ [styles.table]: true })}
        bordered
        columns={columns}
        simple
        rowKey={record => record.roleId}
        rowSelection={null}
      />
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
}

export default List
