import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'

const Auth = ({ location, dispatch, auth, loading }) => {
  const { 
    list, 
    pagination, 
    currentItem,
    modalVisible, 
    modalType, 
    isMotion, 
    selectedRowKeys,
    treeData,
    treeCheckedKeys,
    treeSelectData,
    orgMenuList,
    roleName,
  } = auth;
  const { pageSize } = pagination

  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['auth/update'],
    title: `${modalType === 'create' ? '添加角色' : '编辑角色信息'}`,
    wrapClassName: 'vertical-center-modal',
    treeData,
    treeCheckedKeys,
    treeSelectData,
    orgMenuList,
    onOk (data) {
      // console.log(data);
      // console.log(modalType)
      dispatch({
        type: `auth/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'auth/hideModal',
      })
    },
    onTreeCheck (checkedKeys) {
      dispatch({
        type: 'auth/updateState',
        payload: {
          treeCheckedKeys: checkedKeys,
        },
      })
    },
    updateOrgMenuListByOrg(payload) {
      dispatch({
        type: 'auth/updateOrgMenuListByOrg',
        payload
      })
    }
  }

  const listProps = {
    dataSource: list,
    loading: loading.effects['auth/query'],
    pagination,
    location,
    isMotion,
    onChange(page) {
      dispatch({
        type: 'auth/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
          roleName:roleName
        }
      })
    },
    onDeleteItem (id) {
      dispatch({
        type: 'auth/delete',
        payload: id,
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'auth/menuAuth',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'auth/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }

  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch({
        type: 'auth/queryKeyWords',
        payload: {
          roleName: value.roleName,
        },
      })
    },
    onSearch (fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/auth',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/auth',
      }))
    },
    onAdd () {
      dispatch({
        type: 'auth/menuAuth',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion () {
      dispatch({ type: 'auth/switchIsMotion' })
    },
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'auth/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }

  return (
    <div className="content-inner">
      <Filter {...filterProps} />
        {
          selectedRowKeys.length > 0 &&
          <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
            <Col>
              {`Selected ${selectedRowKeys.length} items `}
              <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
                <Button type="primary" style={{ marginLeft: 8 }}>Remove</Button>
              </Popconfirm>
            </Col>
          </Row>
        }
        <List {...listProps} />
      {modalVisible && <Modal {...modalProps} />}
    </div>
  )
}

Auth.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ auth, loading }) => ({ auth, loading }))(Auth)
