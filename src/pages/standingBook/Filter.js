import React from 'react'
import PropTypes from 'prop-types'
import { config } from 'utils'
import moment from 'moment'
import { FilterItem } from 'components'
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Switch, Select, message } from 'antd'
const { host, api } = config
const { matterReportUpload } = api
const Option = Select.Option;
const Search = Input.Search
const { RangePicker } = DatePicker

const ColProps = {
  xs: 24,
  sm: 12,
  style: {
    marginBottom: 16,
  },
}

const TwoColProps = {
  ...ColProps,
  xl: 96,
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
const Filter = ({
  onAdd,
  isMotion,
  switchIsMotion,
  onFilterChange,
  filter,
  setTime,
  setInputValue,
  changeInput,
  matterReport,
  exportExcel, keywords, start, end,
  form: {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
  },
}) => {
  const handleFields = (fields) => {
    // const { createTime } = fields
    // if (createTime.length) {
    //   fields.createTime = [createTime[0].format('YYYY-MM-DD'), createTime[1].format('YYYY-MM-DD')]
    // }
    return fields
  }

  const params = {
    start: start,
    end: end,
    keywords: keywords,
  };

  let value = "";
  for (let k in params) {
    value += "&" + k + "=" + params[k];
  }
  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const changeTime = (dates, dateStrings) => {
    setTime(dateStrings[0], dateStrings[1])

  }
  const onSearch = (value) => {
    setInputValue(value)
  }
  const { name, address } = filter

  const changeInputValue = (obj) => {
    let f = obj.target.files[0]
    const formdata = new FormData();
    const token = sessionStorage.getItem("token")
    formdata.append('file', f);
    formdata.append('token', token)
    const url = host + matterReportUpload;

    fetch(url, {
      method: 'POST',
      body: formdata,
    }).then(res =>
      res.json().then(obj => {
        if (obj.code ==500) {
          message.error(obj.msg)
        } else {
          message.success(obj.msg)
        }
        let timer = setTimeout(function () {
          window.location.reload(true)
      }, 3000);
      })
    ).catch(error => message.error(error)
      // return
    );
  }

  return (
    <div style={{ paddingBottom: 20 }}>
      <div style={{ overflow: 'hidden' }}>
        <div style={{ float: 'left', height: 32, }}>
          <div style={{ float: 'left', marginTop: 6, marginRight: 36 }}>精确搜索:</div>
          <Search mode="multiple" style={{ width: 286, float: 'left' }} placeholder="请输入查询条件" onSearch={onSearch} />
        </div>
        <div style={{ float: 'right' }}>
          <Button type="primary" className="margin-right"
          // onClick={matterReport}
          >
            <a href={`${exportExcel}${value}`}
              target="_blank"
            >导出</a>
          </Button>
          <Button type="primary" className="margin-right" >
            <input type='file' onChange={changeInputValue} style={{
              opacity: 0,
              width: '100 %',
              height: '100 %',
              position: 'absolute',
              top: 0,
              left: 0
            }} />
            导 &nbsp;入
          </Button>
        </div>
      </div>
      <div style={{ overflow: 'hidden', marginTop: 34 }}>
        <div style={{ float: 'left', height: 32, }}>
          <div style={{ float: 'left', marginTop: 6, marginRight: 36 }}>时间筛选:</div>
          <RangePicker mode="multiple" style={{ width: 224, float: 'left', marginRight: 34 }} onChange={changeTime} allowClear={true} />
        </div>
        <div style={{ float: 'right' }}>
         
        </div>
      </div>
    </div>
  )
}

Filter.propTypes = {
  onAdd: PropTypes.func,
  isMotion: PropTypes.bool,
  switchIsMotion: PropTypes.func,
  form: PropTypes.object,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func,
}

export default Form.create()(Filter)
