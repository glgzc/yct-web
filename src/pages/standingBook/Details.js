import React from 'react'
import { Select, Card, Icon, Button, Table } from 'antd'
import './List.less'
import $ from "jquery"
import Lightbox from 'react-images'
import moment from 'moment'
import { config } from 'utils';
const { fileSource, showVideotURL } = config;
const Option = Select.Option;


class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      value: '',
      bottons: '查看',
      previewVisible: false,
      previewImage: [],
      previewIndex: 0,
      rotate: 0
    };
   
  }


  render() {
    const {
      partyOrganization, partyUser, handleHideDetails,
    } = this.props
    console.log(partyOrganization,'partyOrganization')
    const columns = [
      {
        title: '当事人姓名',
        dataIndex: 'name',
        key: 'name',
        render: (text) => {
          if (text>=10) {
            return text.slice(0, 10) + "...";
          } else {
            return text
          }
          
        }
      },
      {
        title: '身份证',
        dataIndex: 'idCard',
        key: 'idCard'
      },
      {
        title: '户口信息',
        dataIndex: 'registered_residence_info_state',
        key: 'registered_residence_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },
      {
        title: '婚姻信息',
        dataIndex: 'marriagestatus_info_state',
        key: 'marriagestatus_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },
      {
        title: '资产信息',
        dataIndex: 'certificateNumber',
        key: 'certificateNumber',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },
    ]
    const ORGcolumns =[
      {
        title: '机构名',
        dataIndex: 'name',
        key: 'name',
        render: (text) => {
          return text.slice(0, 10) + "...";
        }
      },
      {
        title: '统一社会信用代码',
        dataIndex: 'creditCode',
        key: 'creditCode'
      },
      // {
      //   title: '户口信息',
      //   dataIndex: 'coOrganizer',
      //   key: 'coOrganizer'
      // },
      {
        title: '资产信息(机构)',
        dataIndex: 'assets_info_state',
        key: 'assets_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },
      {
        title: '其他信息(机构)',
        dataIndex: 'other_info_state',
        key: 'other_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },
      {
        title: '法人或委托人姓名',
        dataIndex: 'userName',
        key: 'userName',
      },
      {
        title: '户口信息(自然人)',
        dataIndex: 'registered_residence_info_state',
        key: 'registered_residence_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },  {
        title: '婚姻信息(自然人)',
        dataIndex: 'marriagestatus_info_state',
        key: 'marriagestatus_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },  {
        title: '资产信息(自然人)',
        dataIndex: 'assets_info_state',
        key: 'assets_info_state',
        render: (text, record) => {
          if (text == '0' || text == '') {
            return (
              <div><p style={{ color: 'red' }}>不完整</p></div>
            )
          } else {
            return (
              <div><p style={{ color: 'green' }}>完整</p></div>
            )
          }
        }
      },
    ]
    return (
      <div>
        <h2>当事人信息</h2>
        <a onClick={handleHideDetails}><Icon type="close" className='closeIcon' /></a>
        <h3 style={{marginTop:32,marginBottom:20}}>自然人信息</h3>
        <Table
          bordered={true}
          columns={columns}
          simple
          dataSource={partyUser}
          size="middle"
          pagination={false}
          // scroll={{ x: '130%', y: 240 }}
          rowKey={record => record.userId}
        />
        <h3 style={{marginTop:32,marginBottom:20}}>机构信息</h3>
        <Table
          bordered={true}
          columns={ORGcolumns}
          dataSource={partyOrganization}
          simple
          size="middle"
          // scroll={{ x: '130%', y: 240 }}
          rowKey={record => record.userId}
          pagination={false}
        />
      </div>
    )
  }
}

function unNull(value, replace = '--') {
  return value ? value : replace;
}
export default Details;
