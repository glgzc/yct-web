import React from 'react'
import PropTypes from 'prop-types'
import { Table, Modal, Tabs, Form, Tag } from 'antd'
import classnames from 'classnames'
import { DropOption } from 'components'
import { Link } from 'dva/router'
import AnimTableBody from '../../components/DataTable/AnimTableBody'
import styles from './List.less'

const confirm = Modal.confirm
const TabPane = Tabs.TabPane;
const List = ({ onDeleteItem, onEditItem,onRowClick, editingKey, ...tableProps}) => {
  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: '确定要删除该用户吗?',
        onOk() {
          onDeleteItem(record.userId)
        },
      })
    }
  };
  const columns = [
    {
      title: '',
      children: [{
        title: '批次号',
        dataIndex: 'batchNumber',
        key: 'batchNumber'
      },
      {
        title: '事项名称',
        dataIndex: 'mattersName',
        key: 'mattersName'
      },
      {
        title: '采集时间',
        dataIndex: 'collectionTime',
        key: 'collectionTime'
      }, {
        title: '采集人姓名',
        dataIndex: 'collectionUserName',
        key: 'collectionUserName'
      },
      ]
    },
    {
      title: '事项信息是否完整',
      children: [
        {
          title: '公证证明材料',
          dataIndex: 'item1',
          key: 'item1',
          render: (text, record) => {
            if (text == '0' || text == '') {
              return (
                <div><p style={{ color: 'red' }}>不完整</p></div>
              )
            } else {
              return (
                <div><p style={{ color: 'green' }}>完整</p></div>
              )
            }
          }
        },
        {
          title: '当事人主体资格补充',
          dataIndex: 'item2',
          key: 'item2',
          render: (text, record) => {
            if (text == '0' || text == '') {
              return (
                <div><p style={{ color: 'red' }}>不完整</p></div>
              )
            } else {
              return (
                <div><p style={{ color: 'green' }}>完整</p></div>
              )
            }
          }
        }, {
          title: '办理过程材料',
          dataIndex: 'item3',
          key: 'item3',
          render: (text, record) => {
            if (text == '0' || text == '') {
              return (
                <div><p style={{ color: 'red' }}>不完整</p></div>
              )
            } else {
              return (
                <div><p style={{ color: 'green' }}>完整</p></div>
              )
            }
          }
        },
        {
          title: '有关证明材料',
          dataIndex: 'item4',
          key: 'item4',
          render: (text, record) => {
            if (text == '0' || text == '') {
              return (
                <div><p style={{ color: 'red' }}>不完整</p></div>
              )
            } else {
              return (
                <div><p style={{ color: 'green' }}>完整</p></div>
              )
            }
          }
        }, {
          title: '公证收费资料',
          dataIndex: 'item5',
          key: 'item5',
          render: (text, record) => {
            if (text == '0' || text == '') {
              return (
                <div><p style={{ color: 'red' }}>不完整</p></div>
              )
            } else {
              return (
                <div><p style={{ color: 'green' }}>完整</p></div>
              )
            }
          }
        },
      ]
    },
    {
      title: '配置与收费信息',
      children: [
        {
          title: '配置信息',
          dataIndex: 'basicInfoString',
          key: 'basicInfoString',
          render: (text, record) => {
            if (text == '' ||text==undefined) {
              return (
                <div><p>暂无信息</p></div>
              )
            } else {
              return (
                <div><p>{text}</p></div>
              )
            }
          }
        },
        {
          title: '收费信息',
          dataIndex: 'cost',
          key: 'cost',
          render: (text, record) => {
            if (text=='') {
              return<div><p>暂无信息</p></div>
            } else {
              return <div>{text}</div>
            }
          }
        }, 
      ]
    },
  ]
  return (
    <div>
      <Table
        {...tableProps}
        className={classnames({ [styles.table]: true })}
        bordered={true}
        columns={columns}
        simple
        size="middle"
        // scroll={{ x: '130%', y: 240 }}
        onRowClick={onRowClick.bind(this)}
        rowKey={record => record.userId}
      />
    </div>
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  isMotion: PropTypes.bool,
  location: PropTypes.object,
}

export default List
