import React from 'react'
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm } from 'antd'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'
import Details from './Details'
import { config, host } from "utils";

const StandingBook = ({ location, dispatch, standingBook, loading }) => {
  const { list, pagination, currentItem, modalVisible, modalType, isMotion, selectedRowKeys, treeSelectData, listByOrganization, partyOrganization, partyUser, visibleDetails,
    keywords, start, end,
  } = standingBook
  const { host, api } = config;
  console.log(keywords,'keywords')
  const { pageSize } = pagination
  const modalProps = {
    item: modalType === 'create' ? {} : currentItem,
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['standingBook/update'],
    title: `${modalType === 'create' ? '添加用户' : '编辑用户信息'}`,
    wrapClassName: 'vertical-center-modal',
    treeSelectData,
    listByOrganization,
    onOk(data) {
      dispatch({
        type: `standingBook/${modalType}`,
        payload: data,
      })
    },
    onCancel() {
      dispatch({
        type: 'standingBook/hideModal',
      })
    },
    onChange(data) {
      dispatch({
        type: 'standingBook/roleList',
        payload: data
      })
    }
  }

  const listProps = {
    dataSource: list,
    loading: loading.effects['standingBook/query'],
    pagination,
    location,
    isMotion,
    onChange(page) {
      dispatch({
        type: 'standingBook/changePage',
        payload: {
          page: page.current,
          pageSize: page.pageSize,
          start: start,
          end: end,
          keywords: keywords,
        }
      })
    },
    onDeleteItem(id) {
      dispatch({
        type: 'standingBook/delete',
        payload: id,
      })
    },
    onEditItem(item) {
      dispatch({
        type: 'standingBook/modal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
    onRowClick(record) {
      dispatch({
        type: 'standingBook/querySuccess',
        payload: {
          partyOrganization: record.partyOrganization,
          partyUser: record.partyUser,
          visibleDetails: true
        }
      })
    }
  }

  const filterProps = {
    keywords, start, end,
    exportExcel: `${host}${api.matterReportURL}?token=${sessionStorage.getItem(
      "token"
    )}`,
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange(value) {
      dispatch(routerRedux.push({
        pathname: location.pathname,
        query: {
          ...value,
          page: 1,
          pageSize,
        },
      }))
    },
    onSearch(fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/standingBook',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/standingBook',
      }))
    },
    setTime(start, end) {
      dispatch({
        type: 'standingBook/conditionsQuery',
        payload: {
          start: start,
          end: end,
          keywords: keywords
        }
      })
    },
    setInputValue(value) {
      dispatch({
        type: 'standingBook/conditionsQuery',
        payload: {
          start: start,
          end: end,
          keywords: value
        }
      })
    },
    changeInput(value) {
      dispatch({
        type: 'standingBook/editingKey',
        payload: {
          keywords: value
        }
      })
    },
    // matterReportModels
    matterReport() {
      dispatch({
        type: 'standingBook/matterReportModels',
      })
    }
  }

  const handleDeleteItems = () => {
    dispatch({
      type: 'standingBook/multiDelete',
      payload: {
        ids: selectedRowKeys,
      },
    })
  }
  const detailsProps = {
    partyOrganization, partyUser,
    handleHideDetails() {
      dispatch({
        type: 'standingBook/querySuccess',
        payload: {
          visibleDetails: false
        }
      })
    }
  }
  return (
    <div className="content">
      <div className='content-left'>
        <Filter {...filterProps} />
        {
          selectedRowKeys.length > 0 &&
          <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
            <Col>
              {`Selected ${selectedRowKeys.length} items `}
              <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
                <Button type="primary" style={{ marginLeft: 8 }}>Remove</Button>
              </Popconfirm>
            </Col>
          </Row>
        }
        <List {...listProps} />
      </div>
      {
        visibleDetails ?
          <div className='content-right'>
            <Details {...detailsProps} />
          </div>
          : null
      }
    </div>
  )
}

StandingBook.propTypes = {
  standingBook: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ standingBook, loading }) => ({ standingBook, loading }))(StandingBook)
