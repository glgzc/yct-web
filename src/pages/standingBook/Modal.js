
import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Radio, Modal, Select } from 'antd';

const FormItem = Form.Item
const Option = Select.Option

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const modal = ({
  item = {},
  onOk,
  onChange,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  treeSelectData,
  listByOrganization,
  ...modalProps
}) => {
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: item.key,
      }
      if (!data) return;
      for (const key in data) {
        if (data[key] === undefined) {
          data[key] = ''
        }
      }
      onOk(data)
    })
  }

  const handleChange = (value) => {
    onChange(value)
  }

  const modalOpts = {
    ...modalProps,
    onOk: handleOk,
  };
  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem label="用户名" hasFeedback {...formItemLayout}>
          {getFieldDecorator('username', {
            initialValue: item.username,
            rules: [
              {
                required: true,
                message: '请输入用户名'
              },
            ],
          })(<Input autoComplete={false} />)}
        </FormItem>
        <FormItem label="密码" hasFeedback {...formItemLayout}>
          {getFieldDecorator('password', {
            rules: [
              {
                required: false,
              },
            ],
          })(<Input type="password" placeholder="如无需修改密码，请忽略此项" />)}
        </FormItem>
        <FormItem label="真实姓名" hasFeedback {...formItemLayout}>
          {getFieldDecorator('realName', {
            initialValue: item.realName,
            rules: [
              {
                required: false,
              },
            ],
          })(<Input autoComplete={false}/>)}
        </FormItem>
        <FormItem label="所属机构" hasFeedback {...formItemLayout}>
          {getFieldDecorator('organizationId', {
            initialValue: item.organizationId ? item.organizationId + '': '',
            rules: [
              {
                required: true,
                message: '请选择所属机构'
              },
            ],
          })(
            <Select
              onChange={handleChange}
            >
              {
                (treeSelectData && treeSelectData.length) && treeSelectData.map(element => {
                  return <Option key={element.id+''} type='option'>{element.name}</Option>
                })
              }
            </Select>
          )}
        </FormItem>
        <FormItem label="角色" hasFeedback {...formItemLayout}>
          {getFieldDecorator('roleIdList', {
            initialValue: item.roleList && item.roleList.map(role => role.roleId + ''),
            rules: [
              {
                required: true,
                message: '请选择该用户角色'
              },
            ],
          })(
            <Select mode="multiple" >
              {
                listByOrganization && listByOrganization.length && listByOrganization.map(element => {
                  return <Option key={element.roleId} type='option'>{element.roleName  + "("+ element.remark +")"}</Option>                
                })
              }
            </Select>
          )}
        </FormItem>
        <FormItem label="手机号码" hasFeedback {...formItemLayout}>
          {getFieldDecorator('mobile', {
            initialValue: item.mobile === 'undefined' ? '' : item.mobile,
            rules: [
              {
                required: false,
                pattern: /^1[34578]\d{9}$/,
                message: '手机号码不规范',
              },
            ],
          })(<Input />)}
        </FormItem>
        <FormItem label="状态" hasFeedback {...formItemLayout}>
          {getFieldDecorator('status', {
            initialValue: item.status,
            rules: [
              {
                required: true,
                type: 'number',
              },
            ],
          })(
            <Radio.Group>
              <Radio value={1}>正常</Radio>
              <Radio value={0}>禁用</Radio>
            </Radio.Group>
          )}
        </FormItem>
        {/* 除去管理员权限选项 */}
        {/* <FormItem label="类型" hasFeedback {...formItemLayout}>
          {getFieldDecorator('type', {
            initialValue: item.type,
            rules: [
              {
                required: true,
                type: 'number',
              },
            ],
          })(
            <Radio.Group>
              <Radio value={0}>管理员</Radio>
              <Radio value={1}>普通用户</Radio>
            </Radio.Group>
          )}
        </FormItem> */}
      </Form>
    </Modal>
  )
}

modal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(modal)
