import { request, config } from 'utils'

const { api } = config
const { 
  acceptanceList,
  acceptanceInfo,
  acceptanceFile,
  brtechList,
  appCollectionAddAndEdit,
  getMatterDetailsListURL,
  getWordInfoURL,
  saveWord,
  downloadWordURL,
} = api

export async function getWordInfoService (params) {
  return request({
    url: getWordInfoURL,
    method: 'post',
    data: params,
  })
}
export async function saveWordService (params) {
  return request({
    url: saveWord,
    method: 'post',
    data: params,
  })
}
export async function downloadWordService (params) {
  return request({
    url: downloadWordURL,
    method: 'post',
    data: params,
  })
}
export async function addAndEdit (params) {
  return request({
    url: appCollectionAddAndEdit,
    method: 'post',
    data: params,
  })
}
export async function list (params) {
  return request({
    url: acceptanceList,
    method: 'post',
    data: params,
  })
}

export async function info (params) {
  return request({
    url: acceptanceInfo,
    method: 'post',
    data: params,
  })
}

export async function allList (params) {
  return request({
    url: brtechList,
    method: 'post',
    data: params,
  })
}

export async function getMatterDetailsListService (params) {
  return request({
    url: getMatterDetailsListURL,
    method: 'post',
    data: params,
  })
}

