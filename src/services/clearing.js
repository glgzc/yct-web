import { request, config } from 'utils'

const { comparisonList,clearingDelete } = config.api;

export async function query(params) {
    return request({
        url: comparisonList,
        method: 'post',
        data: params,
    })
}

export async function remove(params) {
    return request({
        url: clearingDelete,
        method: 'post',
        data: params,
        jsonReq:true,
    })
}