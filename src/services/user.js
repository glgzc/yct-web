import { request, config } from 'utils'

const { api } = config
const { users,userInfo,userRemove,userSave,userUpdate,userRole } = api

export async function query (params) {
  return request({
    url: users,
    method: 'get',
    data: params,
  })
}
export async function userinfo (params) {
  return request({
    url: userInfo,
    method: 'get',
    data: params,
  })
}

export async function create (params) {
  return request({
    url: userSave,
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: userRemove,
    method: 'post',
    data: params,
  })
}

export async function update (params) {
  return request({
    url: userUpdate,
    method: 'post',
    data: params,
  })
}
export async function roleList (params) {
  return request({
    url: userRole,
    method: 'post',
    data: params,
  })
}
