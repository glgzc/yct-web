import { request, config } from 'utils'

const { api } = config
const {getRankList,saveOrUpdateRank,deleteRank} = api

export async function query (params) {
  return request({
    url: getRankList,
    method: 'get',
    data: params,
  })
}


export async function create (params) {
  return request({
    url: saveOrUpdateRank,
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: deleteRank,
    method: 'post',
    data: params,
  })
}


