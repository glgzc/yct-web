import { request, config } from 'utils'

const { api } = config
const {getDepartmentList,deleteDepartment,saveOrUpdateDepartment} = api

export async function query (params) {
  return request({
    url: getDepartmentList,
    method: 'post',
    data: params,
  })
}


export async function create (params) {
  return request({
    url: saveOrUpdateDepartment,
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: deleteDepartment,
    method: 'post',
    data: params,
  })
}


