import { request, config } from 'utils'

const { api } = config
const {standingBookList,matterReportURL} = api

export async function query (params) {
  return request({
    url: standingBookList,
    method: 'get',
    data: params,
  })
}
export async function matterReportService (params) {
  return request({
    url: matterReportURL,
    method: 'get',
    data: params,
  })
}

