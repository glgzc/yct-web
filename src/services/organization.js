import { request, config } from 'utils'

const { api } = config
const {
    organizations,
    organizationSave,
    organizationUpdate,
    organizationRemove,
    organizationInfo,
    organizationSelect,
    organizationAllList,
    organizationPriceGroup,
    organizationRechargeList,
    organizationRecharge,
    interfaceCallRecordList
} = api

export async function query (params) {
    return request({
        url: organizations,
        method: 'post',
        data: params,
    })
}

export async function info (params) {
    return request({
        url: organizationInfo,
        method: 'post',
        data: params,
    })
}

export async function create (params) {
    return request({
        url: organizationSave,
        method: 'post',
        data: params,
    })
}

export async function remove (params) {
    return request({
        url: organizationRemove,
        method: 'post',
        data: params,
    })
}

export async function update (params) {
    return request({
        url: organizationUpdate,
        method: 'post',
        data: params,
    })
}

export async function selection (params) {
    return request({
        url: organizationSelect,
        method: 'post',
        data: params,
    })
}

export async function allList (params) {
    return request({
        url: organizationAllList,
        method: 'post',
        data: params,
    })
}

export async function priceGroup (params) {
    return request({
        url: organizationPriceGroup,
        method: 'post',
        data: params,
    })
}

export async function rechargeList (params) {
    return request({
        url: organizationRechargeList,
        method: 'post',
        data: params,
    })
}

export async function recharge (params) {
    return request({
        url: organizationRecharge,
        method: 'post',
        data: params,
    })
}

export async function interfaceCallList (params) {
    return request({
        url: interfaceCallRecordList,
        method: 'post',
        data: params,
    })
}