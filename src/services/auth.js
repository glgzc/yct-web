import { request, config } from 'utils'

const { api,host } = config
const { roles,refreshToken, roleInfo,roleRemove,roleSave,roleUpdate,roleListByOrganization } = api

export async function query (params) {
  return request({
    url: roles,
    method: 'post',
    data: params,
  })
}
export async function refreshTokenByOrg(params) {
  return request({
    url: refreshToken,
    method: 'post',
    data: params,
  })
}
export async function info (params) {
  return request({
    url: roleInfo+`/${params.roleId}`,
    method: 'get',
    data: params,
  })
}

export async function createRole (params) {
  return request({
    url: roleSave,
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: roleRemove,
    method: 'post',
    data: params,
  })
}

export async function updateAuth (params) {
  return request({
    url: roleUpdate,
    method: 'post',
    data: params
  })
}

export async function roleList (params) {
  return request({
    url: roleListByOrganization,
    method: 'post',
    data: params,
  })
}

