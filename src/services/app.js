import { request, config } from 'utils';
const { api } = config
const { userState, userLogout, userLogin,changePassWordURL } = api

export async function changePassWordService (params) {
  return request({
    url: changePassWordURL,
    method: 'get',
    data: params,
  })
}
export async function login (params) {
  return request({
    url: userLogin,
    method: 'post',
    data: params,
  })
}

export async function logout (params) {
  return request({
    url: userLogout,
    method: 'post',
    data: params,
  })
}

export async function query (params) {

  return request({
    url: userState,
    method: 'get',
    data: params,
  })
  
}
