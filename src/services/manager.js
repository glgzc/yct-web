import { request, config } from 'utils'

const { api } = config
const { managers, managerSave, managerInfo, managerRemove, managerUpdate } = api

export async function query (data) {
  return request({
    url: managers,
    method: 'get',
    data,
  })
}

export async function info (data) {
  return request({
    url: `${managerInfo}/${data.id}`,
    method: 'get',
  })
}

export async function save (data) {
  return request({
    url: managerSave,
    method: 'post',
    data,
  })
}

export async function remove (data) {
  return request({
    url: managerRemove,
    method: 'post',
    data,
  })
}

export async function update (data) {
  return request({
    url: managerUpdate,
    method: 'post',
    data,
  })
}
