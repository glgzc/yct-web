import { request, config } from 'utils'

const { api,host } = config
const { getVideoList,downVideoList,deleteVideo } = api

export async function query (params) {
  return request({
    url: getVideoList,
    method: 'post',
    data: params,
  })
}
export async function downloadService(params) {
  return request({
    url: downVideoList,
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: deleteVideo,
    method: 'post',
    data: params,
  })
}

