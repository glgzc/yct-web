import { request, config } from 'utils'

const { api } = config
const { 
  clientList,
  clientInfo,
  addpartyUser,
  fileDeleteURL,
  editPeople,
} = api

export async function add (params) {
  return request({
    url: addpartyUser,
    method: 'post',
    data: params,
  })
}

export async function list (params) {
  return request({
    url: clientList,
    method: 'post',
    data: params,
  })
}

export async function info (params) {
  return request({
    url: clientInfo,
    method: 'post',
    data: params,
  })
}

export async function infoNew (params) {
  return request({
    url: editPeople,
    method: 'post',
    data: params,
  })
}
export async function deleteImg (params) {
  return request({
    url: fileDeleteURL,
    method: 'post',
    data: params,
  })
}