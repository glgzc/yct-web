import { request, config } from 'utils'

const { api } = config
const { 
  groupList, 
  groupPriceList,
  groupSave,
  groupRemove,
  interfaceList,
  interfaceSave,
  interfaceUpdate,
  interfaceRemove,
  settingEnable,
  settingDisable,
  settingUpdate,
  partyOrganization,
  partyUserList
} = api

export async function getPartyUserList (params) {
  return request({
    url: partyUserList,
    method: 'post',
    data: params,
  })
}

export async function add (params) {
  return request({
    url: partyOrganization,
    method: 'post',
    data: params,
  })
}

export async function priceGroupList (params) {
  return request({
    url: groupList,
    method: 'post',
    data: params,
  })
}

export async function priceGroupPriceList (params) {
  return request({
    url: groupPriceList,
    method: 'post',
    data: params,
  })
}

export async function priceGroupSave (params) {
  return request({
    url: groupSave,
    method: 'post',
    data: params,
  })
}

export async function priceGroupRemove (params) {
  return request({
    url: groupRemove,
    method: 'post',
    data: params,
  })
}

export async function chargeInterfaceList (params) {
  return request({
    url: interfaceList,
    method: 'post',
    data: params,
  })
}

export async function chargeInterfaceSave (params) {
  return request({
    url: interfaceSave,
    method: 'post',
    data: params,
  })
}

export async function chargeInterfaceUpdate (params) {
  return request({
    url: interfaceUpdate,
    method: 'post',
    data: params,
  })
}

export async function chargeInterfaceRemove (params) {
  return request({
    url: interfaceRemove,
    method: 'post',
    data: params,
  })
}

export async function priceSettingEnable (params) {
  return request({
    url: settingEnable,
    method: 'post',
    data: params,
  })
}

export async function priceSettingDisable (params) {
  return request({
    url: settingDisable,
    method: 'post',
    data: params,
  })
}

export async function priceSettingUpdate (params) {
  return request({
    url: settingUpdate,
    method: 'post',
    data: params,
  })
}