import { request, config } from 'utils'

const { api } = config
const { nav, menus, menuSelect, menuInfo, menuSave, menuUpdate, menuRemove } = api


export async function queryNav (data) {
  return request({
    url: nav,
    method: 'get',
    data,
  })
}

export async function query (data) {
  return request({
    url: menus,
    method: 'get',
    data,
  })
}
export async function select (data) {
  return request({
    url: menuSelect,
    method: 'get',
    data,
  })
}
export async function info (data) {
  return request({
    url: `${menuInfo}/${data.id}`,
    method: 'get',
  })
}
export async function save (data) {
  return request({
    url: menuSave,
    method: 'post',
    data,
  })
}
export async function update (data) {
  return request({
    url: menuUpdate,
    method: 'post',
    data,
  })
}
export async function remove (data) {
  return request({
    url: menuRemove,
    method: 'post',
    data,
  })
}
