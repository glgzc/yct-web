import { request, config } from 'utils';
const { api } = config;
const { getPushDataURL,pushBatchDataURL} = api

export async function getPushDataURLService (params) {
  return request({
    url: getPushDataURL,
    method: 'post',
    data: params,
  })
}

export async function pushBatchDataService (params) {
  return request({
    url: pushBatchDataURL,
    method: 'post',
    data: params,
  })
}