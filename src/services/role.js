import { request, config } from 'utils'

const { api } = config
const { roles, roleInfo, roleRemove, roleSave, roleUpdate } = api


export async function query (data) {
  return request({
    url: roles,
    method: 'get',
    data,
  })
}
export async function info (data) {
  return request({
    url: `${roleInfo}/${data.id}`,
    method: 'get',
  })
}
export async function remove (data) {
  return request({
    url: roleRemove,
    method: 'post',
    data,
  })
}
export async function save (data) {
  return request({
    url: roleSave,
    method: 'post',
    data,
  })
}
export async function update (data) {
  return request({
    url: roleUpdate,
    method: 'post',
    data,
  })
}
