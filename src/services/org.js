import { request, config } from 'utils';
const { api } = config;
const { 
  orgList,
  orgInfo,
  partyOrganization,
  partyUserList,
  allMORGList,
} = api

export async function getPartyUserList (params) {
  return request({
    url: partyUserList,
    method: 'post',
    data: params,
  })
}
export async function getAllORGList (params) {
  return request({
    url: allMORGList,
    method: 'post',
    data: params,
  })
}

export async function add (params) {
  return request({
    url: partyOrganization,
    method: 'post',
    data: params,
  })
}

export async function list (params) {
  return request({
    url: orgList,
    method: 'post',
    data: params,
  })
}

export async function info (params) {
  return request({
    url: orgInfo,
    method: 'post',
    data: params,
  })
}