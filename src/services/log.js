import { request, config } from 'utils'

const { api } = config
const { logs } = api

export async function query (data) {
  return request({
    url: logs,
    method: 'get',
    data,
  })
}
