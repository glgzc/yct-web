import { request, config } from 'utils'

const { api } = config
const {
  collectionBasicInfoSave,
  collectionBasicInfoUpdate,
  collectionBasicInfo,
  collectionBasicInfoList,
  tagListURL,
  addDefaultItemsURL,
  fileDeleteURL,
  memoTagListURL
} = api

export async function fileDeleteService(params) {
  return request({
    url: fileDeleteURL,
    method: 'get',
    data: params,
  })
}
export async function addDefaultItemsService(params) {
  return request({
    url: addDefaultItemsURL,
    method: 'post',
    data: params,
  })
}
export async function tagListService(params) {
  return request({
    url: tagListURL,
    method: 'post',
    data: params,
  })
}
export async function memoTagList(params) {
  return request({
    url: memoTagListURL,
    method: 'post',
    data: params,
  })
}
export async function add(params) {
  return request({
    url: collectionBasicInfoSave,
    method: 'post',
    data: params,
  })
}

export async function update(params) {
  return request({
    url: collectionBasicInfoUpdate,
    method: 'post',
    data: params,
  })
}

export async function list(params) {
  return request({
    url: collectionBasicInfoList,
    method: 'post',
    data: params,
  })
}

export async function info(params) {
  return request({
    url: collectionBasicInfo,
    method: 'post',
    data: params,
  })
}