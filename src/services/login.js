import { request, config } from 'utils'

const { api } = config
const { userLogin } = api

export async function signIn (data) {
  const uuid = sessionStorage.getItem('uuid')
  data.key = uuid
  return request({
    url: userLogin,
    method: 'post',
    data,
  })
}
