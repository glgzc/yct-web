import React from 'react'

class SignViewer extends React.Component {
    getCtlObject() {
        return document.getElementById("SignViewer");
    }
    regClickEvent() {
        //clear button
        this.getCtlObject().RegBtnClickEvent(1, 965, 208, 120, 60);
        //confirm button
        this.getCtlObject().RegBtnClickEvent(2, 965, 348, 120, 60);
    }

    initSignSetting() {
        var ctl_obj = this.getCtlObject();
        if(!ctl_obj.SetPenSizeRange){
            return
        }
        ctl_obj.SetPenSizeRange(1, 4);
        ctl_obj.SetPenColor(0, 0, 0);
        ctl_obj.BorderVisible = 0;
        ctl_obj.DisplayMapMode = 0;

        //Registration click event
        this.regClickEvent();

        //Gets the connection state of the device.
        if(!ctl_obj.IsConnected()){
            alert("设备连接失败")
        }

    }
    getSignBase64() {

        if (!this.getCtlObject().IsOperated||this.getCtlObject().IsOperated() === false) {
            alert("设备初始化失败")
            return ""
        }

        const w = this.getCtlObject().width;

        const h = this.getCtlObject().height;
        const base64 = this.getCtlObject().SaveSignToBase64(w, h);
        return base64
    }
    clear(){
        this.getCtlObject().clearSign();
    }
    shouldComponentUpdate(){
        return false
    }
    componentDidMount(){
        this.initSignSetting()
    }
    render () {
        const { style } = this.props

        return (
            <object id="SignViewer" 
            border="0" 
            classID="CLSID:5E845176-C3D3-4A79-92D7-CD38F1112621" 
            width={style.width}
            height={style.height}></object>
        )
    }
}

export default SignViewer
