import React from 'react'
import PropTypes from 'prop-types'
import { Menu, Icon, Popover, Dropdown, Modal } from 'antd'
import classnames from 'classnames'
import styles from './Header.less'
import Menus from './Menu'
import ReSetPassWordComponent from './ResetPassWord'
const SubMenu = Menu.SubMenu

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showPassWordModal: false,
    }
  }
  handleClickMenu = item => {
    if (item.key === 'logout') {
      this.props.logout();
      return;
    }
    else if (item.key === 'info') {
      //todo
    } else if (item.key === 'resetPassword') {
      this.setState({
        showPassWordModal: true
      })
    }
  }
  handleOrgChange=item=>{
    for (let org of this.props.orgList) {
      if (org.id === Number(item.key)) {
        this.props.setCurrentOrg(org);
        break;
      }
    }
  }
  say = (newPassWord, oldPassWord) => {
    this.props.reSetPassWord(newPassWord, oldPassWord);
  }
  render() {
    const { user, switchSider, siderFold, orgList, currentOrg, isNavbar, menuPopoverVisible, location, switchMenuPopover, navOpenKeys, changeOpenKeys, menu, } = this.props;
    const { showPassWordModal } = this.state
    const menusProps = {
      menu,
      siderFold: false,
      darkTheme: false,
      isNavbar,
      handleClickNavMenu: switchMenuPopover,
      location,
      navOpenKeys,
      changeOpenKeys,
    }
    const theCancleShowPassWordModal = () => {
      this.setState({
        showPassWordModal: false
      })
    }
    const theOKs = (newPassWord, oldPassWord) => {
      this.say(newPassWord, oldPassWord)
    }
    const ReSetPassWordComponentProps = {
      visible: showPassWordModal,
      title: '修改密码',
      // wrapClassName: 'vertical-center-modal',
      centered: true,
      width: 398,
      onCancel() {
        theCancleShowPassWordModal()
      },
      theCancle() {
        theCancleShowPassWordModal()
      },
      theOK: theOKs
    }
    localStorage.setItem("username", user.username)
    return (
      <div className={styles.header}>
        {isNavbar
          ? <Popover placement="bottomLeft" onVisibleChange={switchMenuPopover} visible={menuPopoverVisible} overlayClassName={styles.popovermenu} trigger="click" content={<Menus {...menusProps} />}>
            <div className={styles.button}>
              <Icon type="bars" />
            </div>
          </Popover>
          : <div
            className={styles.button}
            onClick={switchSider}
          >
            <Icon type={classnames({ 'menu-unfold': siderFold, 'menu-fold': !siderFold })} />
          </div>}
        <Menu mode="horizontal" onClick={this.handleOrgChange} selectable={false}>
          <SubMenu title={<b style={{ color: 'black' }}>
            {currentOrg && currentOrg.name}
          </b>}>
            {
              orgList.map(org => <Menu.Item key={org.id}>
                {org.name}
              </Menu.Item>)
            }
          </SubMenu>
        </Menu>
        <div className={styles.rightWarpper}>
          <div className={styles.button}>
            <Icon type="mail" />
          </div>
          <Menu mode="horizontal" onClick={this.handleClickMenu} selectable={false}>
            <SubMenu
              style={{
                float: 'right',
              }}
              title={<span>
                <Icon type="user" />
                {user.username}
              </span>}
            >
              <Menu.Item key="info">
                个人信息
              </Menu.Item>
              <Menu.Item key="resetPassword">
                修改密码
              </Menu.Item>
              <Menu.Item key="logout">
                注销登录
              </Menu.Item>
            </SubMenu>
          </Menu>
        </div>
        <ReSetPassWordComponent {...ReSetPassWordComponentProps} />
      </div>
    )
  }
}
Header.propTypes = {
  menu: PropTypes.array,
  user: PropTypes.object,
  logout: PropTypes.func,
  switchSider: PropTypes.func,
  siderFold: PropTypes.bool,
  isNavbar: PropTypes.bool,
  menuPopoverVisible: PropTypes.bool,
  location: PropTypes.object,
  switchMenuPopover: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
}

export default Header
