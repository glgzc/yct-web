/* global location */
import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb, Icon } from 'antd'
import { Link } from 'dva/router'
import pathToRegexp from 'path-to-regexp'
import { queryArray } from 'utils'
import styles from './Bread.less'

const Bread = ({ menu }) => {
  // 菜单只映射路由的前两位
  const filterTwo = (pathname) => {
    let temp = pathname.split('/');
    if (temp.length > 3) {
        temp = temp.slice(0, 3);
        return temp.join('/');
    } else {
        return pathname;
    }
  }
  // 匹配当前路由
  let pathArray = []
  let current
  for (let index in menu) {
    if (menu[index].url && pathToRegexp(menu[index].url).exec(filterTwo(location.pathname))) {
      current = menu[index]
      break
    }
  }
  
  const getPathArray = (item) => {
    pathArray.unshift(item)
    if (item.parentId) {
      getPathArray(queryArray(menu, item.parentId, 'menuId'))
    }
  }

  if (!current) {
    pathArray.push(menu[0] || {
      id: 1,
      icon: 'laptop',
      name: 'Dashboard',
    })
    pathArray.push({
      id: 404,
      name: 'Not Found',
    })
  } else {
    getPathArray(current)
  }

  // 递归查找父级
  const breads = pathArray.map((item, key) => {
    const content = (
      <span>{item.icon
        ? <Icon type={item.icon} style={{ marginRight: 4 }} />
        : ''}{item.name}</span>
    )
    return (
      <Breadcrumb.Item key={key}>
        {((pathArray.length - 1) !== key)
          ? <Link to={item.url||'#'}>
            {content}
          </Link>
          : content}
      </Breadcrumb.Item>
    )
  })

  return (
    <div className={styles.bread}>
      <Breadcrumb>
        {breads}
      </Breadcrumb>
    </div>
  )
}

Bread.propTypes = {
  menu: PropTypes.array,
}

export default Bread
