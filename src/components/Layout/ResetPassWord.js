import React from 'react'
import PropTypes from 'prop-types'
import { Form, Tree, Modal, Card, Button, Input, Checkbox, Row, Col, InputNumber, message } from 'antd'
import { config } from 'utils';
import Password from 'antd/lib/input/Password';
const { fileSource } = config;


const reSetPassWord = ({
  theCancle,
  theOK,
  form,
  ...ReSetPassWordComponentProps
}) => {
  const { validateFields, getFieldDecorator, getFieldsValue, resetFields } = form;
  const modalPropsModal = {
    ...ReSetPassWordComponentProps,
    // onOk: click,
  }
  const validatorSize = (rule, value, callback) => {
    let b = /[^\x00-\xff]/ig
    if (value.length < 6) {
      callback('新密码个数必须在6-12个之间!')
    } else if (b.test(value)) {
      callback('密码只能输入数字和英文字母和半角符号!')
    }
  }
  const validatorSame = (rule, value, callback) => {
    const data = getFieldsValue()
    let b = /[^\x00-\xff]/ig
    // 获取新密码的内容
    if (value.length < 6) {
      callback('新密码个数必须在6-12个之间!')
    }
    else if (data.newPassWord != value) {
      callback('新密码与确认密码前后不一致!')
    }else if (b.test(value)) {
      callback('密码只能输入数字和英文字母和半角符号!')
    }
  }
  const handleCancle = () => {
    theCancle()
    resetFields()
  }
  const handleOk = () => {
    const data = getFieldsValue()
    let newPassWord = data.newPassWord
    let oldPassWord = data.currentPassWord
    let okPassWord = data.determinePassWord
    let b = /[^\x00-\xff]/ig
    if (okPassWord != newPassWord) {
      message.error('两次密码不一致!')
      return
    }
    else if (newPassWord.length==0&&okPassWord.length==0) {
      message.error('新密码和确认密码都不能为空!')
    }
    else if (newPassWord.length==0&&okPassWord.length==0) {
      message.error('新密码和确认密码都不能为空!')
    }
    else if (b.test(newPassWord) ||b.test(okPassWord) ) {
      message.error('密码只能输入数字和英文字母和半角符号!')
    }
    else if (newPassWord.length<6||oldPassWord.length<6||okPassWord.length<6) {
      message.error('密码个数必须在6-12个之间!')
    }
    else {
      theOK(newPassWord, oldPassWord)
    }
  }
  return (
    <Modal  {...modalPropsModal}
      footer={[
        <div style={{ textAlign: 'center' }}>
          <Button key="back" type="primary"
            onClick={handleOk}
          >确定</Button>
          <Button key="submit"
            onClick={handleCancle}
          >取消</Button>
        </div>
      ]}
    >
      <div>
        <Form layout={"vertical"}>
          <Form.Item label="当前密码" >
            {getFieldDecorator('currentPassWord', { initialValue: '', rules: [{ required: true, message: '当前密码为必填项!' }], })(<Input maxLength={12} type={'Password'}/>)}
          </Form.Item>
          <Form.Item label="新的密码" >
            {getFieldDecorator('newPassWord', { initialValue: '', rules: [{ required: true, message: '新密码为必填项!' }, { validator: validatorSize }], validateTrigger: 'onBlur' })(<Input maxLength={12} type={'Password'}/>)}
          </Form.Item>
          <Form.Item label="确认密码" >
            {getFieldDecorator('determinePassWord', { initialValue: '', rules: [{ required: true, message: '确认密码为必填项!' }, { validator: validatorSame }],validateTrigger: 'onBlur'  })(<Input maxLength={12} type={'Password'}/>)}
          </Form.Item>
        </Form>
      </div>
    </Modal>
  )
}
function unNull(value, replace = '--') {
  return value ? value : replace;
}
export default Form.create()(reSetPassWord)
