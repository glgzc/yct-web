import {resolve} from "path";

const routes=[
  {
    "path": "/",
    "component": '../layouts/index.js',
    "routes": [
      {
        "path": "/",
        "component": './price'
      },
      {
        "path": "/login",
        "component": './login'
      },
      {
        "path": "/app/iframe",
        "component": './iframe'
      },
      {
        "path": "/app/client",
        "component": './client'
      },
      {
        "path": "/app/client/form",
        "component": './client/form'
      },
      {
        "path": "/app/org",
        "component": './org'
      },
      {
        "path": "/app/org/form",
        "component": './org/form'
      },
      {
        "path": "/app/user",
        "component": './user'
      },
      {
        "path": "/sys/manager",
        "component": './manager'
      },
      {
        "path": "/sys/menu",
        "component": './menu'
      },
      {
        "path": "/sys/role",
        "component": './role'
      },
      {
        "path": "/sys/log",
        "component": './log'
      },

      {
        "path": "/app/price",
        "component": './price',
      },

      {
        "path": "/app/auth",
        "component": './auth'
      },
      {
        "path": "/app/organization",
        "component": './organization'
      },
      {
        "path": "/app/clearing",
        "component": './clearing'
      },
      {
        "path": "/app/acceptance",
        "component": './acceptance'
      },
      {
        "path": "/informationConfiguration/notary",
        "component": './informationConfiguration/NotaryPresuppose'
      },
      {
        "path": "/informationConfiguration/remarks",
        "component": './informationConfiguration/ComplementaryRemarks'
      },
      {
        "path": "/app/standingBook",
        "component": './standingBook'
      },
      {
        "path": "/app/standingBook",
        "component": './standingBook'
      },
      {
        "path": "/app/department",
        "component": './department'
      },
      {
        "path": "/app/rank",
        "component": './rank'
      },
      {
        "path": "/app/video",
        "component": './video'
      },
      {
        "path": "/app/pushData",
        "component": './pushData'
      },
    ]
  }
]
export default {
  treeShaking: true,
  targets: {
    ie: 9,
  },
  routes:routes,
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    ['umi-plugin-react', {
      antd: true,
      dva: true,
      dynamicImport: false,
      title: 'yct',
      dll: false,
      
      routes: {
        exclude: [
          /components\//,
        ],
      },
    }],
  ],
  theme: "./theme.config.js",
  alias: {
    themes: resolve(__dirname, './src/themes'),
    components: resolve(__dirname,"./src/components"),
    utils: resolve(__dirname,"./src/utils"),
    config: resolve(__dirname,"./src/utils/config"),
    enums: resolve(__dirname,"./src/utils/enums"),
    services: resolve(__dirname,"./src/services"),
    models: resolve(__dirname,"./src/models"),
    routes: resolve(__dirname,"./src/routes"),
  },
  cssLoaderOptions:{
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        }
      ]
    }
  }
}
