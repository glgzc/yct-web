var eloam = {
  eloamGlobal: null,
  deviceMain: null,
  deviceAssist: null,
  httpUploadServer:null,
  init: function(httpUploadServer) {
    if (this.eloamGlobal) {
      return this.eloamGlobal;
    }
    this.httpUploadServer=httpUploadServer
    this.eloamGlobal = document.getElementById('EloamGlobal_ID');
    if(!this.eloamGlobal.attachEvent){
        this.eloamGlobal=null
        return
    }
    this.eloamGlobal.attachEvent('DevChange', this.onDevChange.bind(this));
    this.eloamGlobal.attachEvent('Biokey', this.onBiokey.bind(this));
    //this.eloamGlobal.GetKeyFromSoftDog(20);
    if (!this.eloamGlobal) {
      return;
    }
    var ret = this.eloamGlobal.InitDevs();

    if (ret) {
      if (!this.eloamGlobal.VideoCapInit()) {
        alert('初始化录像失败');
      }
      //OpenBothVideo();
    }
    hasLoadSuccess = 1;

    return this.eloamGlobal;
  },
  unload: function() {
    if( window.videoMain){
        window.videoMain.Destroy();
        window.videoMain = null;
    }
    if( window.videoAssist){
        window.videoAssist.Destroy();
        window.videoAssist = null;
    }
    if (this.deviceMain) {
      this.deviceMain.Destroy();
      this.deviceMain = null;
    }

    if (this.deviceAssist) {
      this.deviceAssist.Destroy();
      this.deviceAssist = null;
    }

    if (!this.eloamGlobal) {
      return;
    }
    //this.eloamGlobal.DeinitFaceDetect();
    this.eloamGlobal.DeinitIdCard();
    this.eloamGlobal.DeinitBarcode();
    this.eloamGlobal.StopGetBiokeyFeature()
    this.eloamGlobal.DeinitBiokey();
    this.eloamGlobal.DeinitDevs();
    this.eloamGlobal = null;
    window.eloamGlobal = null;
  },
  onDevChange: function(type, idx, dbt) {
    if (!this.eloamGlobal) {
      return;
    }
    //设备接入和丢失
    //type设备类型， 1 表示视频设备， 2 表示音频设备
    //idx设备索引
    //dbt设备动作类型
    if (1 == type) {
      if (1 == dbt) {
        //dbt 1 表示设备到达
        var deviceType = this.eloamGlobal.GetEloamType(1, idx);

        if (1 == deviceType) {
          //主摄像头
          if (null == this.deviceMain) {
            this.deviceMain = this.eloamGlobal.CreateDevice(1, idx);
          }
        } else if (2 == deviceType || 3 == deviceType) {
          //辅摄像头
          if (null == this.deviceAssist) {
            this.deviceAssist = this.eloamGlobal.CreateDevice(1, idx);
          }
        }
      }

      if (2 == dbt) {
        //dbt 2 表示设备丢失
      }
    }
  },
  onBiokey: function(ret){
   
    //指纹特征
    if (8 == ret){
      var img = this.eloamGlobal.GetBiokeyImg();

      var imageBase64 = img.GetBase64(13, 0x0010);

      sessionStorage.setItem("fingerImage",imageBase64)
      img.Destroy();
    }
    this.eloamGlobal.StopGetBiokeyFeature()
    this.eloamGlobal.DeinitBiokey();
    
  },
  upload:function(image,fileName){
    //var http = this.eloamGlobal.CreateHttp("http://file.sip.feelbang.com/upload?token=c4dc406a542b60d03e5158fa5a41160c&sys=1001");
    if(!this.httpUploadServer){
      alert("文件上传服务器地址不存在")
      return null
    }
    
    var http = this.eloamGlobal.CreateHttp(this.httpUploadServer);
    var info=null
    if (image)
    {
        if(http){
          try{
            var b=http.UploadImage(image, 2, 0, fileName);
            if(!b){
              alert("连接服务器失败")
            }else{
              info=http.GetServerInfo()
            }
          }catch(e){
            console.log(e)
          }
           
        }
        image.Destroy();
        image = null;
        if(http){
            http.Destroy();
            http = null;
        }
       
    }

    return info
  },
  uploadMemory:function(image){
    var http = this.eloamGlobal.CreateHttp("http://192.168.2.153:9000/appCollectionFile/upload");
    var info=null
    if (image)
    {

        if(http){
            var b=http.UploadMemory(0,image,"","","");
    
            info=http.GetServerInfo()

        }
        image.Destroy();
        image = null;
        if(http){
            http.Destroy();
            http = null;
        }
      
    }
    return info
  }
};

window.eloam = eloam;
